# Contributors
Git stores every contribution, use the following command to discover who
we are:

`git log --pretty="format:%an <%ae>" | sort | uniq`

Commits that cannot be traced back to a person shall not concur to the
contributors list.

# Maintainers
Domenico Andreoli <domenico.andreoli@~~µ~~linux.com> minus ~~µ~~

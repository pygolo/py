GNUMAKE ?= gmake

all:
	@${GNUMAKE} $@

.DEFAULT:
	@${GNUMAKE} $@

.PHONY: examples test

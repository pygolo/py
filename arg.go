/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"
import "fmt"

// Arg_ParseTuple parses positional parameters into local variables.
//
// C API: https://docs.python.org/3/c-api/arg.html#c.PyArg_ParseTuple
func (Py Py) Arg_ParseTuple(o Object, args ...interface{}) (err error) {
	length, err := Py.Object_Length(o)
	if err != nil {
		return err
	}
	if len(args) != length {
		return positionalMismatchError(len(args), length)
	}
	j := -1
	defer func() {
		// on error, release all the resources that were acquired with the
		// conversions performed until the error occurred
		if err != nil {
			for i := j; i >= 0; i-- {
				Py.undoGoFromObject(args[i])
			}
		}
	}()
	for i, arg := range args {
		item, err := Py.Tuple_GetItem(o, i)
		if err != nil {
			return err
		}
		err = Py.GoFromObject(item, arg)
		if err != nil {
			return fmt.Errorf("arg #%d: %s", i, err)
		}
		j = i
	}
	return nil
}

// Arg_ParseTupleAndKeywords parses positional and keyword parameters into local variables.
//
// C API: https://docs.python.org/3/c-api/arg.html#c.PyArg_ParseTuple
func (Py Py) Arg_ParseTupleAndKeywords(args, kwargs Object, vars GoArgsKwArgs) (err error) {
	nargs, err := Py.Object_Length(args)
	if err != nil {
		return err
	}
	const (
		mandatory = iota
		optional
		keyword_only
	)
	state := mandatory
	kw_count := make(map[string]int)
	iarg := 0
	j := -1
	defer func() {
		// on error, release all the resources that were acquired with the
		// conversions performed until the error occurred
		if err != nil {
			for i := j; i >= 0; i-- {
				Py.undoGoFromObject(vars.vars[i].v)
			}
		}
	}()
	for i, x := range vars.vars {
		switch x.k {
		case "|":
			if state != mandatory {
				return &GoError{"SystemError", fmt.Sprintf("unexpected Optional delimiter at #%d", i), nil}
			}
			state = optional
		case "$":
			if state == keyword_only {
				return &GoError{"SystemError", fmt.Sprintf("unexpected KeywordOnly delimiter at #%d", i), nil}
			}
			state = keyword_only
		case "":
			if len(kw_count) > 0 || state == keyword_only {
				return &GoError{"SystemError", fmt.Sprintf("unexpected positional variable parsing at #%d", i), nil}
			}
			if iarg < nargs {
				o_value, err := Py.Tuple_GetItem(args, iarg)
				if err != nil {
					return err
				}
				err = Py.GoFromObject(o_value, x.v)
				if err != nil {
					return &GoError{"TypeError", fmt.Sprintf("positional #%d: %s", iarg, err), nil}
				}
				iarg++
			} else if state == mandatory {
				iarg++
			} else if o, ok := x.v.(*Object); ok {
				Py.IncRef(*o)
			}
		default:
			var o_value Object
			if kwargs.o != nil {
				o_key, err := Py.GoToObject(x.k)
				defer Py.DecRef(o_key)
				if err != nil {
					return err
				}
				o_value, err = Py.Dict_GetItem(kwargs, o_key)
				defer Py.DecRef(o_value)
				if err != nil {
					return err
				}
				if o_value.o != nil {
					// o_value is a borrowed reference, need to increase
					// its refcount before removing it from kwargs
					Py.IncRef(o_value)
					err = Py.Dict_DelItem(kwargs, o_key)
					if err != nil {
						return err
					}
				}
			}
			if state != keyword_only && iarg < nargs {
				if o_value.o != nil {
					return &GoError{"TypeError", fmt.Sprintf("got multiple values for argument '%s'", x.k), nil}
				}
				o_value, err = Py.Tuple_GetItem(args, iarg)
				if err != nil {
					return err
				}
				err = Py.GoFromObject(o_value, x.v)
				if err != nil {
					return &GoError{"TypeError", fmt.Sprintf("positional #%d: %s", iarg, err), nil}
				}
				iarg++
			} else if o_value.o != nil {
				err = Py.GoFromObject(o_value, x.v)
				if err != nil {
					return &GoError{"TypeError", fmt.Sprintf("keyword '%s': %s", x.k, err), nil}
				}
			} else if state == mandatory {
				iarg++
			} else if o, ok := x.v.(*Object); ok {
				Py.IncRef(*o)
			}
			kw_count[x.k]++
		}
		j = i
	}
	if iarg != nargs {
		return positionalMismatchError(iarg, nargs)
	}
	if vars.unknown {
		return nil
	}
	return Py.GoForEachKey(kwargs, func(Py pyPy, o_key Object) error {
		var key string
		err := Py.GoFromObject(o_key, &key)
		if err != nil {
			return err
		}
		return &GoError{"TypeError", fmt.Sprintf("got an unexpected keyword argument '%s'", key), nil}
	})
}

func positionalMismatchError(expected, given int) error {
	s := "arguments"
	if expected == 1 {
		s = "argument"
	}
	w := "were"
	if given == 1 {
		w = "was"
	}
	msg := fmt.Sprintf("takes %d positional %s but %d %s given", expected, s, given, w)
	return &GoError{"TypeError", msg, nil}
}

// GoArgsKwArgs describes the parsing of Python call arguments.
//
// Use in conjunction with Arg_ParseTupleAndKeywords to parse
// positional and keyword arguments passed by the Python
// interpreter on function and method invocation.
type GoArgsKwArgs struct {
	vars []struct {
		k string
		v interface{}
	}
	unknown bool
}

func (g *GoArgsKwArgs) append(k string, v interface{}) {
	g.vars = append(g.vars, struct {
		k string
		v interface{}
	}{k, v})
}

// Positional configures a positional argument parser.
//
// v is the pointer to the receiving variable which can be of any type
// supported by the conversion system and contains the default value used
// when, if the argument is optional, no argument is passed by the caller.
//
// In case the default value is used and it's a py.Object, its reference
// count is incremented.
func (g *GoArgsKwArgs) Positional(v interface{}) *GoArgsKwArgs {
	g.append("", v)
	return g
}

// Keyword configures a keyword argument parser.
//
// k is the name of the keyword argument, v is the pointer to the receiving
// variable which can be of any type supported by the conversion system and
// contains the default value used when, if the argument is optional, no
// argument is passed by the caller.
//
// In case the default value is used and it's a py.Object, its reference
// count is incremented.
func (g *GoArgsKwArgs) Keyword(k string, v interface{}) *GoArgsKwArgs {
	g.append(k, v)
	return g
}

// Optional indicates that all the following parsers are for optional arguments.
//
// All the parsers, either positional or keyword, subsequently added to this
// configuration are for optional arguments and won't make the parsing fail
// when the corresponding arguments are not specified.
func (g *GoArgsKwArgs) Optional() *GoArgsKwArgs {
	g.append("|", nil)
	return g
}

// KeywordOnly indicates that all the following parsers are for keyword-only arguments.
//
// All the keyword argument parsers subsequently added to this configuration
// will fail to parse positional arguments; no positional parsers or indicators
// can be added or a parsing failure will result.
func (g *GoArgsKwArgs) KeywordOnly() *GoArgsKwArgs {
	g.append("$", nil)
	return g
}

// AllowUnknownKeywords indicates that unknown keywords shall not fail the parsing.
func (g *GoArgsKwArgs) AllowUnknownKeywords() *GoArgsKwArgs {
	g.unknown = true
	return g
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"

// Buf_simple requests a 1-dimension buffer with no strides.
//
// Flag to be used with Object_GetBuffer.
//
// C API: https://docs.python.org/3/c-api/buffer.html#c.PyBUF_SIMPLE
const Buf_simple = int(C.PyBUF_SIMPLE)

// Buf_writable requests a writable buffer.
//
// Flag to be used with Object_GetBuffer.
//
// C API: https://docs.python.org/3/c-api/buffer.html#c.PyBUF_WRITABLE
const Buf_writable = int(C.PyBUF_WRITABLE)

// Buffer wraps a pointer to a C API Py_buffer.
//
// It is obtained by calling Object_GetBuffer and released by
// Buffer_Release. When used as parameter in a wrapped Go function
// the invocation of the said get/release functions is done by the
// conversion system.
//
// C API: https://docs.python.org/3/c-api/buffer.html#c.Py_buffer
type Buffer struct {
	b *C.Py_buffer
}

// PyClose releases the buffer.
func (b *Buffer) PyClose(Py Py) {
	Py.Buffer_Release(b)
}

// UnsafeSlice returns a byte slice pointing to the wrapped buffer.
//
// The backing storage of the returned slice is owned by the Python
// object that provides the Buffer, it's therefore not garbage
// collected according to the usual Go runtime rules.
//
// Accessing the returned slice is allowed only between calls to
// Object_GetBuffer and Buffer_Release.
//
// UnsafeSlice returns nil if the Buffer was not initialized by
// Object_GetBuffer or after it was released by Buffer_Release.
func (b Buffer) UnsafeSlice() []byte {
	if b.b == nil {
		return nil
	}
	return unsafeSlice(b.b.buf, int(b.b.itemsize*b.b.len))
}

// Object_GetBuffer requests o to fill in b as specified by flags.
//
// C API: https://docs.python.org/3/c-api/buffer.html#c.PyObject_GetBuffer
func (Py Py) Object_GetBuffer(o Object, b *Buffer, flags int) error {
	var buf C.Py_buffer
	if C.PyObject_GetBuffer(o.o, &buf, C.int(flags)) != 0 {
		return Py.GoCatchError()
	}
	b.b = &buf
	return nil
}

// Buffer_Release releases the buffer b.
//
// C API: https://docs.python.org/3/c-api/buffer.html#c.PyBuffer_Release
func (Py Py) Buffer_Release(b *Buffer) {
	if b.b != nil {
		C.PyBuffer_Release(b.b)
		b.b = nil
	}
}

func bufferFromObject(Py Py, o Object, a interface{}) error {
	return Py.Object_GetBuffer(o, a.(*Buffer), Buf_simple)
}

func init() {
	c := GoConvConf{
		TypeOf:     Buffer{},
		FromObject: bufferFromObject,
	}
	atCoreInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(c)
	})
	atCoreFini(func(Py Py) {
		Py.GoDeregisterConversions(c)
	})
}

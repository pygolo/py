/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
//
// int pyBytes_CheckExact(PyObject *o)
// {
//     return PyBytes_CheckExact(o);
// }
import "C"
import "fmt"

// Bytes_Type wraps the Python PyBytes_Type type object.
//
// C API: https://docs.python.org/3/c-api/bytes.html#c.PyBytes_Type
var Bytes_Type = TypeObject{&C.PyBytes_Type}

// Bytes_CheckExact returns true if o is of type Bytes, subtypes excluded.
//
// C API: https://docs.python.org/3/c-api/bytes.html#c.PyBytes_CheckExact
func (Py Py) Bytes_CheckExact(o Object) bool {
	return C.pyBytes_CheckExact(o.o) != 0
}

// bytesToObject converts a Go []byte array to a Python buffer object.
func bytesToObject(Py Py, a interface{}) (Object, error) {
	src := a.([]byte)
	o_buf, err := Py.NewGoBuffer(len(src))
	defer Py.DecRef(o_buf)
	if err != nil {
		return Object{}, err
	}
	var buf Buffer
	err = Py.Object_GetBuffer(o_buf, &buf, Buf_simple)
	defer Py.Buffer_Release(&buf)
	if err != nil {
		return Object{}, err
	}
	dst := buf.UnsafeSlice()
	copy(dst, src)
	return Py.NewRef(o_buf), nil
}

// bytesFromObject converts a Python buffer object to a Go []byte array.
func bytesFromObject(Py Py, o Object, a interface{}) error {
	var buf Buffer
	err := Py.Object_GetBuffer(o, &buf, Buf_simple)
	defer Py.Buffer_Release(&buf)
	if err != nil {
		return fmt.Errorf("could not get buffer from object: %s", err)
	}
	src := buf.UnsafeSlice()
	dst := make([]byte, len(src))
	copy(dst, src)
	switch a := a.(type) {
	case *[]byte:
		*a = dst
	case *interface{}:
		*a = dst
	default:
		return Py.GoErrorConvFromObject(o, a)
	}
	return nil
}

func init() {
	c := GoConvConf{
		TypeOf:     []byte{},
		TypeObject: Bytes_Type,
		ToObject:   bytesToObject,
		FromObject: bytesFromObject,
	}
	atCoreInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(c)
	})
	atCoreFini(func(Py Py) {
		Py.GoDeregisterConversions(c)
	})
}

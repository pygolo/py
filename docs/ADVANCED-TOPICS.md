Here you find additional documentation and special uses of Pygolo, paths
that have been already explored and made available to every user.

[[_TOC_]]

## How the build works

What makes interfacing with the Python interpreter possible is cgo, it
allows a Go program to call C code. The compilation can be customized by
setting `CFLAGS`, `LDFLAGS` and other defines or via environment variables
like `CGO_CFLAGS` and `CGO_LDFLAGS`. Conveniently it also supports
`pkg-config` style settings that are provided by the majority of C
libraries, including Python. For a more comprehensive introduction to cgo,
read [C? Go?  Cgo!](https://go.dev/blog/cgo)

Pygolo uses the `pkg-config` mechanism together with build tags `pyX.Y` so
to support multiple versions of Python, this happens in file
[go-build.go](../go-build.go) here copied in part:

```go
// #cgo py3.6 pkg-config: python-3.6
// #cgo py3.7 pkg-config: python-3.7
// #cgo py3.8 pkg-config: python-3.8-embed
// #cgo py3.9 pkg-config: python-3.9-embed
// #cgo py3.10 pkg-config: python-3.10-embed
// #cgo py3.11 pkg-config: python-3.11-embed
// #cgo !py3.6,!py3.7,!py3.8,!py3.9,!py3.10,!py3.11 pkg-config: python3-embed
import "C"
```

You can see how the `pyX.Y` tag selects the appropriate `pkg-config`
directive for the given version of Python. If no `pyX.Y` tag is provided,
the default `python3` is selected.

Note how for Python >= 3.8 and later the `-embed` variant of the
`pkg-config` settings is used (details at
[cpython#80902](https://github.com/python/cpython/issues/80902)), this
applies also to the default `python3` which is then assumed to be >= 3.8;
therefore if your default Python is < 3.8 you shall specify the
corresponding `pyX.Y` tag.

## Customizing the build

If you have multiple installations of Python, possibly with duplicate X.Y
versions, you may need to adjust the `pkg-config` search path via
`PKG_CONFIG_PATH` environment variable. You can further adjust by defining
`CGO_CFLAGS`, `CGO_LDFLAGS` and the other `CGO_*` environment variables as
needed.

If you want full control of the `CGO_*` flags, you can exclude the Pygolo
`pkg-config` settings by using the `custom_python` build tag. It's
therefore your responsibility to set the appropriate flags for the desired
Python version.

Read the [cgo documentation](https://pkg.go.dev/cmd/cgo) for all the
details on how to modify the behavior of cgo.

## Incorporating Pygolo makefile in your application

If your application uses makefiles, Pygolo provides some goodies for you:

- you get a `PYGOLO_TAGS` variable always containing the right `pyX.Y`
  build tag
- your application targets can benefit from the `PYTHON` parameter
  (see [Makefile parameters](../CONTRIBUTING.md#makefile-parameters))
- you get the `pygolo-diags` target for free
  (see [Makefile targets](../CONTRIBUTING.md#makefile-targets))

First step, include [Python.mk](../Python.mk) in your Makefile. For
example:

```make
include $(shell go list -f '{{.Dir}}' -m gitlab.com/pygolo/py)/Python.mk
```

Be careful, the command above expects that the Pygolo module has been
already downloaded. This is something that the Go compiler does at the
first invocation but, here is the chicken-egg problem, this has not
happened yet in a fresh checkout of your application.

For a correct bootstrap you actually need something like this:

```make
PYGOLO_DIR := $(shell go list -f '{{.Dir}}' -m gitlab.com/pygolo/py)
ifeq ($(PYGOLO_DIR),)
    # If the module is not already in place, download it first
    PYGOLO_DIR := $(shell go get gitlab.com/pygolo/py && go list -f '{{.Dir}}' -m gitlab.com/pygolo/py)
endif
include $(PYGOLO_DIR)/Python.mk
```

Next, adjust your Makefile so to use the `PYGOLO_TAGS` variable.

For example, replace occurrences of `go build .` with `go build -tags
$(PYGOLO_TAGS) .`. If you use [staticcheck](https://staticcheck.dev/),
adjust its invocation similarly. For example `staticcheck -tags
$(PYGOLO_TAGS) .`.

## Deploying to virtual environments

If you are in the necessity of deploying your Go application to a virtual
environment, Pygolo covers your back (actually it's the embedded Python
interpreter that does the magic).

All you have to do is:

1. build your application
2. copy it to the `bin` sub-directory of the virtual environment of choice

Try this from the `pygolo/py` source directory (the actual output will
likely differ):

```shell
$ python3 -m venv .venv
$ go build examples/info.go
$ ./info
Python:
  version: "3.9"
  platform: "macosx-10.9-universal2"
  platlib: "/Library/Python/3.9/site-packages"
$ cp info .venv/bin
$ ./.venv/bin/info
Python:
  version: "3.9"
  platform: "macosx-10.9-universal2"
  platlib: "/Users/cavokz/pygolo/py/.venv/lib/python3.9/site-packages"
```

See how, in the second invocation of `info`, `platlib` points to a
directory inside the virtual environment, this even without having
activated the virtual environment before.

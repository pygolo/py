Go ahead and learn how to embed Python in your application!

[[_TOC_]]

## What does "embed the interpreter" mean?

Your Go application initializes and runs its own private Python runtime
side by side, in the same process, and accesses it as needed.

For more info see the related Python documentation,
[Embedding Python in Another Application](https://docs.python.org/3/extending/embedding.html).

## Download and import Pygolo

Before your application can do any use of Pygolo you need to download it
with the following command (execute it from your application source tree
root):

```shell
$ go get gitlab.com/pygolo/py
```

Then your source code needs to import the package, put the following
import clause in the the relevant Go sources of your application:

```go
import "gitlab.com/pygolo/py"
```

## Initialization and finalization

Your application can now create the Python runtime by calling
`py.GoEmbed()`. Whenever you want it to dismiss the Python runtime,
call `Py.Close()`; this may well happen when your application is
terminating.

Here is a basic application that initializes the runtime at the very
beginning and releases it right before terminating its execution:

```go
package main

import "gitlab.com/pygolo/py"

func main() {
	Py, err := py.GoEmbed()
	defer Py.Close()
	if err != nil {
		panic(err)
	}

	// do something useful
}
```

Note how you need a `py.Py` context before you can invoke any Pygolo
function. Naming such context `Py` will make all your Pygolo calls
resemble their counterparts in the Python C API. Of course you are free to
choose the name that most pleases you.

## Build and run

Now try to build and run the application. Suppose it is in file `main.go`,
the run command is:

```shell
$ go run main.go
```

If the build does not succeed, your system is probably missing some
dependency. Check that `python3-dev`, `pkg-config`, and `gcc` (or their
equivalent on your system) are installed.

`python3-dev` provides the Python C API library that your application will
be eventually linked to.

`pkg-config` is used by cgo to locate the Python C API library and other
needed parameters.

`gcc` is used by cgo to compile and link to the Python C API library.

The command above assumes that your system Python version is 3.8 or newer,
if not or you run on FreeBSD (see https://gitlab.com/pygolo/py/-/issues/14)
or you want to use a specific version, say 3.7, add an extra argument:

```shell
$ go run -tags py3.7 main.go
```

See [Test Matrix](TEST-MATRIX.md) for all the supported versions,
check your system for what are made available to you.

For more details on installing the dependencies, read
[Environment set up](../CONTRIBUTING.md#environment-set-up).

## Creating a list

To get our feet wet, let's create an empty list and append a few items:

```go
// MakeList creates a list containing 'Hello' and 'World'
func MakeList(Py py.Py) (py.Object, error) {
	o, err := Py.List_New(0)
	if err != nil {
		return py.Object{}, err
	}

	err = Py.List_Append(o, "Hello")
	if err != nil {
		Py.DecRef(o)
		return py.Object{}, err
	}

	err = Py.List_Append(o, "World")
	if err != nil {
		Py.DecRef(o)
		return py.Object{}, err
	}

	return o, nil
}
```

You can observe a few things:

- Whereas the Python C API uses `PyObject*`, Pygolo uses `py.Object`.
- `py.Object` is a type and sits in the `py` package.
- Error paths follow the Go best practice, Python exceptions are caught
  and translated into Go errors.
- Objects reference counting is your responsibility as it would be
  with the Python C API. Take the time to read
  [Reference Counts](https://docs.python.org/3/extending/extending.html#reference-counts).
- `Py.List_Append()` accepts a string as item to be added to the list.

## Creating a dictionary

To get also our knees wet, let's create a dictionary:

```go
// MakeDict creates a dictionary containing 'zero', 1, and 'pi'
func MakeDict(Py py.Py) (py.Object, error) {
	o, err := Py.Dict_New()
	defer Py.DecRef(o)
	if err != nil {
		return py.Object{}, err
	}

	err = Py.Dict_SetItem(o, "zero", 0)
	if err != nil {
		return py.Object{}, err
	}

	err = Py.Dict_SetItem(o, 1, "one")
	if err != nil {
		return py.Object{}, err
	}

	err = Py.Dict_SetItem(o, "pi", math.Pi)
	if err != nil {
		return py.Object{}, err
	}

	return Py.NewRef(o), nil
}
```

In addition to the now familiar object and error handling, you can see a
different way of managing the reference counting. By using `defer
Py.DecRef(o)` you delegate to the Go compiler the task of cleaning up the
dictionary, useful to simplify the error paths though it's now your
responsibility to adjust the dictionary reference count with
`Py.NewRef(o)` in case of success.

Note: decrementing the reference count of a zero `py.Object` value is safe,
nothing bad really happens, and can help you in maintaining the flow simple.

Once more, you could notice a free use of strings, integers and floats as
keys and values of the newly added dictionary items.

## Type conversion

It's time to remove the veil from the Pygolo type conversion features.

While the Python C API generally deals with `PyObject*` it occasionally
provides helpers with specific C types to simplify developers' life.

For example, the C API `PyObject_GetAttr()` looks for an attribute in a
given object, the search key being an arbitrary object itself. For the
common case where the key is a string, the `PyObject_GetAttrString()` is
available and takes a C string as key instead of a generic object.

The corresponding Go API `Py.Object_GetAttr()` instead expects a parameter
of type `any`. If the actual parameter type is `py.Object` it is used
as-is, otherwise a conversion is attempted; there is no need of the string
variant.

In general where the C API expects a `PyObject*` the Go API expects a
`any` value, except for the first parameter which typically is the object
the call operates on, for example the list that `Py.List_Append()` appends
to or the object whose attributes `Py.Object_GetAttr()` searches in.

### Conversion helpers

Let's use the conversion helpers directly, this is how you could rewrite
`MakeList`:

```go
// ReMakeList creates a list by converting a Go slice
func ReMakeList(Py py.Py) (py.Object, error) {
	list := []string{
		"Hello",
		"World",
	}
	return Py.GoToObject(list)
}
```

Now the turn of `MakeDict`:

```go
// ReMakeDict creates a dictionary by converting a Go map
func ReMakeDict(Py py.Py) (py.Object, error) {
	dict := map[any]any{
		"zero": 0,
		1:     "one",
		"pi":   math.Pi,
	}
	return Py.GoToObject(dict)
}
```

Realize that the object returned by the conversion is totally detached
from the original Go value it was converted from, changes made to either
are not reflected to the other. In a sense, the conversion happens by
value.

Naturally there is an helper that converts in the opposite direction:

```go
// GetDict converts a dictionary to a Go map
func GetDict(Py py.Py, o py.Object) (map[any]any, error) {
	var dict map[any]any
	err := Py.GoFromObject(o, &dict)
	return dict, err
}
```

If you feed the result of `MakeDict()` to `GetDict()`, you would receive
an exact copy of what you passed to `MakeDict()`, however there is more.

You can further drive the conversion by the type of the target variable,
like this:

```go
func DoDict(Py py.Py) (map[string]bool, error) {
	// map: any => any
	dict := map[any]any{
		"zero": 0,
		1:      "one",
		"pi":   math.Pi,
	}
	o, err := Py.GoToObject(dict)
	defer Py.DecRef(o)
	if err != nil {
		return nil, err
	}

	// map: string => bool
	var dict2 map[string]bool
	err = Py.GoFromObject(o, &dict2)
	return dict2, err
}
```

Note how the type of `dict2` differs from the original `dict`'s one. What
is the content of `dict2` then?

```go
map[string]bool{
	"zero": false,
	"1":    true,
	"pi":   true,
}
```

You can use `Py.GoFromObject()` also to do something like destructuring:

```go
func DesDict(Py py.Py) (map[any]py.Object, error) {
	// map: any => any
	dict := map[any]any{
		"zero": 0,
		1:     "one",
		"pi":   math.Pi,
	}
	o, err := Py.GoToObject(dict)
	defer Py.DecRef(o)
	if err != nil {
		return nil, err
	}

	// map: any => py.Object
	var dict2 map[any]py.Object
	err = Py.GoFromObject(o, &dict2)
	return dict2, err
}
```

Here we convert to/from a dictionary but on the way back we keep the map
values in their object form.

### Reference counting

Given the amount of conversions happening behind your back and the objects
destructuring, what happens to the reference counts?

`Py.GoToObject()` needs to create new objects when regular Go values are
passed to it, for consistency then it increments the reference count of
the objects it instead passes through. Its behaviour is then linear: it
always returns a new reference that you need to dispose after use.

`Py.GoFromObject()` generally returns Go values, they are detached from
the objects they are coming from and therefore there is no need to
increment any reference count. The exception is when `Py.GoFromObject()`
is used for destructuring, in that case some objects are effectively
returned to you and their reference count is then incremented, it's your
responsibility to decrement it when done.

## Error handling

Python makes extensive use of exceptions to report errors. Go's closest
match is panicking and recovering but it's not the idiomatic way of
handling errors, therefore the `py.GoError` type is introduced.

Python exceptions are fetched as soon as they are detected. Exception
type, value and traceback are extracted and stored in textual form in a
`py.GoError` value. The traceback, if present, is augmented with the Go
callers frames in a way that mimics the Python stack trace.

```
Traceback (most recent call last):
  File "test/go-util_test.go", line 57, in gitlab.com/pygolo/py/test.TestCatchError
  File "eval.go", line 58, in gitlab.com/pygolo/py.Py.Eval_EvalCode
  File "test", line 3, in <module>
  File "test", line 2, in foo
```

The `py.GoError` type implements the Go `error` interface, its `Error()`
method returns a string like the one printed by the stand-alone Python
interpreter. Its `Type`, `Value` and `Traceback` members can be accessed
for further examination of the error cause.

## Module importing

Import a Python module in this way:

```go
func main() {
	// interpreter initialization

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	// handle errors

	// do something useful
}
```

Don't forget to DecRef the modules once you are done, defer is your best friend.

Contrary to the stand-alone interpreter, the embedded one does not have
the current directory (`.`) in the module search path, you can add it back
if needed:

```go
func AddCwd(Py py.Py, o_sys py.Object) error {
	o_sys_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_sys_path)
	if err != nil {
		return err
	}

	return Py.List_Append(o_sys_path, ".")
}
```

## Object calling

The C API provides a range of ways to call an object, Pygolo currently
provides just a few.

### `Py.Object_Call()`

This is the most generic way to call an object. It expects three parameters:
the callable object, the positional arguments and the keyword arguments.

Suppose you have this Python function (from [examples/example.py](examples/example.py)):

```python
def hello(name, salutation="Hello"):
	return f"{salutation}, {name}!"
```

You could wrap it in Go as follows (the `o_hello` parameter is the object
of the function above):

```go
func Hello(Py py.Py, o_hello py.Object, name, salutation string) (string, error) {
	o_ret, err := Py.Object_Call(o_hello, py.GoArgs{name}, py.GoKwArgs{"salutation": salutation})
	defer Py.DecRef(o_ret)
	if err != nil {
		return "", err
	}

	var ret string
	err = Py.GoFromObject(o_ret, &ret)
	return ret, err
}
```

While the C API requires a tuple for the positional arguments and a
dictionary for the keyword ones, the corresponding Go API accepts also
`py.GoArgs` and `py.GoKwArgs` for relying on the type conversion helpers.

`py.GoArgs` wraps a slice of `any` values, `py.GoKwArgs` a map of `any` values
keyed by `string`. They exist to make call sites more terse, easier to read
and more maintainable over time.

### `Py.Object_CallFunction()`

This variant helps when the object call does not use any keyword argument.
You are dispensed from putting the parameters into a `py.GoArgs`, simply
pass them as-is. Because it is variadic, the number of parameters after
the object is variable and can even be zero.

```go
func HelloFun(Py py.Py, o_hello py.Object, name string) (string, error) {
	o_ret, err := Py.Object_CallFunction(o_hello, name)
	defer Py.DecRef(o_ret)
	if err != nil {
		return "", err
	}

	var ret string
	err = Py.GoFromObject(o_ret, &ret)
	return ret, err
}
```

Compare with the previous call to `Call_Object`: no `py.GoArgs` or
`py.GoKwArgs` are in sight, the parameter `name` is straight after the
`o_hello` object.

### `Py.Object_CallMethod()`

This last variant is for calling an object's method, when you would first
search the desired callable among the attributes of the object and then
call it.

Suppose you want to rewrite the following Python function:

```python
def my_count(o, s):
	"""Count the occurrences of s in the string representation of o"""
	return str(o).count(s)
```

In Go it would be:

```go
func MyCount(Py py.Py, o py.Object, s string) (int, error) {
	// x = str(o)
	o_str, err := Py.Object_Str(o)
	defer Py.DecRef(o_str)
	if err != nil {
		return 0, err
	}

	// x.count(s)
	o_count, err := Py.Object_CallMethod(o_str, "count", s)
	defer Py.DecRef(o_count)
	if err != nil {
		return 0, err
	}

	var count int
	err = Py.GoFromObject(o_count, &count)
	return count, err
}
```

## Structs

Type conversion supports also structs, only public members (fields and methods)
can be exported. By default all the public members are exported using their Go names.

You have two ways to deviate from the above default: struct tags and `GoStruct`.

### Struct tags

Struct tags are data oriented and are commonly used to marshal Go structs to/from
different formats, like json or yaml. To use them you need to own the struct
definition, you cannot add tags to structs defined in packages not in your control.
Moreover they can be used only for fields, methods do not have struct tags.

In this example you see how to use struct tags to achieve the following result:

 - field `S` is exported with its name, `S`
 - field `B` is exported with name `b`
 - field `I` is not exported despite it has a capitalized name
 - field `f` is not exported, its name starts with a lowercase letter

```go
func toPythonStruct(Py py.Py) (py.Object, error) {
	type MyStruct struct {
		S string
		B bool   `python:"b"`
		I int    `python:",omit"`
		f float64
	}

	s := MyStruct{"this is a string", true, 42, 3.14}
	return Py.GoToObject(s)
}
```

### GoStruct

With `GoStruct` you get full control over the export. In this example
you get the same effect of the above struct tags:

```go
func toPythonStruct(Py py.Py) (py.Object, error) {
	type MyStruct struct {
		S string
		B bool
		I int
		f float64
	}

	s := py.GoStruct{
		Struct: MyStruct{"this is a string", true, 42, 3.14},
	}

	s.Rename("B", "b")
	s.Exclude("I")

	return Py.GoToObject(s)
}
```

You could also do the same in this other way (note the use of `OmitByDefault` to
invert the default):

```go
func toPythonStruct(Py py.Py) (py.Object, error) {
	type MyStruct struct {
		S string
		B bool
		I int
		f float64
	}

	s := py.GoStruct{
		Struct:        MyStruct{"this is a string", true, 42, 3.14},
		OmitByDefault: true,
	}

	s.Include("S")
	s.Rename("B", "b")

	return Py.GoToObject(s)
}
```

### Registering the struct type

Each time you convert a struct to/from Python, the conversion system uses
the Go reflection to reason about the member names and types in addition
to struct tags and `GoStruct` customizations. If you don't want to pay
such cost on every conversion, you can register the type upfront.

See here how to register a new Python type (this example uses `GoStruct`
but it would work with struct tags as well):

```go
func toPythonStruct(Py py.Py) (py.Object, error) {
	type MyStruct struct {
		S string
		B bool
		I int
		f float64
	}

	s := py.GoStruct{
		Struct:        MyStruct{},
		OmitByDefault: true,
	}

	s.Include("S")
	s.Rename("B", "b")

	err := Py.GoRegisterStruct(s)
	if err != nil {
		return py.Object{}, fmt.Errorf("could not register type: %w", err)
	}

	return Py.GoToObject(MyStruct{"this is a string", true, 42, 3.14})
}
```

Registering a struct and then converting it just once does not save anything
but if you have to execute conversions of many values then you may want to
consider this option.

There is a caveat though: you need to invoke `Py.GoRegisterStruct` carefully.
The rule is, first register all your struct types than do all the conversions
you want. Not having to synchronize `Py.GoRegisterStruct` with the rest of the
conversion systems gives you maximum freedom of conversion later.

Another side note, it's not really important what's the struct value passed
during the registration, what matters is only its type. Once you register a
struct with `GoStruct`, it's not necessary to use `GoStruct` for the later
conversions, the struct type has been identified and registered in the
conversion system.

An additional benefit of registering a struct type is that its Python type
will be always the same while in the other case every converted object will
have identical but different (to the eyes of the interpreter) types. In essence,
with registered struct types if `o1` and `o2` are of the same Go struct type
then their type is the same also in Python (`type(o1) is type(o2)`) otherwise no.

### Instantiating from Python

Once your converted object is made available to the Python interpreter also
its type is accessible from there. This means that the Python interpreter could,
and by default it can, instantiate it.

```python
s = MyStruct(S="this is a string", b=true)
```

It's not possible to initialize the fields that are not exported to Python
therefore they will be initialized to their zero value.

If you want to prevent the initialization from Python then you need to register
the struct type using `nil` as value, like this:

```go
func toPythonStruct(Py py.Py) (py.Object, error) {
	type MyStruct struct {
		S string
		B bool
		I int
		f float64
	}

	s := py.GoStruct{
		Struct:        (*MyStruct)(nil),
		OmitByDefault: true,
	}

	s.Include("S")
	s.Rename("B", "b")

	err := Py.GoRegisterStruct(s)
	if err != nil {
		return py.Object{}, fmt.Errorf("could not register type: %w", err)
	}

	return Py.GoToObject(MyStruct{"this is a string", true, 42, 3.14})
}
```

Note the cast to `*MyStruct`, without that Go reflection does not have a
struct type to work with (`nil` by itself has no type).

### Converting by value and by pointer

Registering a struct by value or by pointer does not make any difference,
except for the `nil` case explained above; the end result is anyway that
both conversions by value and by pointer are possible. However what does
that actually mean?

By value means that the Go value and the Python objects are totally
disconnected, changes made to one are not reflected to the other.

By pointer means that the Python object holds a pointer to the Go value
that it was converted from, any change made from either Go or Python
is visible to both the runtimes. Ensure to synchronize the struct
internals as needed by your application concurrency.

### Converting from Python ####

Structs can be converted from Python as well, what makes a substantial
difference is whether or not the Python object is of the same type of the Go
struct. For example, it was converted from a Go value of that same struct type
or it's an instantiation of the Python type corresponding to that struct.

Let's consider first the case where the Python object is totally unrelated
to the Go struct. In this situation it's only possible the conversion by value,
every exposed field of the Go struct will be searched as attribute in the Python
object in accordance to the struct tags and `GoStruct` customizations.

For example:

```go
// The o object is of this class:
//
//   class MyClass:
//     S = "python string"
//     b = True
//     I = 42
//     f = 3.14
//     N = None
func fromPythonStruct(Py py.Py, o py.Object) error {
	var s MyStruct
	err := Py.GoFromObject(o, &s)
	if err != nil {
		return fmt.Errorf("Could not convert from Python: %w", err)
	}

	if (s != MyStruct{S: "python string", B: true, I: 0, f: 0}) {
		return fmt.Errorf("Pygolo is bugged!")
	}

	return nil
}
```

Note how the attributes `I` and `f` of `o` are not converted to the Go
struct, also `N` is ignored.

In the case the Go struct and the Python object are type-related there
isn't a real conversion, it's more an extraction. By value, the Go value
wrapped by the Python object is copied into the destination Go value.
By pointer, the destination Go value pointer will point directly to the
Go value wrapped in the Python object. That's because the Python object
it's just an envelop around the Go value with added machinery to make
it accessible from Python.

## Sharing a file

You may want to access a file from Go and Python at the same time.

### Read

Say you have an `*os.File` value, you read it from Python in this way:

```go
func ReadFromPython(Py py.Py, file *os.File) error {
	// get a Python file object for reading
	o_file, err := Py.GoToObject(file)
	defer Py.DecRef(o_file)
	if err != nil {
		return fmt.Errorf("could not get the file: %w", err)
	}

	// read all the content in lines
	o_lines, err := Py.Object_CallMethod(o_file, "readlines")
	defer Py.DecRef(o_lines)
	if err != nil {
		return fmt.Errorf("could not read the content: %w", err)
	}

	// do something with o_lines

	// it's good practice to close the file after use, the interpreter
	// would eventually do it as well but as later as it wants
	err = Py.Object_CallMethod(o_file, "close")
	if err != nil {
		return fmt.Errorf("could not close the file: %w", err)
	}
	return nil
}
```

The so created `o_file` object and the original `file` value share the
underlying OS file representation but their lifetime is independent, each
runtime has the responsibility of closing its own part and only when both
have done so the underlying OS file is eventually closed.

Similarly you can read a Python file from Go:

```go
func ReadFromGo(Py py.Py, o_file py.Object) error {
	var file *os.File
	err := Py.GoFromObject(o_file, &file)
	if err != nil {
		return fmt.Errorf("could not get the file: %w", err)
	}

	// read all the content in lines
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	err = scanner.Err()
	if err != nil {
		return fmt.Errorf("could not read the content: %w", err)
	}

	// do something with lines

	// close the file after use
	err = file.Close()
	if err != nil {
		return fmt.Errorf("could not close the file: %w", err)
	}
	return nil
}
```

Sharing the underlying OS file representation has an implication: the
current read/write offset within the file is shared in the two runtimes,
any change made from one is immediately visible to the other.

If this is not what you want then you can actually open the same file from
the two runtimes, without passing through `GoToObject` or `GoFromObject`,
each runtime would truly get an independent OS representation with its own
current read/write offset.

### Write

To obtain a writable Python file object you need to use a `py.GoOSFile`
value, like this:

```go
func WriteFromPython(Py py.Py, file *os.File) error {
	// get a Python file object for writing
	o_file, err := Py.GoToObject(py.GoOSFile{File: file, Mode: "w"})
	defer Py.DecRef(o_file)
	if err != nil {
		return fmt.Errorf("could not get the file: %w", err)
	}

	// write something
	o_ret, err := Py.Object_CallMethod(o_file, "write", "Hello, World!")
	defer Py.DecRef(o_ret)
	if err != nil {
		return fmt.Errorf("could not write the content: %w", err)
	}

	// close after use
	err = Py.Object_CallMethod(o_file, "close")
	if err != nil {
		return fmt.Errorf("could not close the file: %w", err)
	}
	return nil
}
```

Remember that each runtime has its own write buffering, you have to
properly sync (Go) and flush (Python) to ensure that the written content
becomes visible to the other runtime. This applies also to interleaved
writes from both the runtimes.

### Pipe

You may want to establish a file-like communication channel between the
two runtimes, with a pipe you can:

```go
// PipeToPython creates a pipe that's written from Go and read from Python
func PipeToPython(Py py.Py) (py.Object, *os.File, error) {
	r, w, err := os.Pipe()
	if err != nil {
		return py.Object{}, nil, fmt.Errorf("could not create the pipe: %w", err)
	}

	// get a Python file object for reading
	o_r, err := Py.GoToObject(r)
	if err != nil {
		// error, destroy the pipe
		r.Close()
		w.Close()
		return fmt.Errorf("could not get the read end: %w", err)
	}

	// won't read the pipe from Go, let's close it
	r.Close()
	return o_r, w, nil
}
```

Writes made from Go are immediately visible to the other end, synching a
pipe is even an invalid operation, whereas for writes from Python you need
to flush as for regular files.

## Concurrency

Your Go application, which is likely also a concurrent one, needs to
access the Python interpreter with some care. The interpreter is accessed
through the `py.Py` context, such context cannot be shared across goroutines.

Play safe, confine the interpreter to a goroutine. Do something along these lines:

```go
func StartInterpreter() (chan<- func(py.Py), error) {
	var requests chan func(py.Py)
	ret := make(chan error)

	go func() {
		Py, err := py.GoEmbed()
		defer Py.Close()

		if err != nil {
			ret <- err
			return
		}

		requests = make(chan func(py.Py))
		ret <- nil

		for req := range requests {
			req(Py)
		}
	}()

	return requests, <-ret
}
```

Every time you want to execute some Python code, use this pattern:

```go
func usePython(requests chan<- func(py.Py)) {
	done := make(chan struct{})
	requests <- func(Py py.Py) {
		defer close(done)

		// do Python stuff here
	}
	<- done
}
```

The `py.Py` context as returned by `py.GoEmbed` has access to the
interpreter by default. You may occasionally relinquish the access, use
`Py.GoLeavePython` to _exit_ from the interpreter and `Py.GoEnterPython`
to _enter_ in it.

Take the time to read
[Thread State and the Global Interpreter Lock](https://docs.python.org/3/c-api/init.html#thread-state-and-the-global-interpreter-lock),
keep in mind that in such context "thread" is equivalent to "goroutine".

## Conclusion

You should now have a good grasp of how to embed the Python interpreter in
your Go application and hopefully acquired a taste for doing so with Pygolo.

Feel free to explore the [Pygolo API](https://pkg.go.dev/gitlab.com/pygolo/py#pkg-index),
don't forget to read once also the common functions mentioned in the examples above.

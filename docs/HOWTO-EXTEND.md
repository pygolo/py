Here you will get familiar with using Go to extend Python.

[[_TOC_]]

## What does "extend the interpreter" mean?

You write a loadable component in Go that the Python interpreter can use
like any other native module.

For more info see the related Python documentation,
[Extending Python with C or C++](https://docs.python.org/3/extending/extending.html).

## Basics

This document assumes you have a working environment and that you are
familiar with the basics. If this is not the case, please read
[How to Embed](HOWTO-EMBED.md).

## Anatomy of an extension

The Python interpreter defines a protocol for loading extensions,
therefore every extension needs to have a given structure.

This is the backbone of an extension written in Go:

```go
package main

import (
	"C"
	"unsafe"
	"gitlab.com/pygolo/py"
)

func myext(Py py.Py, m py.Object) error {
	// here goes the extension initialization code
	return nil
}

//export PyInit_myext
func PyInit_myext() unsafe.Pointer {
	return py.GoExtend(myext)
}

// required by cgo but unused
func main() {
}
```

Let's get a few details straight.

First, like every regular Go application, the package shall be named
`main`. Similarly there shall be the `main` function, albeit it's never
invoked and serve no purpose but silencing a linker error.

The key function, the extension entry point searched by the Python
interpreter, is `PyInit_<name>`. The extension in the example is named
`myext` therefore the entry point is `PyInit_myext`.

Additionally, the entry point needs to be reachable by the Python
interpreter as a regular C function; that's why you need the `//export
PyInit_myext` comment (no space between `//` and `export`) right before
the entry point function. To make it effective, you also need to import
the `C` package.

The sole purpose of the entry point is to return to the Python interpreter
a specification of how to initialize the extension module. You do that by
calling `py.GoExtend` and passing it the initialization function, which
name and location are a choice of yours.

Given that all the action is in function `myext`, from now on all the
examples will omit the above boilerplate code.

## Build and import

Python extensions are dynamic libraries, not plain executables; let's see
how to build one for our example.

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
```

A little explanation is due.

`-tags py_ext` is to select the right compilation and linking flags for
the Python C API library.

`-buildmode=c-shared` is to instruct the Go compiler to build a
dynamic library instead of a regular executable.

`-o myext.so` is to name the extension so that the Python interpreter can
find it.

Lastly, `myext.go` is the extension source file. As for any Go package, it
can also be `.` or any other directory according to your sources tree.

Now, try the extension.

```shell
$ python3
Python 3.9.6 (default, Oct  1 2023, 17:38:11)
[Clang 15.0.0 (clang-1500.0.40.1)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import myext
>>> myext.__name__
'myext'
>>> myext.__file__
'/tmp/myext.so'
>>> myext.__doc__
>>>
```

It seems all right, let's see how to make it more useful.

## Initialization function

The initialization function `myext` receives two arguments of type
`py.Py` and `py.Object`, returns an `error`.

The `py.Py` value is the context needed to access the Python interpreter,
`py.Object` is the module that eventually the Python interpreter will use
to interact with the extension.

Return `nil` to signal a success or non-nil to abort the module import and
raise an exception.

Let's add a docstring to our extension:

```go
func myext(Py py.Py, m py.Object) error {
	if err := Py.Module_SetDocString(m, "My Go extension"); err != nil {
		return err
	}
	return nil
}
```

Check the result:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.__doc__)'
My Go extension
```

You can also export strings, numbers, functions, etc.

```go
func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "hello", "Hello, World!"); err != nil {
		return err
	}
	if err := Py.Object_SetAttr(m, "the_answer", 42); err != nil {
		return err
	}
	return nil
}
```

Try it:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.hello); print("The answer is", myext.the_answer)'
Hello, World!
The answer is 42
```

## Plain functions

The most simple function that can be exported to Python is one that does
not interact with the interpreter. Exporting it is not different from
exporting any other Go value:

```go
// inc returns n incremented by one.
func inc(n int) int {
	return n + 1
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "inc", inc); err != nil {
		return err
	}
	return nil
}
```

Sure enough it works:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.inc(99))'
100
```

It knows its name and signature:

```shell
$ python3 -c 'import myext; print(myext.inc.__name__)'
inc
$ python3 -c 'import myext; print(myext.inc.__doc__)'
func(int) int
```

It does check the number of arguments:

```shell
$ python3 -c 'import myext; print(myext.inc())'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: takes 1 positional argument but 0 were given
$ python3 -c 'import myext; print(myext.inc(0, 1))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: takes 1 positional argument but 2 were given
```

It also checks their type:

```shell
$ python3 -c 'import myext; print(myext.inc("text"))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: arg #0: cannot convert Python str to Go int
$ python3 -c 'import myext; print(myext.inc(True))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: arg #0: cannot convert Python bool to Go int
```

As you could expect, any number of arguments and return values is
supported:

```go
// inc2 returns a and b incremented by one.
func inc2(a, b int) (int, int) {
	return a + 1, b + 1
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "inc2", inc2); err != nil {
		return err
	}
	return nil
}
```

No surprises, multiple values are returned in a tuple:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.inc2(3, 5))'
(4, 6)
```

## Variadic functions

Talking of multiple arguments, you can export variadic functions as well:

```go
// sum returns the sum of the passed numbers.
func sum(numbers ...int) int {
	sum := 0
	for _, n := range numbers {
		sum += n
	}
	return sum
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "sum", sum); err != nil {
		return err
	}
	return nil
}

```

As expected:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.sum())'
0
$ python3 -c 'import myext; print(myext.sum(1))'
1
$ python3 -c 'import myext; print(myext.sum(1, 3))'
4
$ python3 -c 'import myext; print(myext.sum(1, 3, 5))'
9
```

Type checks are always in place:

```shell
$ python3 -c 'import myext; print(myext.sum(1, 3, "five"))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: arg #2: cannot convert Python str to Go int
```

## Errors and panics

Any good Go function should be able to return errors and panic as needed.
See how it looks like.

```go
// sum2 returns the sum of a and b or an error if the sum overflows.
func sum2(a, b int32) (int32, error) {
	if b > 0 {
		if a > math.MaxInt32-b {
			return 0, fmt.Errorf("integer overflow")
		}
	} else {
		if a < math.MinInt32-b {
			return 0, fmt.Errorf("integer overflow")
		}
	}
	return a + b, nil
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "sum2", sum2); err != nil {
		return err
	}
	return nil
}
```

As promised:

```shell
$ python3 -c 'import myext; print(myext.sum2(2147483327, -2147483327))'
0
$ python3 -c 'import myext; print(myext.sum2(2147483327, 2147483327))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: integer overflow
```

Let's try with some panic:

```go
// div returns the integer division of a by b.
func div(a, b int) int {
	return a / b
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "div", div); err != nil {
		return err
	}
	return nil
}
```

Once again, everything seems in order:

```shell
$ go build -tags py_ext -buildmode=c-shared -o myext.so myext.go
$ python3 -c 'import myext; print(myext.div(9, 3))'
3
$ python3 -c 'import myext; print(myext.div(9, 0))'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
RuntimeError: panic: runtime error: integer divide by zero
```

## Accessing the interpreter

Those exported above are all "pure" Go functions, nothing relates them to
the Python interpreter. As such they can also be busy for long time or block
without affecting the interpreter, they run _outside_ the interpreter.

You may also want to export functions that actually interact with the
interpreter, for those you need the `py.Py` context. Just add it to the
function parameters and export as usual:

```python
// pyappend appends item to list.
func pyappend(Py py.Py, list py.Object, item any) error {
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	return Py.List_Append(list, item)
}

func myext(Py py.Py, m py.Object) error {
	if err := Py.Object_SetAttr(m, "append", pyappend); err != nil {
		return err
	}
	return nil
}
```

Observe how you first _enter_ the interpreter, then use it, and eventually
_leave_ it. You are not allowed to access the interpreter out of a
`GoEnterPython`-`GoLeavePython` block.

Depending on the interpreter implementation (ex. if it has the GIL -
global interpreter lock), entering the interpreter could exclude others to
come in; it's worth considering to do slow or blocking operations only out
of the interpreter.

Given that having access to the interpreter does not necessarily exclude
others to have it as well, it's therefore only your responsibility to
regulate concurrent access to Go structures in the exported functions.

If your function is quick you may want to execute it right _inside_ the
interpreter. Use `py.GoFunction` to customize the function exporting, set
`InterpreterAccess`.

This is the example above rewritten to run `pyappend` inside the
interpreter:

```python
func pyappend(Py py.Py, list py.Object, item any) error {
	return Py.List_Append(list, item)
}

func myext(Py py.Py, m py.Object) error {
	// execute pyappend from inside the interpreter
	pyappend := py.GoFunction{Fn: pyappend, InterpreterAccess: true}
	if err := Py.Object_SetAttr(m, "append", pyappend); err != nil {
		return err
	}
	return nil
}
```

Note how the enter/leave calls are gone from `pyappend`, they are now
implicit.

## Concurrency

Let's complicate things by exporting a function that delays the call of a
Python function, like this:

```python
import myext
import time

# execute in 1.5s
myext.later(1.5, lambda: print("done"))
# wait 2s
time.sleep(2)
```

`later` starts a goroutine and returns immediately, the goroutine instead
waits for `delay` seconds, it invokes the callback then exits.

Something like this:

```go
// later schedules a delayed execution and returns immediately.
func later(Py py.Py, delay float64, callback py.Object) {
	go func() {
		// wait as needed
		time.Sleep(time.Duration(delay*1000) * time.Millisecond)

		// get access to the interpreter, leave it after goroutine exit
		Py.GoEnterPython()
		defer Py.GoLeavePython()

		// call the user function, discard its return value
		ret, err := Py.Object_CallFunction(callback)
		defer Py.DecRef(ret)
		if err != nil {
			panic(err)
		}
	}()
}
```

A problem with this implementation is that `py.Py` context is used across
different goroutines, it may work but it would not be reliable. It has to
do with the Python interpreter being thread-centric and goroutines
migrating from one thread to another.

To overcome this limitation, use `Py.GoNewFlow`. It shall be invoked from
within the new goroutine, it essentially forks off the `py.Py` context and
pins the goroutine to its own OS thread.

`later` then becomes as follows:

```go
func later(Py py.Py, delay float64, callable py.Object) {
	go func() {
		// fork off the Python context, release it at goroutine exit
		Py, err := Py.GoNewFlow(nil)
		defer Py.Close()
		if err != nil {
			panic(err)
		}

		time.Sleep(time.Duration(delay*1000) * time.Millisecond)

		Py.GoEnterPython()
		defer Py.GoLeavePython()

		ret, err := Py.Object_CallFunction(callable)
		defer Py.DecRef(ret)
		if err != nil {
			panic(err)
		}
	}()
}
```

Something subtle is still wrong though, it has to do with the `callable`
object lifetime.

When the interpreter invokes `later`, it passes a so called _borrowed
reference_ to `callable` which lasts only until `later` returns. This is a
problem because by that time also the spawned goroutine will have a
reference to `callable` and it could become invalid by the time it wants
to use it.

Python knows nothing of the Go runtime, goroutines and closures. It only
knows that once `later` returns it is free to release the `callable`
object if nothing else is holding it. Therefore it's necessary to inform
Python that the spawned goroutine is holding a reference to `caller`,
reference that needs to be released once the goroutine has done its job.

```go
func later(Py py.Py, delay float64, callable py.Object) error {
	Py.GoEnterPython()
	defer Py.GoLeavePython()

	// hold `callable` for later use
	Py.IncRef(callable)

	ret := make(chan error)
	go func() {
		Py, err := Py.GoNewFlow(nil)
		defer Py.Close()
        if ret <- err; err != nil {
			return
		}

		time.Sleep(time.Duration(delay*1000) * time.Millisecond)

		Py.GoEnterPython()
		defer Py.GoLeavePython()

		// unhold `callable` after goroutine exit
		defer Py.DecRef(callable)

		ret, err := Py.Object_CallFunction(callable)
		defer Py.DecRef(ret)
		if err != nil {
			panic(err)
		}
	}()

	// wait for the spawned goroutine to start, unhold `callable` if something went wrong
	if err := <-ret; err != nil {
		Py.DecRef(callable)
		return err
	}

	return nil
}
```

Note how we need to enter the interpreter before calling `Py.IncRef` and
how `later` needs to decrement the reference count of `callable` in case
of error, the goroutine could not do that by itself exactly because it's
without the needed valid `py.Py` context.

Albeit correct this is not straightforward to read and it's fragile to
maintain; not to talk of if you need to start multiple goroutines.  In
the overall, it's a complicated matter to do something as simple as
starting a goroutine.

That's why `GoClosure` and `GoClosureBuddy` are made available.

## GoClosure and its buddy

Purpose of `GoClosure` and `GoClosureBuddy` is to handle the reference
counting responsibilities across goroutines.

Let's see how you can use them by rewriting `later`.

```go
func later(Py py.Py, delay float64, callable py.Object) {
	// 1. create the closure buddy
	buddy := Py.GoNewClosureBuddy()
	defer buddy.Close()

	Py.GoEnterPython()
	defer Py.GoLeavePython()

	go func(closure *py.GoClosure) {
		// 3. pass closure to GoNewFlow
		Py, err := Py.GoNewFlow(closure)
		defer Py.Close()
		if err != nil {
			panic(err)
		}

		time.Sleep(time.Duration(delay*1000) * time.Millisecond)

		Py.GoEnterPython()
		defer Py.GoLeavePython()

		ret, err := Py.Object_CallFunction(callable)
		defer Py.DecRef(ret)
		if err != nil {
			panic(err)
		}
	// 2. create the closure
	}(buddy.Capture(callable))
}
```

You can see that all the reference counting is gone, the borrowed
referencing is effectively extended to the spawned goroutine.

This is achieved by:

1. creating the _buddy_ in the outer function
2. creating a closure that captures the wanted objects (outer function)
3. passing the closure to `GoNewFlow` (spawned goroutine)

The reference counts increment is done by `buddy.Capture` at step 2, the
decrement is done by `Close` of either the newly created flow or the
buddy, depending on whether such new flow was successfully created or not.

You can use the same buddy for capturing arguments for multiple goroutines
by repeating steps 2 and 3.

Let's see how by implementing a `distribute` function that calls a Python
callable passing an argument from a list, each call made by a separate
goroutine.

```python
import myext
import time

# call `print("one")`, `print("two")`, etc..
myext.distribute(print, "one", "two", "three")
# wait for the goroutines to complete
time.sleep(3)
```

Go implementation of `distribute`:

```go
// distribute schedules the invocations of callable with each argument
// and returns immediately.
func distribute(Py py.Py, callable py.Object, args ...py.Object) {
	buddy := Py.GoNewClosureBuddy()
	defer buddy.Close()

	Py.GoEnterPython()
	defer Py.GoLeavePython()

	for _, arg := range args {
		go func(closure *py.GoClosure, arg py.Object) {
			Py, err := Py.GoNewFlow(closure)
			defer Py.Close()
			if err != nil {
				panic(err)
			}

			Py.GoEnterPython()
			defer Py.GoLeavePython()

			ret, err := Py.Object_CallFunction(callable, arg)
			defer Py.DecRef(ret)
			if err != nil {
				panic(err)
			}
		}(buddy.Capture(callable, arg), arg)
	}
}
```

`distributed` is variadic, `args` is the slice of the arguments to be
passed to each invocation of `callable`. Each goroutine receives its own
`arg` in addition to the closure.

Note how the structure is only marginally complicated by the creation of
multiple goroutines.

It's important to mention though that `buddy.Close()` needs to wait for
all the closures to be either accepted (the new flow creation succeeded)
or rejected (the new flow creation failed). Who does this job of accepting
or rejecting a closure? `GoNewFlow` itself before returning, if a closure
was passed as argument.

## Conclusion

It's unlikely that you'll choose Go to just implement primitives like
`later` or `distribute` but they are good examples to show how to keep the
interaction of goroutines and Python objects reference counting under
control.

Never the less, making a goroutine access the interpreter comes with some
price in added complexity; be sure that the benefits are worth the price.

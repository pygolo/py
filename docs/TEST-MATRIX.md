# Test Matrix

Tempted to configure such a test matrix where many versions of Python and
Go are all combined, we eventually opted for checking only the versions as
actually used by some common platforms.

```json:table
{
	"fields": [
		{"key": "go", "label": "↓Go Python→"},
		{"key": "3.13"},
		{"key": "3.12"},
		{"key": "3.11"},
		{"key": "3.10"},
		{"key": "3.9"},
		{"key": "3.8"},
		{"key": "3.7"},
		{"key": "3.6"}
	],
	"items": [
		{"go": "1.24", "3.6": "openSUSE Leap 15"},
		{"go": "1.23", "3.13": "Fedora 41", "3.11": "OpenBSD 7", "3.9": "macOS 14 xc-15"},
		{"go": "1.22", "3.12": "Alpine 3.20, Fedora 39, Fedora 40, Ubuntu 24.04", "3.11": "NetBSD 10", "3.9": "RHEL UBI 9", "3.6": "RHEL UBI 8"},
		{"go": "1.21", "3.13": "Windows", "3.11": "Alpine 3.19, Fedora 38, FreeBSD 14", "3.6": "SLE BCI 15.4"},
		{"go": "1.20", "3.11": "Alpine 3.18, Fedora 37", "3.9": "macOS 13 xc-14"},
		{"go": "1.19", "3.11": "Debian 12", "3.10": "Alpine 3.17, Fedora 36"},
		{"go": "1.18", "3.10": "Alpine 3.16, Ubuntu 22.04"},
		{"go": "1.17", "3.9": "Alpine 3.15"},
		{"go": "1.16", "3.10": "Fedora 35"},
		{"go": "1.15", "3.9": "Debian 11"},
		{"go": "1.13", "3.8": "Ubuntu 20.04"},
		{"go": "1.11", "3.7": "Debian 10"},
		{"go": "1.10", "3.6": "Ubuntu 18.04"}
	],
	"caption": "Versions and platforms"
}
```

Don't miss [.gitlab-ci.yml](.gitlab-ci.yml), where all the magic is configured.

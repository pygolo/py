/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Show how to control the Python interpreter stdin/stdout/stderr from Go.
package main

import (
	"fmt"
	"io"
	"os"
	"sync"

	"gitlab.com/pygolo/py"
)

func exit_if_error(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func AddCwd(Py py.Py) error {
	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	if err != nil {
		return err
	}

	o_sys_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_sys_path)
	if err != nil {
		return err
	}

	return Py.List_Append(o_sys_path, ".")
}

type Console struct {
	StdinR  *os.File
	StdinW  *os.File
	StdoutR *os.File
	StdoutW *os.File
	StderrR *os.File
	StderrW *os.File
}

func NewConsole() (c Console, err error) {
	stdinR, stdinW, err := os.Pipe()
	if err != nil {
		err = fmt.Errorf("stdin pipe: %w", err)
		return
	}

	stdoutR, stdoutW, err := os.Pipe()
	if err != nil {
		err = fmt.Errorf("stdout pipe: %w", err)
		return
	}

	stderrR, stderrW, err := os.Pipe()
	if err != nil {
		err = fmt.Errorf("stderr pipe: %w", err)
		return
	}

	return Console{
		StdinR:  stdinR,
		StdinW:  stdinW,
		StdoutR: stdoutR,
		StdoutW: stdoutW,
		StderrR: stderrR,
		StderrW: stderrW,
	}, nil
}

func (c Console) Close() {
	exit_if_error(c.StdinW.Close())
	exit_if_error(c.StdoutR.Close())
	exit_if_error(c.StderrR.Close())
}

func (c Console) Set(Py py.Py) error {
	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	if err != nil {
		return err
	}

	o_warnings, err := Py.Import_Import("warnings")
	defer Py.DecRef(o_warnings)
	if err != nil {
		return err
	}

	for _, entry := range []struct {
		file *os.File
		name string
		mode string
	}{
		{c.StdinR, "stdin", "r"},
		{c.StdoutW, "stdout", "w"},
		{c.StderrW, "stderr", "w"},
	} {
		o_file, err := Py.Object_GetAttr(o_sys, entry.name)
		defer Py.DecRef(o_file)
		if err != nil {
			return fmt.Errorf("cannot get \"%s\": %w", entry.name, err)
		}

		o_ret, err := Py.Object_CallMethod(o_file, "close")
		Py.DecRef(o_ret)
		if err != nil {
			return fmt.Errorf("cannot close \"%s\": %w", entry.name, err)
		}

		o_file, err = Py.GoToObject(py.GoOSFile{File: entry.file, Mode: entry.mode})
		defer Py.DecRef(o_file)
		if err != nil {
			return err
		}

		err = Py.Object_SetAttr(o_sys, entry.name, o_file)
		if err != nil {
			return fmt.Errorf("cannot set \"%s\": %w", entry.name, err)
		}

		err = entry.file.Close()
		if err != nil {
			return fmt.Errorf("cannot close \"%s\": %w", entry.name, err)
		}

		o_fd, err := Py.Object_CallMethod(o_file, "fileno")
		defer Py.DecRef(o_fd)
		if err != nil {
			return fmt.Errorf("cannot get fileno of \"%s\": %w", entry.name, err)
		}

		var fd int
		err = Py.GoFromObject(o_fd, &fd)
		if err != nil {
			return fmt.Errorf("cannot convert fileno of \"%s\": %w", entry.name, err)
		}

		message := fmt.Sprintf("unclosed file .* name=%d mode='%s'", fd, entry.mode)
		o_ret, err = Py.Object_CallMethod(o_warnings, "filterwarnings", "ignore", message, py.Exc_ResourceWarning)
		defer Py.DecRef(o_ret)
		if err != nil {
			return err
		}
	}

	return nil
}

func main() {
	con, err := NewConsole()
	defer con.Close()
	exit_if_error(err)

	wg := sync.WaitGroup{}
	defer wg.Wait()

	Py, err := py.GoEmbed()
	defer Py.Close()
	exit_if_error(err)

	err = AddCwd(Py)
	exit_if_error(err)

	err = con.Set(Py)
	exit_if_error(err)

	// let's just connect the Python console with Go's one
	go io.Copy(con.StdinW, os.Stdin)
	wg.Add(1)
	go func() {
		io.Copy(os.Stdout, con.StdoutR)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		io.Copy(os.Stderr, con.StderrR)
		wg.Done()
	}()

	o_example, err := Py.Import_Import("example")
	defer Py.DecRef(o_example)
	exit_if_error(err)

	o_ret, err := Py.Object_CallMethod(o_example, "say_hello", "World")
	defer Py.DecRef(o_ret)
	exit_if_error(err)
}

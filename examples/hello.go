/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Show how to call a function exported by a module.
package main

import (
	"fmt"
	"os"
	"os/user"

	"gitlab.com/pygolo/py"
)

func exit_if_error(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func main() {
	Py, err := py.GoEmbed()
	defer Py.Close()
	exit_if_error(err)

	var greeting string

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	exit_if_error(err)

	o_sys_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_sys_path)
	exit_if_error(err)

	err = Py.List_Append(o_sys_path, ".")
	exit_if_error(err)

	o_example, err := Py.Import_Import("example")
	defer Py.DecRef(o_example)
	exit_if_error(err)

	o_greeting, err := Py.Object_CallMethod(o_example, "hello", "World")
	defer Py.DecRef(o_greeting)
	exit_if_error(err)
	err = Py.GoFromObject(o_greeting, &greeting)
	exit_if_error(err)
	fmt.Println(greeting)

	o_hello, err := Py.Object_GetAttr(o_example, "hello")
	defer Py.DecRef(o_hello)
	exit_if_error(err)

	user, err := user.Current()
	exit_if_error(err)

	o_greeting, err = Py.Object_Call(o_hello, py.GoArgs{user.Username}, py.GoKwArgs{"salutation": "Hi"})
	defer Py.DecRef(o_greeting)
	exit_if_error(err)
	err = Py.GoFromObject(o_greeting, &greeting)
	exit_if_error(err)
	fmt.Println(greeting)
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Print some info about the Python interpreter configuration.
package main

import (
	"fmt"
	"os"

	"gitlab.com/pygolo/py"
)

func exit_if_error(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func main() {
	Py, err := py.GoEmbed()
	defer Py.Close()
	exit_if_error(err)

	o_sysconfig, err := Py.Import_Import("sysconfig")
	defer Py.DecRef(o_sysconfig)
	exit_if_error(err)

	var pyVersion string
	o_pyVersion, err := Py.Object_CallMethod(o_sysconfig, "get_python_version")
	defer Py.DecRef(o_pyVersion)
	exit_if_error(err)
	err = Py.GoFromObject(o_pyVersion, &pyVersion)
	exit_if_error(err)

	var pyPlatform string
	o_pyPlatform, err := Py.Object_CallMethod(o_sysconfig, "get_platform")
	defer Py.DecRef(o_pyPlatform)
	exit_if_error(err)
	err = Py.GoFromObject(o_pyPlatform, &pyPlatform)
	exit_if_error(err)

	var pyPlatlib string
	o_pyPlatlib, err := Py.Object_CallMethod(o_sysconfig, "get_path", "platlib")
	defer Py.DecRef(o_pyPlatlib)
	exit_if_error(err)
	err = Py.GoFromObject(o_pyPlatlib, &pyPlatlib)
	exit_if_error(err)

	var pyNoGIL bool
	o_pyNoGIL, err := Py.Object_CallMethod(o_sysconfig, "get_config_var", "Py_GIL_DISABLED")
	defer Py.DecRef(o_pyNoGIL)
	exit_if_error(err)
	err = Py.GoFromObject(o_pyNoGIL, &pyNoGIL)
	exit_if_error(err)

	fmt.Printf("Python:\n")
	fmt.Printf("  version: \"%s\"\n", pyVersion)
	fmt.Printf("  platform: \"%s\"\n", pyPlatform)
	fmt.Printf("  platlib: \"%s\"\n", pyPlatlib)
	fmt.Printf("  GIL: %t\n", !pyNoGIL)
}

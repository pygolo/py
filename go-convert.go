/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"
import (
	"fmt"
	"io"
	"reflect"
)

// GoConvToObject is the type of a Go→Python conversion handler.
type GoConvToObject func(Py, interface{}) (Object, error)

// GoConvFromObject is the type of a Python→Go conversion handler.
type GoConvFromObject func(Py, Object, interface{}) error

type stateConverters struct {
	toType         map[reflect.Type]GoConvToObject
	toKind         map[reflect.Kind]GoConvToObject
	fromType       map[reflect.Type]GoConvFromObject
	fromKind       map[reflect.Kind]GoConvFromObject
	fromTypeObject map[TypeObject]GoConvFromObject
}

func (Py Py) initConverters() {
	Py.state.converters.toType = make(map[reflect.Type]GoConvToObject)
	Py.state.converters.toKind = make(map[reflect.Kind]GoConvToObject)
	Py.state.converters.fromType = make(map[reflect.Type]GoConvFromObject)
	Py.state.converters.fromKind = make(map[reflect.Kind]GoConvFromObject)
	Py.state.converters.fromTypeObject = make(map[TypeObject]GoConvFromObject)
}

// GoConvConf configures the Go⟷Python conversion handling.
//
// When converting from Go to Python, the (source) Go value is examined
// to find the most suitable conversion function. The type is examined
// first, if no handler for that type is found then the kind is examined.
// If again no handler is found, the conversion fails.
//
// When converting from Python to Go, in addition to the rules above
// also the type of the (source) Python object is examined as last.
type GoConvConf struct {
	// TypeOf is any Go value which type is key in the conversion.
	//
	// The type of this value, not the value, is used as key for registering
	// the conversion handlers. If omitted, the handlers are not registered
	// in the conversion map based on value type.
	TypeOf interface{}

	// Kind identifies a Go value kind key in the conversion.
	//
	// If omitted, the handlers are not registered in the conversion map based
	// on value kind.
	reflect.Kind

	// TypeObject identifies the Python type key in the conversion.
	//
	// If omitted, the handler is not registered in the Python type conversion map.
	TypeObject

	// ToObject handles the Go→Python conversion
	ToObject GoConvToObject

	// FromObject handles the Python→Go conversion
	FromObject GoConvFromObject
}

// GoRegisterConversions adds the FromObject and ToObject handlers to the
// appropriate conversion maps.
//
// When multiple converters are passed, either all or none are registered.
//
// The access to the underlying structures is not synchronized and therefore
// this function must be called during a single threaded initialization phase.
func (Py Py) GoRegisterConversions(cc ...GoConvConf) (err error) {
	var i int
	var c GoConvConf
	defer func() {
		if err != nil {
			Py.GoDeregisterConversions(cc[:i]...)
		}
	}()
	for i, c = range cc {
		if c.TypeOf == nil && c.Kind == reflect.Invalid && c.TypeObject == (TypeObject{}) {
			return fmt.Errorf("either TypeOf, Kind or TypeObject must be set")
		}
		if c.TypeObject != (TypeObject{}) && c.FromObject == nil {
			return fmt.Errorf("FromObject must be set")
		}
		if c.ToObject == nil && c.FromObject == nil {
			return fmt.Errorf("either ToObject or FromObject must be set")
		}
		Type := reflect.TypeOf(c.TypeOf)
		if Type != nil {
			_, found_to := Py.state.converters.toType[Type]
			_, found_from := Py.state.converters.fromType[Type]
			if found_to && c.ToObject != nil || found_from && c.FromObject != nil {
				return fmt.Errorf("Type handler is already registered: %s", Type)
			}
		}
		if c.Kind != reflect.Invalid {
			_, found_to := Py.state.converters.toKind[c.Kind]
			_, found_from := Py.state.converters.fromKind[c.Kind]
			if found_to && c.ToObject != nil || found_from && c.FromObject != nil {
				//lint:ignore ST1005 'Kind' is a proper name
				return fmt.Errorf("Kind handler is already registered: %s", c.Kind)
			}
		}
		if c.TypeObject != (TypeObject{}) {
			_, found_from := Py.state.converters.fromTypeObject[c.TypeObject]
			if found_from && c.FromObject != nil {
				return fmt.Errorf("TypeObject handler is already registered: %v", c.TypeObject)
			}
		}
		if Type != nil && c.ToObject != nil {
			Py.state.converters.toType[Type] = c.ToObject
		}
		if Type != nil && c.FromObject != nil {
			Py.state.converters.fromType[Type] = c.FromObject
		}
		if c.Kind != reflect.Invalid && c.ToObject != nil {
			Py.state.converters.toKind[c.Kind] = c.ToObject
		}
		if c.Kind != reflect.Invalid && c.FromObject != nil {
			Py.state.converters.fromKind[c.Kind] = c.FromObject
		}
		if c.TypeObject != (TypeObject{}) && c.FromObject != nil {
			Py.IncRef(c.TypeObject.AsObject())
			Py.state.converters.fromTypeObject[c.TypeObject] = c.FromObject
		}
	}
	return nil
}

// GoDeregisterConversions removes the FromObject and ToObject handlers
// from all the conversion maps.
//
// The access to the underlying structures is not synchronized and therefore
// this function must be called during a single threaded deinitialization phase.
func (Py Py) GoDeregisterConversions(cc ...GoConvConf) {
	for _, c := range cc {
		Type := reflect.TypeOf(c.TypeOf)
		if c.TypeOf != nil {
			delete(Py.state.converters.toType, Type)
			delete(Py.state.converters.fromType, Type)
		}
		if c.Kind != reflect.Invalid {
			delete(Py.state.converters.toKind, c.Kind)
			delete(Py.state.converters.fromKind, c.Kind)
		}
		if c.TypeObject != (TypeObject{}) {
			if _, ok := Py.state.converters.fromTypeObject[c.TypeObject]; ok {
				delete(Py.state.converters.fromTypeObject, c.TypeObject)
				Py.DecRef(c.TypeObject.AsObject())
			}
		}
	}
}

// GoToObject converts a Go value to a Python object.
//
// A new object is created or a new reference to an existing one is returned.
func (Py Py) GoToObject(a interface{}) (Object, error) {
	if o, ok := a.(Object); ok {
		return Py.NewRef(o), nil
	}
	if a == nil {
		return Object{}, Py.GoErrorConvToObject(a, TypeObject{})
	}
	t := reflect.TypeOf(a)
	if to := Py.state.converters.toType[t]; to != nil {
		return to(Py, a)
	}
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if to := Py.state.converters.toKind[t.Kind()]; to != nil {
		return to(Py, a)
	}
	return Object{}, Py.GoErrorConvToObject(a, TypeObject{})
}

// GoFromObject converts a Python object to a Go value.
//
// A pointer to the Go value is passed so that its type can be examined
// for guiding the conversion. Either the type is suitable or the conversion
// fails.
//
// If `any` is used as Go type, then the conversion is driven by the
// Python object type and it's left to the caller to use type assertion
// for accessing the actual value.
func (Py Py) GoFromObject(o Object, a interface{}) error {
	v := reflect.ValueOf(a)
	if v.Kind() != reflect.Ptr {
		return fmt.Errorf("cannot store in %T, need a pointer", a)
	}
	if v.IsNil() {
		return fmt.Errorf("cannot store in %v, need a pointer", a)
	}
	if o == (Object{}) {
		return Py.GoErrorConvFromObject(o, a)
	}
	if a, ok := a.(*Object); ok {
		*a = Py.NewRef(o)
		return nil
	}
	t := v.Type().Elem()
	if from := Py.state.converters.fromType[t]; from != nil {
		return from(Py, o, a)
	}
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Interface {
		if from := Py.state.converters.fromKind[t.Kind()]; from != nil {
			return from(Py, o, a)
		}
	}
	if from := Py.state.converters.fromTypeObject[o.Type()]; from != nil {
		return from(Py, o, a)
	}
	return Py.GoErrorConvFromObject(o, a)
}

// GoWithPyClose is used to release resources returned by GoFromObject.
//
// This is used by Arg_ParseTuple* to undo partial conversions when
// an error occours and by GoFunction object to undo the parameters
// conversion after the wrapped function returns.
type GoWithPyClose interface {
	PyClose(Py)
}

func (Py Py) undoGoFromObject(a interface{}) {
	switch a := a.(type) {
	case GoWithPyClose:
		a.PyClose(Py)
	case io.Closer:
		a.Close()
	}
}

// GoErrorConvToObject formats an error encountered by GoToObject.
func (Py Py) GoErrorConvToObject(a interface{}, t TypeObject) error {
	if t.t == nil {
		return fmt.Errorf("cannot convert a Go %T to Python", a)
	}
	return fmt.Errorf("cannot convert a Go %T to Python %s", a, t.Name())
}

// GoErrorConvFromObject formats an error encountered by GoFromObject.
func (Py Py) GoErrorConvFromObject(o Object, a interface{}) error {
	return fmt.Errorf("cannot convert a Python %s to Go %s",
		o.Type().Name(), reflect.TypeOf(a).Elem())
}

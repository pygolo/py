/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
//
// PyTypeObject *GoStructField_Type(PyTypeObject *base);
import "C"
import "reflect"

type goStructField struct {
	t   reflect.Type
	pos int
	GoAttrs
}

func (Py Py) newGoStructField_Type() (TypeObject, error) {
	t := TypeObject{C.GoStructField_Type(Py.GoObject_Type().t)}
	if t.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	return t, nil
}

func (Py Py) newStructField(t reflect.Type, pos int) (Object, error) {
	f := goStructField{
		t:       t,
		pos:     pos,
		GoAttrs: GoAttrsMap{},
	}
	return Py.newGoObject(Py.state.types.GoStructField_Type, f)
}

//export pgl_struct_field_descr_get
func pgl_struct_field_descr_get(field_, struct_, type_ *C.PyObject) *C.PyObject {
	var field goStructField
	Py, err := extendObject(field_, extractGoValue(&field))
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	if !Py.GoStructBase_Check(Object{struct_}) {
		Py.Err_Format(Exc_AttributeError, "struct object has the wrong type: %s", Object{struct_}.Type().Name())
		return nil
	}
	s := reflect.New(reflect.PtrTo(field.t))
	if !traverseGoObject(struct_, extractGoStruct(s.Interface())) {
		Py.Err_Format(Exc_AttributeError, "struct object has no %s values", field.t)
		return nil
	}
	v := reflect.Indirect(reflect.Indirect(s)).Field(field.pos)
	o_ret, err := Py.GoToObject(v.Interface())
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	return o_ret.o
}

//export pgl_struct_field_descr_set
func pgl_struct_field_descr_set(field_, struct_, value_ *C.PyObject) int {
	var field goStructField
	Py, err := extendObject(field_, extractGoValue(&field))
	if err != nil {
		Py.GoSetError(err)
		return -1
	}
	if !Py.GoStructBase_Check(Object{struct_}) {
		Py.Err_Format(Exc_AttributeError, "struct object has the wrong type: %s", Object{struct_}.Type().Name())
		return -1
	}
	s := reflect.New(reflect.PtrTo(field.t))
	if !traverseGoObject(struct_, extractGoStruct(s.Interface())) {
		Py.Err_Format(Exc_AttributeError, "struct object has no %s values", field.t)
		return -1
	}
	v := reflect.Indirect(reflect.Indirect(s)).Field(field.pos)
	err = Py.GoFromObject(Object{value_}, v.Addr().Interface())
	if err != nil {
		Py.GoSetError(err)
		return -1
	}
	return 0
}

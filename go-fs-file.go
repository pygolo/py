//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"
import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
)

// GoFSFile describes how a Go fs.File is exposed to Python.
type GoFSFile struct {
	// File holds the fs.File being exposed.
	File fs.File
}

func (g GoFSFile) read(Py Py, size int) (Object, error) {
	if size == -1 {
		fi, err := g.File.Stat()
		if err != nil {
			return Object{}, fmt.Errorf("could not stat file: %s", err)
		}
		size = int(fi.Size())
	}
	o_buf, err := Py.NewGoBuffer(size)
	defer Py.DecRef(o_buf)
	if err != nil {
		return Object{}, fmt.Errorf("could not allocate a buffer of size %d: %s", size, err)
	}
	var buf Buffer
	err = Py.Object_GetBuffer(o_buf, &buf, Buf_simple)
	defer Py.Buffer_Release(&buf)
	if err != nil {
		return Object{}, err
	}
	Py.GoLeavePython()
	_, err = g.File.Read(buf.UnsafeSlice())
	Py.GoEnterPython()
	if err != nil {
		return Object{}, fmt.Errorf("could not read file content: %s", err)
	}
	return Py.NewRef(o_buf), nil
}

func (g GoFSFile) readlines(hint int) ([]string, error) {
	var lines []string
	if hint > 0 {
		lines = make([]string, 0, hint)
	}
	scanner := bufio.NewScanner(g.File)
	for scanner.Scan() {
		lines = append(lines, scanner.Text()+"\n")
	}
	err := scanner.Err()
	if err != nil {
		return nil, fmt.Errorf("could not read lines: %s", err)
	}
	return lines, nil
}

func (g GoFSFile) write(buf Buffer) (int, error) {
	f, ok := g.File.(io.Writer)
	if !ok {
		return 0, fmt.Errorf("file is not writable")
	}
	return f.Write(buf.UnsafeSlice())
}

func (g GoFSFile) flush() {
}

func (g GoFSFile) close() {
	if closer, ok := g.File.(io.Closer); ok {
		closer.Close()
	}
}

func (g GoFSFile) isClosed() bool {
	return false
}

func (g GoFSFile) isReadable() bool {
	_, ok := g.File.(io.Reader)
	return ok
}

func (g GoFSFile) isWritable() bool {
	_, ok := g.File.(io.Writer)
	return ok
}

func (g GoFSFile) isSeekable() bool {
	_, ok := g.File.(io.Seeker)
	return ok
}

func (g GoFSFile) enterContext() GoFSFile {
	return g
}

func (g GoFSFile) exitContext(exc_type, exc_value, tb Object) bool {
	g.close()
	return false
}

func goFSFileRead(Py Py, args, kwargs Object) (Object, error) {
	var g GoFSFile
	size := int(-1)
	vars := GoArgsKwArgs{}
	vars.Positional(&g)
	vars.Optional()
	vars.Keyword("size", &size)
	err := Py.Arg_ParseTupleAndKeywords(args, kwargs, vars)
	if err != nil {
		return Object{}, err
	}
	return g.read(Py, size)
}

func goFSFileReadLines(Py Py, args, kwargs Object) ([]string, error) {
	var g GoFSFile
	hint := int(-1)
	vars := GoArgsKwArgs{}
	vars.Positional(&g)
	vars.Optional()
	vars.Keyword("hint", &hint)
	err := Py.Arg_ParseTupleAndKeywords(args, kwargs, vars)
	if err != nil {
		return nil, err
	}
	Py.GoLeavePython()
	defer Py.GoEnterPython()
	return g.readlines(hint)
}

func init() {
	GoAtInit(func(Py Py, m Object) error {
		fsFile := GoStruct{
			// do not allow instantiation from Python
			Struct:        (*GoFSFile)(nil),
			OmitByDefault: true,
		}
		fsFile.Method("read", GoFunction{Fn: goFSFileRead, InterpreterAccess: true, naked: true})
		fsFile.Method("readlines", GoFunction{Fn: goFSFileReadLines, InterpreterAccess: true, naked: true})
		fsFile.Method("write", GoFunction{Fn: GoFSFile.write})
		fsFile.Method("flush", GoFunction{Fn: GoFSFile.flush, InterpreterAccess: true})
		fsFile.Method("close", GoFunction{Fn: GoFSFile.close})
		fsFile.Property("closed", GoFunction{Fn: GoFSFile.isClosed, InterpreterAccess: true}, nil)
		fsFile.Method("readable", GoFunction{Fn: GoFSFile.isReadable, InterpreterAccess: true})
		fsFile.Method("writable", GoFunction{Fn: GoFSFile.isWritable, InterpreterAccess: true})
		fsFile.Method("seekable", GoFunction{Fn: GoFSFile.isSeekable, InterpreterAccess: true})
		fsFile.Method("__enter__", GoFunction{Fn: GoFSFile.enterContext, InterpreterAccess: true})
		fsFile.Method("__exit__", GoFunction{Fn: GoFSFile.exitContext, InterpreterAccess: true})
		err := Py.GoRegisterStruct(fsFile)
		if err != nil {
			return fmt.Errorf("could not register fs.File struct: %s", err)
		}
		return nil
	})
}

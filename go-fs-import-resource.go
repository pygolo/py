//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

import (
	"fmt"
	"io/fs"
	"path"
)

type fsImportResourceReader struct {
	fs   fs.FS
	path string
}

func (r fsImportResourceReader) OpenResource(name string) (GoFSFile, error) {
	p := path.Join(r.path, name)
	f, err := r.fs.Open(p)
	if err != nil {
		return GoFSFile{}, &GoError{"FileNotFountError", p, nil}
	}
	return GoFSFile{File: f}, nil
}

func (r fsImportResourceReader) ResourcePath(name string) (string, error) {
	p := path.Join(r.path, name)
	f, err := r.fs.Open(p)
	if err != nil {
		return "", &GoError{"FileNotFountError", p, nil}
	}
	defer f.Close()
	return p, nil
}

func (r fsImportResourceReader) IsResource(name string) (bool, error) {
	p := path.Join(r.path, name)
	f, err := r.fs.Open(p)
	if err != nil {
		return false, &GoError{"FileNotFountError", p, nil}
	}
	defer f.Close()
	_, ok := f.(fs.ReadDirFile)
	return !ok, nil
}

func (r fsImportResourceReader) Contents() ([]string, error) {
	f, err := r.fs.Open(r.path)
	if err != nil {
		return nil, &GoError{"FileNotFountError", r.path, nil}
	}
	defer f.Close()
	d, ok := f.(fs.ReadDirFile)
	if !ok {
		return nil, &GoError{"NotADirectoryError", r.path, nil}
	}
	entries, err := d.ReadDir(0)
	if err != nil {
		return nil, &GoError{"OSError", err.Error(), nil}
	}
	content := make([]string, len(entries))
	for i, entry := range entries {
		content[i] = entry.Name()
	}
	return content, nil
}

func init() {
	GoAtInit(func(Py Py, m Object) error {
		resourceReader := GoStruct{
			// do not allow instantiation from Python
			Struct: (*fsImportResourceReader)(nil),
		}
		resourceReader.Rename("OpenResource", "open_resource")
		resourceReader.Rename("ResourcePath", "resource_path")
		resourceReader.Rename("IsResource", "is_resource")
		resourceReader.Rename("Contents", "contents")
		err := Py.GoRegisterStruct(resourceReader)
		if err != nil {
			return fmt.Errorf("could not register resource reader struct: %s", err)
		}
		return nil
	})
}

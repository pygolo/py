//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

import (
	"embed"
	"fmt"
	"io/fs"
	"path"
	"strings"
	"sync"
	"testing/fstest"
)

type stateFSImportFinders struct {
	// 1. adding finders is a side effect of a conversion, which is subtle
	// 2. module importing may happen at any time
	// therefore we cannot ask the users to follow any rules, need to
	// synchronize access to finders on our own
	sync.Mutex
	m map[string]fsImportFinder
}

// GoFSImport describes how a Go fs.FS is exposed to Python.
//
// The Python object corresponding to a Go fs.FS is not very useful by
// itself, it's just a string like "fs.FS{0xabcdef}". Such conversion
// however has a side effect: it installs a module importer that
// knows how to search and load modules from that Go fs.FS.
//
// Adding an fs.FS to a modules search path like sys.path has
// the combined effect of allowing you to import modules from it.
//
// Example:
//
//	//go:embed *.py
//	var embedded embed.FS
//
//	func installEmbeddedFS(Py py.Py) error {
//	    sys, err := Py.Import_Import("sys")
//	    defer Py.DecRef(sys)
//	    if err != nil {
//	        return fmt.Errorf("could not import 'sys': %w", err)
//	    }
//
//	    path, err := Py.Object_GetAttr(sys, "path")
//	    defer Py.DecRef(path)
//	    if err != nil {
//	        return fmt.Errorf("could not get 'sys.path': %w", err)
//	    }
//
//	    return Py.List_Append(path, py.GoFSImport{FS: embedded})
//	}
type GoFSImport struct {
	// FS is the file system to be exposed to Python.
	FS fs.FS

	// Prefix is the path prefix applied to every look up in this file system.
	//
	// Leave it emtpy if you want to search modules in the whole file system.
	Prefix string

	// Namespace is where this file system is attached to.
	//
	// Leave it empty if you want to attach this file system to the root.
	Namespace string

	// Root is the path root that identifies this file system.
	//
	// embed.FS and fstest.MapFS do not need this.
	//
	// Leave it empty if the "%v" formatting of the file system contains
	// a pointer or some other unique value.
	Root string
}

type fsImportFinder struct {
	GoFSImport `python:",omit"`
}

type fsImportLoader struct {
	fs   fs.FS
	root string
	path string
}

// root returns a string representing the specific fs.FS.
func (g GoFSImport) root() string {
	root := g.Root
	if root == "" {
		root = fmt.Sprintf("%T", g.FS)
		switch fs := g.FS.(type) {
		case fstest.MapFS:
			root += fmt.Sprintf("{%p}", fs)
		default:
			root += fmt.Sprintf("%v", fs)
		}
	}
	if g.Namespace != "" {
		root += fmt.Sprintf("@%s", g.Namespace)
	}
	return root + ":"
}

func (g GoFSImport) String() string {
	return path.Join(g.root(), g.Prefix)
}

// FindSpec returns a module spec if the name module is found.
//
// The searched module could also be a namespace package which,
// as regular packages, would expand the search paths used for
// subsequent module imports.
//
// The Python import system caches which finders are authoritative
// for a given package, in the case of namespace packages there could
// be multiple finders.
func (f fsImportFinder) FindSpec(Py Py, name string, target Object) (Object, error) {
	parts := strings.Split(name, ".")
	if f.Namespace != "" {
		for _, part := range strings.Split(f.Namespace, ".") {
			if len(parts) == 0 {
				// the Python import system is exploring this finder offering,
				// so far it probed only namespace packages
				return f.getSpec(Py, name, "", "")
			}
			if parts[0] != part {
				Py.GoEnterPython()
				defer Py.GoLeavePython()
				// the Python import system is searching for something not
				// offered by this finder, let's make it stop searching here
				return Py.NewRef(None), nil
			}
			parts = parts[1:]
		}
	}
	prefix := strings.Join(parts, "/")
	if f.Prefix != "" {
		// every look up needs to start somewhere deeper in the tree
		prefix = path.Join(f.Prefix, prefix)
	}
	// we have a module path to search, let's see what we have in the file system:
	//   <module path>/__init__.py    (a package)
	//   <module path>.py             (a plain module)
	//   <module path>                (a namespace package)
	for _, suffix := range []string{"/__init__.py", ".py", ""} {
		path := strings.Trim(prefix+suffix, "/")
		if path == "" {
			path = "."
		}
		// check if it's in the file system
		if fi, err := fs.Stat(f.FS, path); err == nil {
			// it must be either a .py file or a directory
			if suffix != "" || fi.IsDir() {
				return f.getSpec(Py, name, prefix, suffix)
			}
		}
	}
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	// the Python import system is searching for something not
	// offered by this finder, let's make it stop searching here
	return Py.NewRef(None), nil
}

func (f fsImportFinder) getSpec(Py Py, name, prefix, suffix string) (Object, error) {
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	root := f.root()
	// default: make the Python import system create a namespace package
	var loader interface{} = None
	// if it's a real file, go with the loader that picks from our file system
	if suffix != "" {
		loader = fsImportLoader{f.FS, root, strings.Trim(prefix+suffix, "/")}
	}
	// path of the module as seen by the Python interpeter
	path := path.Join(root, prefix)
	o_util, err := Py.Import_Import("importlib.util")
	defer Py.DecRef(o_util)
	if err != nil {
		return Object{}, fmt.Errorf("could not import 'importlib.util': %s", err)
	}
	o_spec_from_loader, err := Py.Object_GetAttr(o_util, "spec_from_loader")
	defer Py.DecRef(o_spec_from_loader)
	if err != nil {
		return Object{}, fmt.Errorf("could not get 'importlib.util.spec_from_loader': %s", err)
	}
	is_package := suffix != ".py"
	o_spec, err := Py.Object_Call(o_spec_from_loader, GoArgs{name, loader},
		GoKwArgs{"is_package": is_package, "origin": path + suffix})
	defer Py.DecRef(o_spec)
	if err != nil {
		return Object{}, fmt.Errorf("could not invoke 'importlib.util.spec_from_loader': %s", err)
	}
	// namespace packages have no location
	err = Py.Object_SetAttr(o_spec, "has_location", suffix != "")
	if err != nil {
		return Object{}, fmt.Errorf("could not set spec's 'has_location': %s", err)
	}
	if is_package {
		// let the Python import system know it can search here for more modules
		o_submodule_search_locations, err := Py.Object_GetAttr(o_spec, "submodule_search_locations")
		defer Py.DecRef(o_submodule_search_locations)
		if err != nil {
			return Object{}, fmt.Errorf("could not get spec's 'submodule_search_locations': %s", err)
		}
		err = Py.List_Append(o_submodule_search_locations, path)
		if err != nil {
			return Object{}, fmt.Errorf("could not append path to spec's 'submodule_search_locations': %s", err)
		}
	}
	return Py.NewRef(o_spec), nil
}

func (f fsImportFinder) PyRepr() (string, error) {
	return fmt.Sprintf("<fsImportFinder object '%v'>", f), nil
}

func (l fsImportLoader) CreateModule(Py Py, spec Object) (Object, error) {
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	// it's ok with the default module created by the Python import system for us
	return Py.NewRef(None), nil
}

func (l fsImportLoader) ExecModule(Py Py, o_module Object) error {
	// read the module file
	code, err := fs.ReadFile(l.fs, l.path)
	if err != nil {
		return fmt.Errorf("could not read the module file: %s", err)
	}
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	// compile it as a regular file, use the virtual file path as name in case
	// of any traceback
	o_code, err := Py.CompileString(string(code), path.Join(l.root, l.path), File_input)
	defer Py.DecRef(o_code)
	if err != nil {
		return fmt.Errorf("could not compile the module code: %s", err)
	}
	o_builtins, err := Py.Import_Import("builtins")
	defer Py.DecRef(o_builtins)
	if err != nil {
		return fmt.Errorf("could not import 'builtins': %s", err)
	}
	o_builtins_dict, err := Py.Object_GetAttr(o_builtins, "__dict__")
	defer Py.DecRef(o_builtins_dict)
	if err != nil {
		return fmt.Errorf("could not get 'builtins.__dict__': %s", err)
	}
	o_module_dict, err := Py.Object_GetAttr(o_module, "__dict__")
	defer Py.DecRef(o_module_dict)
	if err != nil {
		return fmt.Errorf("could not get module's '__dict__': %s", err)
	}
	// manually add the builtins to the new module environment, older
	// Python interpreters don't do it
	err = Py.Dict_SetItem(o_module_dict, "__builtins__", o_builtins_dict)
	if err != nil {
		return fmt.Errorf("could not set module's '__builtins__': %s", err)
	}
	// eventually evaluate the module code!
	o_ret, err := Py.Eval_EvalCode(o_code, o_module_dict, o_module_dict)
	defer Py.DecRef(o_ret)
	if err != nil {
		return fmt.Errorf("could not evaluate module code: %s", err)
	}
	return nil
}

func (l fsImportLoader) GetResourceReader(anchor string) fsImportResourceReader {
	return fsImportResourceReader{fs: l.fs, path: strings.ReplaceAll(anchor, ".", "/")}
}

func (l fsImportLoader) PyRepr() (string, error) {
	return fmt.Sprintf("<fsImportLoader object '%s/%s'>", l.root, l.path), nil
}

// fsImportPathHook returns a finder for the given path if it's one of ours.
//
// It may be invoked multiple times while the Python import system discovers
// any package under the top path registered initially.
func (Py Py) fsImportPathHook(path string) (fsImportFinder, error) {
	// we don't need the Python GIL because we don't access the interpreter but
	// we still need to synchronize the access to the registered finders
	Py.state.fsFinders.Lock()
	defer Py.state.fsFinders.Unlock()
	for root, finder := range Py.state.fsFinders.m {
		if path == root || strings.HasPrefix(path, root+"/") {
			return finder, nil
		}
	}
	return fsImportFinder{}, &GoError{"ImportError", "fsImportFinder not found", nil}
}

func fsToObject(Py Py, a interface{}) (Object, error) {
	g, ok := a.(GoFSImport)
	if !ok {
		g = GoFSImport{FS: a.(fs.FS)}
	}
	path := g.String()
	o_path, err := Py.GoToObject(path)
	if err != nil {
		return Object{}, fmt.Errorf("could not convert fs' path to Python: %s", err)
	}
	Py.state.fsFinders.Lock()
	defer Py.state.fsFinders.Unlock()
	if _, ok := Py.state.fsFinders.m[path]; !ok {
		Py.state.fsFinders.m[path] = fsImportFinder{g}
	}
	return o_path, nil
}

func init() {
	GoAtInit(func(Py Py, m Object) error {
		Py.state.fsFinders.m = make(map[string]fsImportFinder)
		finder := GoStruct{
			// do not allow instantiation from Python
			Struct: (*fsImportFinder)(nil),
		}
		loader := GoStruct{
			// do not allow instantiation from Python
			Struct: (*fsImportLoader)(nil),
		}
		finder.Rename("FindSpec", "find_spec")
		loader.Rename("CreateModule", "create_module")
		loader.Rename("ExecModule", "exec_module")
		loader.Rename("GetResourceReader", "get_resource_reader")
		err := Py.GoRegisterStruct(finder, loader)
		if err != nil {
			return fmt.Errorf("could not register file system import structs: %s", err)
		}
		o_sys, err := Py.Import_Import("sys")
		defer Py.DecRef(o_sys)
		if err != nil {
			return fmt.Errorf("could not import 'sys': %s", err)
		}
		o_path_hooks, err := Py.Object_GetAttr(o_sys, "path_hooks")
		defer Py.DecRef(o_path_hooks)
		if err != nil {
			return fmt.Errorf("could not get 'sys.path_hooks': %s", err)
		}
		// install our callback, it's bound to the Py context
		err = Py.List_Append(o_path_hooks, Py.fsImportPathHook)
		if err != nil {
			return fmt.Errorf("could not append to 'sys.path_hooks': %s", err)
		}
		return nil
	})

	GoAtFini(func(Py Py) {
		Py.GoDeregisterStruct(fsImportFinder{})
		Py.GoDeregisterStruct(fsImportLoader{})
		Py.state.fsFinders.m = nil
	})

	cc := []GoConvConf{
		{ToObject: fsToObject, TypeOf: embed.FS{}},
		{ToObject: fsToObject, TypeOf: fstest.MapFS{}},
		{ToObject: fsToObject, TypeOf: GoFSImport{}},
	}
	GoAtInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(cc...)
	})
	GoAtFini(func(Py Py) {
		Py.GoDeregisterConversions(cc...)
	})
}

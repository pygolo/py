/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
//
// PyTypeObject *GoFunction_Type(PyTypeObject *);
import "C"
import (
	"fmt"
	"reflect"
)

// GoFunction_Type returns the GoFunction type object.
func (Py Py) GoFunction_Type() TypeObject {
	return Py.state.types.GoFunction_Type
}

// GoFunction_Check returns true if o is of type GoFunction_Type.
//
// Subtypes of GoFunction_Type also match this check.
func (Py Py) GoFunction_Check(o Object) bool {
	return Py.Type_IsSubtype(o.Type(), Py.GoFunction_Type())
}

// GoFunction describes how a Go function is exposed to Python.
type GoFunction struct {
	// Fn holds a reference to the Go function being exposed.
	Fn interface{}

	// GoAttrs provides the get/set attributes protocol of the function object.
	GoAttrs

	// InterpreterAccess controls whether or not the exposed
	// function is allowed to access the interpreter by default.
	//
	// Setting this to true is equivalent to wrapping the function
	// between Py.GoEnterPython() and Py.GoLeavePython().
	InterpreterAccess bool

	naked bool
}

func (Py Py) newGoFunction_Type() (TypeObject, error) {
	t := TypeObject{C.GoFunction_Type(Py.GoObject_Type().t)}
	defer Py.DecRef(t.AsObject())
	if t.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	c := GoConvConf{
		TypeObject: t,
		FromObject: funcFromObject,
	}
	err := Py.GoRegisterConversions(c)
	if err != nil {
		return TypeObject{}, err
	}
	Py.IncRef(t.AsObject())
	return t, nil
}

func (Py Py) delGoFunction_Type(t TypeObject) {
	c := GoConvConf{
		TypeObject: t,
	}
	Py.GoDeregisterConversions(c)
}

//export pgl_call
func pgl_call(self_, args_, kwargs_ *C.PyObject) *C.PyObject {
	var self *GoFunction
	Py, err := extendObject(self_, extractGoValue(&self))
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	o_ret, err := Py.genericCall(*self, Object{args_}, Object{kwargs_})
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	return o_ret.o
}

func (Py Py) genericCall(self GoFunction, args, kwargs Object) (Object, error) {
	if kwargs.o != nil && !self.naked {
		truthy, err := Py.Object_IsTrue(kwargs)
		if err != nil {
			return Object{}, err
		}
		if truthy {
			return Object{}, &GoError{"TypeError", "Unexpected keyword arguments", nil}
		}
	}

	fn := reflect.ValueOf(self.Fn)
	fn_t := fn.Type()
	num_in := fn_t.NumIn()
	num_out := fn_t.NumOut()
	py_t := reflect.TypeOf(Py)

	ins := make([]reflect.Value, 0, num_in)
	a_ins := make([]interface{}, 0, num_in)
	a_outs := make([]interface{}, 0, num_out)

	if self.naked {
		ins = append(ins, reflect.ValueOf(Py))
		ins = append(ins, reflect.ValueOf(args))
		ins = append(ins, reflect.ValueOf(kwargs))
	} else {
		for i := 0; i < num_in; i++ {
			in_t := fn_t.In(i)
			if in_t == py_t {
				ins = append(ins, reflect.ValueOf(Py))
			} else if i < num_in-1 || !fn_t.IsVariadic() {
				in := reflect.New(in_t)
				ins = append(ins, reflect.Indirect(in))
				a_ins = append(a_ins, in.Interface())
			} else {
				length, err := Py.Object_Length(args)
				if err != nil {
					return Object{}, err
				}
				for len(a_ins) < length {
					in := reflect.New(in_t.Elem())
					ins = append(ins, reflect.Indirect(in))
					a_ins = append(a_ins, in.Interface())
				}
			}
		}

		err := Py.Arg_ParseTuple(args, a_ins...)
		if err != nil {
			return Object{}, err
		}
	}

	if !self.InterpreterAccess {
		Py.GoLeavePython()
	}
	outs, err := safeCall(fn, ins)
	if !self.InterpreterAccess {
		Py.GoEnterPython()
	}

	if !self.naked {
		// release all the resources acquired with the params conversions, whether
		// the wrapped function returns an error or not
		for _, a_in := range a_ins {
			Py.undoGoFromObject(a_in)
		}
	}

	if err != nil {
		return Object{}, &GoError{"RuntimeError", err.Error(), nil}
	}
	for _, out := range outs {
		a_outs = append(a_outs, out.Interface())
	}

	if num_out > 0 {
		var err error
		if outs[num_out-1].Type() == reflect.TypeOf(&err).Elem() {
			if err, ok := a_outs[num_out-1].(error); ok {
				return Object{}, err
			}
			a_outs = a_outs[:num_out-1]
		}
	}

	var o_ret Object
	if len(a_outs) > 1 {
		o_ret, err = Py.Tuple_Pack(a_outs...)
	} else if len(a_outs) > 0 {
		o_ret, err = Py.GoToObject(a_outs[0])
	} else {
		o_ret = Py.NewRef(None)
	}
	if err != nil {
		return Object{}, &GoError{"TypeError", err.Error(), nil}
	}
	return o_ret, nil
}

func safeCall(fn reflect.Value, ins []reflect.Value) (outs []reflect.Value, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic: %v", r)
		}
	}()
	return fn.Call(ins), nil
}

func (Py Py) validateGoFunction(fn GoFunction) error {
	if fn.Fn == nil {
		return Py.GoErrorConvToObject(fn.Fn, Py.GoFunction_Type())
	}
	fn_t := reflect.TypeOf(fn.Fn)
	if fn_t.Kind() != reflect.Func {
		return Py.GoErrorConvToObject(fn.Fn, Py.GoFunction_Type())
	}
	if reflect.ValueOf(fn.Fn).IsNil() {
		return Py.GoErrorConvToObject(nil, Py.GoFunction_Type())
	}
	py_t := reflect.TypeOf(Py)
	py_ptr_t := reflect.PtrTo(py_t)
	py_slice_t := reflect.SliceOf(py_t)
	py_ptr_slice_t := reflect.PtrTo(py_slice_t)
	py_slice_ptr_t := reflect.SliceOf(py_ptr_t)
	found := false
	for i := 0; i < fn_t.NumIn(); i++ {
		switch fn_t.In(i) {
		case py_t:
			if found {
				err := Py.GoErrorConvToObject(fn.Fn, Py.GoFunction_Type())
				return fmt.Errorf("%s: cannot handle multiple %v parameters", err, fn_t.In(i))
			}
			found = true
		case py_ptr_t, py_slice_t, py_ptr_slice_t, py_slice_ptr_t:
			err := Py.GoErrorConvToObject(fn.Fn, Py.GoFunction_Type())
			if i < fn_t.NumIn()-1 || !fn_t.IsVariadic() {
				return fmt.Errorf("%s: cannot handle %v parameters", err, fn_t.In(i))
			}
			return fmt.Errorf("%s: cannot handle a variable number of %v parameters", err, fn_t.In(i).Elem())
		}
	}
	return nil
}

// funcToObject wraps a Go function in a Python object.
func funcToObject(Py Py, a interface{}) (Object, error) {
	fn, ok := a.(GoFunction)
	if !ok {
		fn = GoFunction{Fn: a}
	}
	err := Py.validateGoFunction(fn)
	if err != nil {
		return Object{}, err
	}
	if fn.GoAttrs == nil {
		fn.GoAttrs = GoAttrsMap{}
	}
	err = fn.GoAttrs.PySetAttr(Py, "__doc__", fmt.Sprintf("%T", fn.Fn))
	if err != nil {
		return Object{}, err
	}
	return Py.newGoObject(Py.GoFunction_Type(), &fn)
}

// funcFromObject returns the Go function that was wrapped in a Python object.
func funcFromObject(Py Py, o Object, a interface{}) error {
	if !Py.GoFunction_Check(o) {
		return Py.GoErrorConvFromObject(o, a)
	}
	var self *GoFunction
	if !traverseGoObject(o.o, extractGoValue(&self)) {
		return fmt.Errorf("function object is not properly initialized")
	}

	fn := reflect.ValueOf(self.Fn)
	dest := reflect.ValueOf(a)

	switch dest.Elem().Kind() {
	case reflect.Interface:
	case reflect.Func:
		if dest.Elem().Type() != fn.Type() {
			return fmt.Errorf("cannot convert a Python <%s> to Go <%s>", fn.Type(), dest.Elem().Type())
		}
	default:
		return Py.GoErrorConvFromObject(o, a)
	}
	dest.Elem().Set(fn)
	return nil
}

func init() {
	cc := []GoConvConf{
		{
			Kind:     reflect.Func,
			TypeOf:   GoFunction{},
			ToObject: funcToObject,
		}, {
			Kind:       reflect.Func,
			FromObject: funcFromObject,
		},
	}
	atCoreInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(cc...)
	})
	atCoreFini(func(Py Py) {
		Py.GoDeregisterConversions(cc...)
	})
}

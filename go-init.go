/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// There are two kinds of initializers/finalizers: core and regular.
// Core initializers are all executed before the regular ones.
//
// This is to cope with interdependencies in an unsofisticated way:
// - core are all independent from each other and can be executed
//   in any order
// - regular may depend on core ones but any other dependency has
//   to be managed in some other way; they are executed in the same
//   order/opposite order of registration depending on whether
//   it's a run for initialization or finalization

var initializers []GoInitializer
var finalizers []GoFinalizer

// GoInitializer is the type of a Go initializer function.
//
// Object is the extension module that is being initialized
// or the zero-value in case of an embedded interpreter.
//
// See GoAtInit for more details.
type GoInitializer func(Py, Object) error

// GoFinalizer is the type of a Go finalizer function.
//
// See GoAtFini for more details.
type GoFinalizer func(Py)

func atCoreInit(fn GoInitializer) {
	initializers = append([]GoInitializer{fn}, initializers...)
}

func atCoreFini(fn GoFinalizer) {
	finalizers = append([]GoFinalizer{fn}, finalizers...)
}

// GoAtInit appends fn to the Python state initialization sequence.
//
// The initializers are executed in the same order of registration with GoAtInit;
// therefore, if invoked from package init functions, the same rules are applied.
//
// Example: all the initializers of the sub-packages are executed before those
// of the including package.
func GoAtInit(fn GoInitializer) {
	initializers = append(initializers, fn)
}

// GoAtFini appends fn to the Python state finalization sequence.
//
// The finalizers are executed in the reverse order of registration with GoAtFini;
// therefore, if invoked from package init functions, the same rules are applied.
//
// Example: all the finalizers of the sub-packages are executed after those
// of the including package.
func GoAtFini(fn GoFinalizer) {
	finalizers = append(finalizers, fn)
}

func (Py Py) callInitializers(m Object) error {
	for _, fn := range initializers {
		err := fn(Py, m)
		if err != nil {
			return err
		}
	}
	return nil
}

func (Py Py) callFinalizers() {
	for i := len(finalizers) - 1; i >= 0; i-- {
		fn := finalizers[i]
		fn(Py)
	}
}

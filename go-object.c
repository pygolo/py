/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include "_cgo_export.h"

typedef struct {
	PyObject_VAR_HEAD
	void *data[];
} GoObject;

static PyGetSetDef pgl_getset[] = {
	{"__class__", pgl_get_attr, NULL, NULL, "__class__"},
	{"__doc__", pgl_get_attr, pgl_set_attr, NULL, "__doc__"},
	{"__module__", pgl_get_attr, pgl_set_attr, NULL, "__module__"},
	{"__name__", pgl_get_attr, pgl_set_attr, NULL, "__name__"},
	{NULL} /* Sentinel */
};

int pgl_disallow_init(PyObject *, PyObject *, PyObject *);
static int pgl_bf_getbuffer(PyObject *, Py_buffer *, int);

static PyBufferProcs pgl_as_buffer = {
	.bf_getbuffer = pgl_bf_getbuffer,
};

PyTypeObject *
GoObject_Type(Py_ssize_t size)
{
	PyType_Slot slots[] = {
		{Py_tp_new, pgl_new},
		{Py_tp_init, pgl_disallow_init},
		{Py_tp_dealloc, pgl_dealloc},
		{Py_tp_getset, pgl_getset},
		{0, NULL} /* Sentinel */
	};

	PyType_Spec spec = {
		.name = "GoObject",
		.flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
		.basicsize = sizeof(GoObject) + size,
		.itemsize = sizeof(unsigned char),
		.slots = slots,
	};

	PyTypeObject *t = (PyTypeObject *) PyType_FromSpec(&spec);
	t->tp_as_buffer = &pgl_as_buffer;
	return t;
}

PyObject *
pgl_new_object(PyTypeObject *type, Py_ssize_t size)
{
	GoObject *o = PyObject_NewVar(GoObject, type, size);
	if (o) {
		assert(Py_TYPE(o)->tp_basicsize > sizeof(GoObject));
		assert(Py_TYPE(o)->tp_itemsize * size >= 0);
		memset(o->data, 0, Py_TYPE(o)->tp_basicsize - sizeof(GoObject) + Py_TYPE(o)->tp_itemsize * size);

#if PY_VERSION_HEX < 0x03080000
		// Workaround for Python issue 35810; no longer necessary in Python 3.8
		// See https://docs.python.org/3/whatsnew/3.8.html#changes-in-the-c-api
		// and https://github.com/python/cpython/issues/84398
		Py_INCREF(type);
#endif
	}
	return (PyObject *) o;
}

int
pgl_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
	return 0;
}

int
pgl_disallow_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
	PyErr_Format(PyExc_TypeError, "cannot create '%s' instances", Py_TYPE(self)->tp_name);
	return -1;
}

void
pgl_del_object(PyObject *self)
{
	PyTypeObject *tp = Py_TYPE(self);
	tp->tp_free(self);
#if PY_VERSION_HEX >= 0x03080000
	// This was not needed before Python 3.8 (Python issue 35810)
	// See https://docs.python.org/3/whatsnew/3.8.html#changes-in-the-c-api
	// and https://github.com/python/cpython/issues/84398
	Py_DECREF(tp);
#endif
}

void *
pgl_get_object_data(PyObject *o)
{
	return ((GoObject *) o)->data;
}

static int
pgl_bf_getbuffer(PyObject *o, Py_buffer *view, int flags)
{
	void *buf = ((unsigned char *) o) + Py_TYPE(o)->tp_basicsize;
	Py_ssize_t size = Py_TYPE(o)->tp_itemsize * ((PyVarObject *) o)->ob_size;
	return PyBuffer_FillInfo(view, o, buf, size, 0, flags);
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
// #include "go-object.h"
//
// PyTypeObject *GoObject_Type(Py_ssize_t);
import "C"
import (
	"fmt"
	"reflect"
	"unsafe"
)

type goObject struct {
	state  *state
	values []interface{}
}

type goObjectVisitor func(interface{}) bool

func (g goObject) traverse(visit goObjectVisitor) bool {
	if visit == nil {
		return false
	}
	for _, value := range g.values {
		if visit(value) {
			return true
		}
	}
	return false
}

func traverseGoObject(o *C.PyObject, visit goObjectVisitor) bool {
	return getGoObjectHandle(o).Get().(*goObject).traverse(visit)
}

// GoObject_Type returns the GoObject type object.
func (Py Py) GoObject_Type() TypeObject {
	return Py.state.types.GoObject_Type
}

// GoObject_Check returns true if o is of type GoObject_Type.
//
// Subtypes of GoObject_Type also match this check.
func (Py Py) GoObject_Check(o Object) bool {
	return Py.Type_IsSubtype(o.Type(), Py.GoObject_Type())
}

func (Py Py) newGoObject_Type() (TypeObject, error) {
	t := TypeObject{C.GoObject_Type(C.Py_ssize_t(unsafe.Sizeof(GoHandle{})))}
	defer Py.DecRef(t.AsObject())
	if t.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	o_state, err := Py.newGoObject(t, nil)
	defer Py.DecRef(o_state)
	if err != nil {
		return TypeObject{}, fmt.Errorf("could not create __go_state__: %s", err)
	}
	err = Py.Object_SetAttr(t.AsObject(), "__go_state__", o_state)
	if err != nil {
		return TypeObject{}, fmt.Errorf("could not set __go_state__: %s", err)
	}
	Py.IncRef(t.AsObject())
	return t, nil
}

func (Py Py) newGoVarObject(t TypeObject, a interface{}, size int) (Object, error) {
	if size < 0 {
		return Object{}, fmt.Errorf("invalid buffer size: %d", size)
	}
	// Allocate the Python object, add storage for the extra data
	o := C.pgl_new_object(t.t, C.Py_ssize_t(size))
	if o == nil {
		return Object{}, &GoError{"MemoryError", "could not allocate GoObject", nil}
	}
	if a, ok := a.(GoAttrs); ok {
		err := a.PySetAttr(Py, "__class__", t)
		if err != nil {
			Py.DecRef(Object{o})
			return Object{}, err
		}
	}
	// Store the Go value in the Python object via GoHandle
	getGoObjectHandle(o).Set(&goObject{Py.state, []interface{}{a}})
	return Object{o}, nil
}

func (Py Py) newGoObject(t TypeObject, a interface{}) (Object, error) {
	return Py.newGoVarObject(t, a, 0)
}

// NewGoBuffer allocates a buffer shared between Go and Python.
func (Py Py) NewGoBuffer(size int) (Object, error) {
	return Py.newGoVarObject(Py.GoObject_Type(), GoAttrsMap{}, size)
}

func getGoObjectHandle(o *C.PyObject) *GoHandle {
	return (*GoHandle)(C.pgl_get_object_data(o))
}

func extractGoValue(out_ interface{}) goObjectVisitor {
	out := reflect.ValueOf(out_)
	if out.Kind() != reflect.Ptr {
		return nil
	}
	out = reflect.Indirect(out)
	fn := func(in_ interface{}) bool {
		in := reflect.ValueOf(in_)
		if in.Type() == out.Type() || out.Kind() == reflect.Interface && in.Type().Implements(out.Type()) {
			out.Set(in)
			return true
		}
		return false
	}
	return fn
}

//export pgl_new
func pgl_new(type_ *C.PyTypeObject, args_, kwargs_ *C.PyObject) *C.PyObject {
	o := C.PyType_GenericNew(type_, args_, kwargs_)
	if o == nil {
		return nil
	}
	Py, err := extendNewObject(o)
	if err != nil {
		Py.DecRef(Object{o})
		Py.GoSetError(err)
		return nil
	}
	getGoObjectHandle(o).Set(&goObject{Py.state, nil})
	return o
}

//export pgl_dealloc
func pgl_dealloc(o *C.PyObject) {
	getGoObjectHandle(o).Close()
	C.pgl_del_object(o)
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
//
// PyTypeObject *GoProperty_Type(PyTypeObject *base);
import "C"
import "fmt"

// GoProperty_Type returns the GoProperty type object.
func (Py Py) GoProperty_Type() TypeObject {
	return Py.state.types.GoProperty_Type
}

// GoProperty_Check returns true if o is of type GoProperty_Type.
//
// Subtypes of GoProperty_Type also match this check.
func (Py Py) GoProperty_Check(o Object) bool {
	return Py.Type_IsSubtype(o.Type(), Py.GoProperty_Type())
}

// GoProperty describes how Go getter/setter functions become a Python property.
type GoProperty struct {
	// Getter is the function that reads the property. It has no parameters.
	Getter GoFunction

	// Setter is the function that writes the property. It has only one parameter, the value to be written.
	Setter GoFunction

	// GoAttrs provides the get/set attributes protocol of the property object.
	GoAttrs
}

func (Py Py) newGoProperty_Type() (TypeObject, error) {
	t := TypeObject{C.GoProperty_Type(Py.GoObject_Type().t)}
	if t.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	return t, nil
}

//export pgl_struct_property_descr_get
func pgl_struct_property_descr_get(property_, struct_, type_ *C.PyObject) *C.PyObject {
	var property GoProperty
	Py, err := extendObject(property_, extractGoValue(&property))
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	if property.Getter.Fn == nil {
		o_name, err := property.PyGetAttr(Py, "__name__")
		if err != nil {
			Py.GoSetError(err)
			return nil
		}
		var name string
		err = Py.GoFromObject(o_name, &name)
		if err != nil {
			Py.GoSetError(err)
			return nil
		}
		Py.Err_SetString(Exc_AttributeError, fmt.Sprintf("property '%s' of '%s' has no getter",
			name, Object{struct_}.Type().Name()))
		return nil
	}
	args := [1]Object{Object{struct_}}
	o_args, err := Py.GoToObject(args)
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	o_ret, err := Py.genericCall(property.Getter, o_args, Object{})
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	return o_ret.o
}

//export pgl_struct_property_descr_set
func pgl_struct_property_descr_set(property_, struct_, value_ *C.PyObject) int {
	var property GoProperty
	Py, err := extendObject(property_, extractGoValue(&property))
	if err != nil {
		Py.GoSetError(err)
		return -1
	}
	if property.Setter.Fn == nil {
		o_name, err := property.PyGetAttr(Py, "__name__")
		if err != nil {
			Py.GoSetError(err)
			return -1
		}
		var name string
		err = Py.GoFromObject(o_name, &name)
		if err != nil {
			Py.GoSetError(err)
			return -1
		}
		Py.Err_SetString(Exc_AttributeError, fmt.Sprintf("property '%s' of '%s' has no setter",
			name, Object{struct_}.Type().Name()))
		return -1
	}
	args := [2]Object{Object{struct_}, Object{value_}}
	o_args, err := Py.GoToObject(args)
	if err != nil {
		Py.GoSetError(err)
		return -1
	}
	o_ret, err := Py.genericCall(property.Setter, o_args, Object{})
	defer Py.DecRef(o_ret)
	if err != nil {
		Py.GoSetError(err)
		return -1
	}
	return 0
}

// propertyToObject wraps property getter/setter in a Python object.
func propertyToObject(Py Py, a interface{}) (Object, error) {
	p := a.(GoProperty)
	if p.Getter.Fn != nil {
		err := Py.validateGoFunction(p.Getter)
		if err != nil {
			return Object{}, fmt.Errorf("invalid getter: %s", err)
		}
	}
	if p.Setter.Fn != nil {
		err := Py.validateGoFunction(p.Setter)
		if err != nil {
			return Object{}, fmt.Errorf("invalid setter: %s", err)
		}
	}
	if p.GoAttrs == nil {
		p.GoAttrs = GoAttrsMap{}
	}
	return Py.newGoObject(Py.GoProperty_Type(), p)
}

func init() {
	c := GoConvConf{
		TypeOf:   GoProperty{},
		ToObject: propertyToObject,
	}
	atCoreInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(c)
	})
	atCoreFini(func(Py Py) {
		Py.GoDeregisterConversions(c)
	})
}

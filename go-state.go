/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"
import "fmt"

type state struct {
	converters stateConverters
	types      stateTypes
	structs    stateStructs
	fsFinders  stateFSImportFinders
}

func (Py Py) initState(m Object) error {
	Py.initConverters()
	err := Py.callInitializers(m)
	if err != nil {
		return err
	}
	return nil
}

func (Py Py) deinitState() {
	Py.callFinalizers()
}

func (Py Py) getStateFromModule(m Object) *GoHandle {
	return (*GoHandle)(C.PyModule_GetState(m.o))
}

func getStateFromObjectType(t TypeObject) (*state, error) {
	// running without context, cannot use the conversion system
	Py := Py{}
	o_key, err := Py.Unicode_FromString("__go_state__")
	defer Py.DecRef(o_key)
	if err != nil {
		return nil, fmt.Errorf("could not create __go_state__ key: %s", err)
	}
	o_state, err := Py.Object_GetAttr(t.AsObject(), o_key)
	if err != nil {
		return nil, fmt.Errorf("could not get __go_state__: %s", err)
	}
	return getGoObjectHandle(o_state.o).Get().(*goObject).state, nil
}

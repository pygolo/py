/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "_cgo_export.h"
#include "go-object.h"
#include <string.h>

static PyGetSetDef pgl_getset[] = {
	{"__class__", pgl_get_attr, NULL, NULL, "__class__"},
	{"__doc__", pgl_get_attr, pgl_set_attr, NULL, "__doc__"},
	{"__module__", pgl_get_attr, pgl_set_attr, NULL, "__module__"},
	{"__name__", pgl_get_attr, pgl_set_attr, NULL, "__name__"},
	{NULL} /* Sentinel */
};

PyTypeObject *
GoStructBase_Type(PyTypeObject *base)
{
	PyType_Slot slots[] = {
		{Py_tp_base, base},
		{Py_tp_init, pgl_init},
		{Py_tp_getset, pgl_getset},
		{Py_tp_str, pgl_struct_str},
		{Py_tp_repr, pgl_struct_repr},
		{0, NULL} /* Sentinel */
	};

	PyType_Spec spec = {
		.name = "GoStructBase",
		.flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
		.slots = slots,
	};

	return (PyTypeObject *) PyType_FromSpec(&spec);
}

PyTypeObject *
GoStruct_Type(PyTypeObject *base, const char *name, int has_init)
{
	PyType_Slot slots[] = {
		{Py_tp_base, base},
		{Py_tp_getset, pgl_getset},
		{0, NULL} /* Sentinel */
	};

#if PY_VERSION_HEX < 0x030b0000
	// Workaround for Python issue 89478; no longer necessary in Python 3.11
	// See https://github.com/python/cpython/issues/89478
	name = strdup(name);
#endif

	PyType_Spec spec = {
		.name = name,
		.flags = Py_TPFLAGS_DEFAULT,
		.slots = slots,
	};

	if (has_init) {
		spec.flags |= Py_TPFLAGS_BASETYPE;
	}

	PyObject *o = PyType_FromSpec(&spec);

#if PY_VERSION_HEX < 0x030b0000
	// Workaround for Python issue 89478; no longer necessary in Python 3.11
	// See https://github.com/python/cpython/issues/89478
	if (!o) {
		free((void *) name);
	}
#endif

	return (PyTypeObject *) o;
}

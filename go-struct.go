/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
//
// PyTypeObject *GoStructBase_Type(PyTypeObject *);
// PyTypeObject *GoStruct_Type(PyTypeObject *, const char *, int);
import "C"
import (
	"fmt"
	"reflect"
	"sort"
	"strings"
	"unsafe"
)

type stateStructs struct {
	types map[reflect.Type]struct {
		TypeObject
		refcnt int
	}
}

func (Py Py) getStructType(t reflect.Type) (TypeObject, bool) {
	entry, ok := Py.state.structs.types[t]
	return entry.TypeObject, ok
}

func (Py Py) addStructType(t reflect.Type, t_struct TypeObject) {
	if Py.state.structs.types == nil {
		Py.state.structs.types = make(map[reflect.Type]struct {
			TypeObject
			refcnt int
		})
	}
	entry, ok := Py.state.structs.types[t]
	if !ok {
		entry.TypeObject = t_struct
	} else if entry.TypeObject != t_struct {
		panic(fmt.Sprintf("expected %v, got %v", entry.TypeObject, t_struct))
	}
	entry.refcnt++
	Py.state.structs.types[t] = entry
}

func (Py Py) delStructType(t reflect.Type) bool {
	entry, ok := Py.state.structs.types[t]
	if !ok {
		return false
	}
	if entry.refcnt > 1 {
		entry.refcnt--
		Py.state.structs.types[t] = entry
		return false
	}
	delete(Py.state.structs.types, t)
	return true
}

// GoWithPyInit is used to implement a custom Python constructor.
//
// Structs exposed to Python have a default constructor that allows
// setting any/all their exposed fields, either by position or by
// keyword, but if you want something differemt implement this interface.
//
// Example:
//
//	type Point struct {
//		x float64
//		y float64
//		z float64
//	}
//
//	func (p *Point) PyInit(Py py.Py, args, kwargs py.Object) error {
//		// default values
//		p.x = math.E
//		p.y = math.Pi
//		p.z = math.Phi
//
//		vars := py.GoArgsKwArgs{}
//		vars.Optional()
//		vars.Keyword("x", &p.x)
//		vars.Keyword("y", &p.y)
//		vars.Keyword("z", &p.z)
//
//		return Py.Arg_ParseTupleAndKeywords(args, kwargs, vars)
//	}
type GoWithPyInit interface {
	PyInit(Py Py, args, kwargs Object) error
}

// GoStructBase_Type returns the GoStructBase type object.
func (Py Py) GoStructBase_Type() TypeObject {
	return Py.state.types.GoStructBase_Type
}

// GoStructBase_Check returns true if o is of type GoStructBase_Type.
//
// Subtypes of GoStructBase_Type also match this check.
func (Py Py) GoStructBase_Check(o Object) bool {
	return Py.Type_IsSubtype(o.Type(), Py.GoStructBase_Type())
}

// GoStruct describes how a Go struct is exposed to Python.
type GoStruct struct {
	// Struct holds a reference to the Go struct being exposed.
	Struct interface{}

	// GoAttrs provides the get/set attributes protocol of the struct object.
	GoAttrs

	// OmitByDefault determines whether or not members are exported by default.
	OmitByDefault bool

	renames    structRenames
	methods    map[string]GoFunction
	properties map[string]GoProperty
}

// Rename configures a new name for the exported member.
func (s *GoStruct) Rename(orig, new string) {
	if s.renames == nil {
		s.renames = make(structRenames)
	}
	s.renames[orig] = new
}

// Include configures the export of a member.
//
// Use it to export member when OmitByDefault is set to true.
func (s *GoStruct) Include(member string) {
	s.Rename(member, member)
}

// Exclude configures the omission from the exported members.
//
// Use it to hide member when OmitByDefault is set to false.
func (s *GoStruct) Exclude(member string) {
	s.Rename(member, "")
}

// Method adds a new method fn named as name.
func (s *GoStruct) Method(name string, fn GoFunction) {
	if s.methods == nil {
		s.methods = make(map[string]GoFunction)
	}
	s.methods[name] = fn
}

// Property configures a field accessed via getter and setter.
func (s *GoStruct) Property(name string, getter_, setter_ interface{}) {
	if s.properties == nil {
		s.properties = make(map[string]GoProperty)
	}
	getter, ok := getter_.(GoFunction)
	if !ok {
		getter = GoFunction{Fn: getter_}
	}
	setter, ok := setter_.(GoFunction)
	if !ok {
		setter = GoFunction{Fn: setter_}
	}
	s.properties[name] = GoProperty{Getter: getter, Setter: setter}
}

func (Py Py) newGoStructBase_Type() (TypeObject, error) {
	t := TypeObject{C.GoStructBase_Type(Py.GoObject_Type().t)}
	if t.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	return t, nil
}

// GoGetStructType returns the Python type corresponding to struct a.
//
// If a is not a struct or was not previously registered with
// Py.GoRegisterStruct, the returned boolean is false.
//
// The returned TypeObject is a new reference that needs to be
// disposed with Py.DecRef after use.
//
// Example:
//
//	func PrintTypeOf(Py Py, s SomeStruct) {
//		if t, ok := Py.GoGetStructType(s); ok {
//			fmt.Printf("t: %s\n", t.Name())
//			Py.DecRef(t.AsObject())
//		}
//	}
func (Py Py) GoGetStructType(a interface{}) (TypeObject, bool) {
	t, err := normalizeStructType(a)
	if err != nil {
		return TypeObject{}, false
	}
	t_struct, ok := Py.getStructType(t)
	if ok {
		Py.IncRef(t_struct.AsObject())
	}
	return t_struct, ok
}

func normalizeStructType(a interface{}) (reflect.Type, error) {
	var v reflect.Value
	if s, ok := a.(GoStruct); ok {
		v = reflect.ValueOf(s.Struct)
	} else {
		v = reflect.ValueOf(a)
	}
	switch v.Kind() {
	case reflect.Struct:
		return v.Type(), nil
	case reflect.Ptr:
		if v.Type() != nil && v.Type().Elem().Kind() == reflect.Struct {
			return v.Type().Elem(), nil
		}
	}
	return nil, fmt.Errorf("not a struct or a pointer to struct: %s", v.Type())
}

func normalizeStruct(a interface{}) (GoStruct, reflect.Type, error) {
	s, ok := a.(GoStruct)
	if !ok {
		s = GoStruct{Struct: a}
	}
	if s.GoAttrs == nil {
		s.GoAttrs = GoAttrsMap{}
	}
	v := reflect.ValueOf(s.Struct)
	switch v.Kind() {
	case reflect.Struct:
		// allocate a new struct
		ptr := reflect.New(v.Type())
		// copy by value from a
		reflect.Indirect(ptr).Set(v)
		// store a pointer of it
		s.Struct = ptr.Interface()
		return s, v.Type(), nil
	case reflect.Ptr:
		if v.Type() != nil && v.Type().Elem().Kind() == reflect.Struct {
			return s, v.Type().Elem(), nil
		}
	}
	return GoStruct{}, nil, fmt.Errorf("not a struct or a pointer to struct: %T", s.Struct)
}

// GoRegisterStruct examines structs aa and registers converters for them.
//
// When multiple structs are passed, either all or none are registered.
//
// The access to the underlying structures is not synchronized and therefore
// this function must be called during a single threaded deinitialization phase.
func (Py Py) GoRegisterStruct(aa ...interface{}) (err error) {
	var i int
	var a interface{}
	defer func() {
		if err != nil {
			Py.GoDeregisterStruct(aa[:i]...)
		}
	}()
	for i, a = range aa {
		s, t, err := normalizeStruct(a)
		if err != nil {
			return fmt.Errorf("could not normalize struct: %s", err)
		}
		t_struct, ok := Py.getStructType(t)
		if ok {
			Py.addStructType(t, t_struct)
			return nil
		}
		// duplicate the renames map, will check if all the entries are really used
		s.renames = s.renames.clone()
		names, positions, err := getStructFields(s, t)
		if err != nil {
			return fmt.Errorf("could not get struct fields: %s", err)
		}
		t_struct, err = Py.createStructType(s, t, names, positions)
		defer Py.DecRef(t_struct.AsObject())
		if err != nil {
			return fmt.Errorf("could not create struct type: %s", err)
		}
		if len(s.renames) > 0 {
			return fmt.Errorf("could not rename member(s): %s", s.renames)
		}
		subs := Py.getStructSubStructs(t, positions)
		err = Py.GoRegisterStruct(subs...)
		if err != nil {
			return fmt.Errorf("could not register substructs: %s", err)
		}
		ToObject := func(Py pyPy, a interface{}) (Object, error) {
			return structToObjectRegistered(Py, a, t_struct)
		}
		FromObject := func(Py pyPy, o Object, a interface{}) error {
			if o.Type() == t_struct || Py.Type_IsSubtype(o.Type(), t_struct) {
				return structFromTypeObjectRegistered(Py, o, a, t)
			}
			return structFromObjectRegistered(Py, o, a, t, names, positions)
		}
		FromTypeObject := func(Py pyPy, o Object, a interface{}) error {
			return structFromTypeObjectRegistered(Py, o, a, t)
		}
		cc := []GoConvConf{
			// struct
			{FromObject: FromObject, ToObject: ToObject, TypeOf: reflect.Indirect(reflect.New(t)).Interface()},
			// struct pointer
			{FromObject: FromObject, ToObject: ToObject, TypeOf: reflect.New(t).Interface()},
			// struct Python type
			{FromObject: FromTypeObject, TypeObject: t_struct},
		}
		err = Py.GoRegisterConversions(cc...)
		if err != nil {
			Py.GoDeregisterStruct(subs...)
			return fmt.Errorf("could not register conversions: %s", err)
		}
		Py.IncRef(t_struct.AsObject())
		Py.addStructType(t, t_struct)
	}
	return nil
}

func getStructFields(s GoStruct, t reflect.Type) ([]string, []int, error) {
	names := make([]string, 0, t.NumField())
	positions := make([]int, 0, t.NumField())

fields:
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		// Skip field if nonpublic
		if f.PkgPath != "" {
			continue
		}
		tags := strings.Split(f.Tag.Get("python"), ",")
		var unknown []string
		for _, tag := range tags[1:] {
			tag = strings.TrimSpace(tag)
			// Skip field if to be omitted
			if tag == "omit" {
				continue fields
			}
			unknown = append(unknown, tag)
		}
		if len(unknown) > 0 {
			return nil, nil, fmt.Errorf("field '%s' has unknown tags: %v", f.Name, unknown)
		}
		name, ok := s.renames[f.Name]
		delete(s.renames, f.Name)
		if ok && name == "" {
			continue
		}
		if !ok && s.OmitByDefault {
			continue
		}
		if name == "" {
			name = strings.TrimSpace(tags[0])
		}
		if name == "" {
			name = f.Name
		}
		names = append(names, name)
		positions = append(positions, i)
	}
	return names, positions, nil
}

func (Py Py) getStructSubStructs(t reflect.Type, positions []int) []interface{} {
	subs := make([]interface{}, 0, len(positions))
	v := reflect.New(t).Elem()
	for _, pos := range positions {
		f := v.Field(pos)
		if f.Kind() == reflect.Struct || f.Kind() == reflect.Ptr && f.Type() != nil && f.Type().Elem().Kind() == reflect.Struct {
			subs = append(subs, f.Interface())
		}
	}
	return subs
}

func (Py Py) createStructType(s GoStruct, t reflect.Type, names []string, positions []int) (TypeObject, error) {
	has_init := C.int(1)
	if reflect.ValueOf(s.Struct).IsNil() {
		has_init = 0
	}
	c_name := C.CString(t.Name())
	defer C.free(unsafe.Pointer(c_name))
	t_struct := TypeObject{C.GoStruct_Type(Py.state.types.GoStructBase_Type.t, c_name, has_init)}
	defer Py.DecRef(t_struct.AsObject())
	if t_struct.t == nil {
		return TypeObject{}, Py.GoCatchError()
	}
	__init__ := GoFunction{
		Fn: func(Py pyPy, args, kwargs Object) error {
			if has_init == 0 {
				msg := fmt.Sprintf("cannot create '%s' instances", t_struct.Name())
				return &GoError{"TypeError", msg, nil}
			}
			self, err := Py.Tuple_GetItem(args, 0)
			if err != nil {
				return &GoError{"SystemError", "could not find self", nil}
			}
			length, err := Py.Object_Length(args)
			if err != nil {
				return &GoError{"SystemError", "could not get args size", nil}
			}
			args, err = Py.Tuple_GetSlice(args, 1, length)
			defer Py.DecRef(args)
			if err != nil {
				return &GoError{"SystemError", "could not slice out self", nil}
			}
			v := reflect.New(t)
			s := GoStruct{Struct: v.Interface(), GoAttrs: GoAttrsMap{}}
			err = s.PySetAttr(Py, "__class__", self.Type())
			if err != nil {
				return err
			}
			insertGoStruct(self, s)
			if s, ok := s.Struct.(GoWithPyInit); ok {
				err = s.PyInit(Py, args, kwargs)
				if err != nil {
					return err
				}
			} else {
				v := reflect.Indirect(v)
				vars := GoArgsKwArgs{}
				vars.AllowUnknownKeywords()
				vars.Optional()
				for i, name := range names {
					vars.Keyword(name, v.Field(positions[i]).Addr().Interface())
				}
				err = Py.Arg_ParseTupleAndKeywords(args, kwargs, vars)
				if err != nil {
					return err
				}
			}
			o_builtins, err := Py.Import_Import("builtins")
			defer Py.DecRef(o_builtins)
			if err != nil {
				return fmt.Errorf("could not import 'builtins': %s", err)
			}
			o_super, err := Py.Object_CallMethod(o_builtins, "super", t_struct.AsObject(), self)
			defer Py.DecRef(o_super)
			if err != nil {
				return err
			}
			o_super_init, err := Py.Object_GetAttr(o_super, "__init__")
			defer Py.DecRef(o_super_init)
			if err != nil {
				return err
			}
			o_ret, err := Py.Object_Call(o_super_init, GoArgs{}, kwargs)
			defer Py.DecRef(o_ret)
			return err
		},
		InterpreterAccess: true,
		naked:             true,
	}
	err := Py.Object_SetAttr(t_struct.AsObject(), "__init__", __init__)
	if err != nil {
		return TypeObject{}, err
	}
	err = Py.addStructMethods(t_struct, t, s)
	if err != nil {
		return TypeObject{}, fmt.Errorf("could not add struct methods: %s", err)
	}
	err = Py.addStructFields(t_struct, t, s, names, positions)
	if err != nil {
		return TypeObject{}, fmt.Errorf("could not add struct fields: %s", err)
	}
	Py.IncRef(t_struct.AsObject())
	return t_struct, nil
}

func (Py Py) addStructMethods(t_struct TypeObject, t reflect.Type, s GoStruct) error {
	// want also methods with pointer receiver
	t = reflect.PtrTo(t)
	for i := 0; i < t.NumMethod(); i++ {
		m := t.Method(i)
		name, ok := s.renames[m.Name]
		delete(s.renames, m.Name)
		if ok && name == "" {
			continue
		}
		if !ok && s.OmitByDefault {
			continue
		}
		if name == "" {
			name = m.Name
		}
		err := Py.Object_SetAttr(t_struct.AsObject(), name, m.Func.Interface())
		if err != nil {
			return err
		}
	}
	for name, method := range s.methods {
		err := Py.Object_SetAttr(t_struct.AsObject(), name, method)
		if err != nil {
			return fmt.Errorf("could not add method '%s': %s", name, err)
		}
	}
	return nil
}

func (Py Py) addStructFields(t_struct TypeObject, t reflect.Type, s GoStruct, names []string, positions []int) error {
	for i, name := range names {
		o_field, err := Py.newStructField(t, positions[i])
		defer Py.DecRef(o_field)
		if err != nil {
			return err
		}
		err = Py.Object_SetAttr(t_struct.AsObject(), name, o_field)
		if err != nil {
			return err
		}
	}
	for name, property := range s.properties {
		err := Py.Object_SetAttr(t_struct.AsObject(), name, property)
		if err != nil {
			return err
		}
	}
	return nil
}

func insertGoStruct(o Object, s GoStruct) {
	g := getGoObjectHandle(o.o).Get().(*goObject)
	for i, value := range g.values {
		if value, ok := value.(GoStruct); ok {
			if reflect.TypeOf(value.Struct) == reflect.TypeOf(s.Struct) {
				g.values[i] = s
				return
			}
		}
	}
	g.values = append(g.values, s)
}

// GoDeregisterStruct removes the converters for structs aa.
//
// The passed structs are deregistered in the reverse order.
//
// You need to call GoDeregisterStruct as many times as GoRegisterStruct
// was previously called before the struct is effectively unregistered.
//
// The access to the underlying structures is not synchronized and therefore
// this function must be called during a single threaded deinitialization phase.
func (Py Py) GoDeregisterStruct(aa ...interface{}) {
	for i := len(aa) - 1; i >= 0; i-- {
		s, t, _ := normalizeStruct(aa[i])
		t_struct, _ := Py.getStructType(t)
		if Py.delStructType(t) {
			_, positions, _ := getStructFields(s, t)
			subs := Py.getStructSubStructs(t, positions)
			Py.GoDeregisterStruct(subs...)
			cc := []GoConvConf{
				{TypeOf: reflect.Indirect(reflect.New(t)).Interface()},
				{TypeOf: reflect.New(t).Interface()},
				{TypeObject: t_struct},
			}
			Py.GoDeregisterConversions(cc...)
			Py.DecRef(t_struct.AsObject())
		}
	}
}

// GoWithPyStr is used to customize the value returned by Python str(x).
type GoWithPyStr interface {
	PyStr() (string, error)
}

//export pgl_struct_str
func pgl_struct_str(self_ *C.PyObject) *C.PyObject {
	var self GoStruct
	Py, err := extendObject(self_, extractGoValue(&self))
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	s, ok := self.Struct.(GoWithPyStr)
	if !ok {
		return pgl_struct_repr(self_)
	}
	var str string
	str, err = s.PyStr()
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	o_ret, err := Py.GoToObject(str)
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	return o_ret.o
}

// GoWithPyRepr is used to customize the value returned by Python repr(x).
type GoWithPyRepr interface {
	PyRepr() (string, error)
}

//export pgl_struct_repr
func pgl_struct_repr(self_ *C.PyObject) *C.PyObject {
	var self GoStruct
	Py, err := extendObject(self_, extractGoValue(&self))
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	var repr string
	switch s := self.Struct.(type) {
	case GoWithPyRepr:
		repr, err = s.PyRepr()
		if err != nil {
			Py.GoSetError(err)
			return nil
		}
	case fmt.Stringer:
		repr = s.String()
	default:
		repr = fmt.Sprintf("%#v", s)
	}
	o_ret, err := Py.GoToObject(repr)
	if err != nil {
		Py.GoSetError(err)
		return nil
	}
	return o_ret.o
}

type structRenames map[string]string

func (r structRenames) clone() structRenames {
	if r != nil {
		new := make(structRenames, len(r))
		for k, v := range r {
			new[k] = v
		}
		r = new
	}
	return r
}

func (r structRenames) String() string {
	members := make([]string, 0, len(r))
	for k := range r {
		members = append(members, k)
	}
	sort.Strings(members)
	return strings.Join(members, ", ")
}

func extractGoStruct(out_ interface{}) goObjectVisitor {
	out := reflect.ValueOf(out_)
	if out.Kind() != reflect.Ptr {
		return nil
	}
	out = reflect.Indirect(out)
	switch out.Kind() {
	case reflect.Interface:
	case reflect.Struct:
	case reflect.Ptr:
		if out.Type().Elem().Kind() != reflect.Struct {
			return nil
		}
	default:
		return nil
	}
	fn := func(in_ interface{}) bool {
		if in_, ok := in_.(GoStruct); ok {
			in := reflect.ValueOf(in_.Struct)
			if out.Kind() == reflect.Struct {
				in = reflect.Indirect(in)
			}
			if in.Type() == out.Type() || out.Kind() == reflect.Interface {
				out.Set(in)
				return true
			}
		}
		return false
	}
	return fn
}

func structToObjectRegistered(Py Py, a interface{}, t_struct TypeObject) (Object, error) {
	s, _, err := normalizeStruct(a)
	if err != nil {
		return Object{}, err
	}
	return Py.newGoObject(t_struct, s)
}

func structFromObjectRegistered(Py Py, o Object, a interface{}, t reflect.Type, names []string, positions []int) error {
	v := reflect.ValueOf(a).Elem()
	switch v.Kind() {
	case reflect.Struct:
		if v.Type() != t {
			return Py.GoErrorConvFromObject(o, a)
		}
	case reflect.Interface:
		v = reflect.Indirect(reflect.New(t))
		*a.(*interface{}) = v.Interface()
	default:
		return Py.GoErrorConvFromObject(o, a)
	}
	return structFromObjectGeneric(Py, o, v, names, positions)
}

func structFromTypeObjectRegistered(Py Py, o Object, a interface{}, t reflect.Type) error {
	var self interface{}
	if !traverseGoObject(o.o, extractGoStruct(&self)) {
		return Py.GoErrorConvFromObject(o, a)
	}
	s := reflect.ValueOf(self)
	v := reflect.ValueOf(a).Elem()
	switch v.Kind() {
	case reflect.Interface:
	case reflect.Struct:
		if v.Type() != t {
			return Py.GoErrorConvFromObject(o, a)
		}
		s = reflect.Indirect(s)
	case reflect.Ptr:
		if v.Type() == nil || v.Type().Elem() != t {
			return Py.GoErrorConvFromObject(o, a)
		}
	default:
		return Py.GoErrorConvFromObject(o, a)
	}
	v.Set(s)
	return nil
}

func structToObjectUnregistered(Py Py, a interface{}) (Object, error) {
	s, t, err := normalizeStruct(a)
	if err != nil {
		return Object{}, err
	}
	if t_struct, ok := Py.getStructType(t); ok {
		return structToObjectRegistered(Py, a, t_struct)
	}
	// duplicate the renames map, will check if all the entries are really used
	s.renames = s.renames.clone()
	names, positions, err := getStructFields(s, t)
	if err != nil {
		return Object{}, fmt.Errorf("could not get struct fields: %s", err)
	}
	t_struct, err := Py.createStructType(s, t, names, positions)
	defer Py.DecRef(t_struct.AsObject())
	if err != nil {
		return Object{}, err
	}
	if len(s.renames) > 0 {
		return Object{}, fmt.Errorf("could not rename member(s): %s", s.renames)
	}
	return Py.newGoObject(t_struct, s)
}

func structFromObjectUnregistered(Py Py, o Object, a interface{}) error {
	v := reflect.ValueOf(a).Elem()
	s, t, err := normalizeStruct(v.Interface())
	if err != nil {
		return err
	}
	// see if we can do an extraction
	if Py.GoStructBase_Check(o) && traverseGoObject(o.o, extractGoStruct(a)) {
		return nil
	}
	// no extraction was possible, attempt a conversion but that can be done only by value
	if v.Kind() == reflect.Ptr {
		return Py.GoErrorConvFromObject(o, a)
	}
	names, positions, err := getStructFields(s, t)
	if err != nil {
		return err
	}
	return structFromObjectGeneric(Py, o, v, names, positions)
}

func structFromObjectGeneric(Py Py, o Object, v reflect.Value, names []string, positions []int) error {
	// search every exposed field of the Go struct as attribute in the Python object
	for i, name := range names {
		o_value, err := Py.Object_GetAttr(o, name)
		defer Py.DecRef(o_value)
		if err != nil {
			return err
		}
		f := v.Field(positions[i]).Addr()
		err = Py.GoFromObject(o_value, f.Interface())
		if err != nil {
			return fmt.Errorf("attr \"%s\": %s", name, err)
		}
	}
	return nil
}

func init() {
	cc := []GoConvConf{
		{
			Kind:     reflect.Struct,
			TypeOf:   GoStruct{},
			ToObject: structToObjectUnregistered,
		}, {
			Kind:       reflect.Struct,
			FromObject: structFromObjectUnregistered,
		},
	}
	atCoreInit(func(Py Py, m Object) error {
		return Py.GoRegisterConversions(cc...)
	})
	atCoreFini(func(Py Py) {
		Py.GoDeregisterConversions(cc...)
	})
}

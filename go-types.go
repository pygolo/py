/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

type stateTypes struct {
	GoObject_Type      TypeObject
	GoFunction_Type    TypeObject
	GoProperty_Type    TypeObject
	GoStructBase_Type  TypeObject
	GoStructField_Type TypeObject
}

type typeDescr struct {
	Type   *TypeObject
	Init   func() (TypeObject, error)
	Deinit func(TypeObject)
}

func (Py Py) getTypesDescr() []typeDescr {
	types := &Py.state.types
	return []typeDescr{
		{&types.GoObject_Type, Py.newGoObject_Type, nil},
		{&types.GoFunction_Type, Py.newGoFunction_Type, Py.delGoFunction_Type},
		{&types.GoProperty_Type, Py.newGoProperty_Type, nil},
		{&types.GoStructBase_Type, Py.newGoStructBase_Type, nil},
		{&types.GoStructField_Type, Py.newGoStructField_Type, nil},
	}
}

func init() {
	atCoreInit(func(Py Py, m Object) error {
		for _, d := range Py.getTypesDescr() {
			t, err := d.Init()
			defer Py.DecRef(t.AsObject())
			if err != nil {
				return err
			}
			if m.o != nil {
				err := Py.moduleAddType(m, t)
				if err != nil {
					return err
				}
			}
			Py.IncRef(t.AsObject())
			*d.Type = t
		}
		return nil
	})

	atCoreFini(func(Py Py) {
		dd := Py.getTypesDescr()
		for i := len(dd) - 1; i >= 0; i-- {
			d := dd[i]
			if d.Deinit != nil {
				d.Deinit(*d.Type)
			}
			Py.DecRef(d.Type.AsObject())
			*d.Type = TypeObject{}
		}
	})
}

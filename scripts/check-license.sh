#!/usr/bin/env bash
#
#  Copyright 2022, Pygolo Project contributors
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

if [ $# -eq 0 ]; then
	# call self with all the file names in HEAD as arguments
	git ls-tree -r -z --name-only HEAD | xargs -0 $0
	exit $?
fi

# load the license as array of lines
IFS=$'\n' read -r -d '' -a LICENSE << EOL
Copyright 2022, Pygolo Project contributors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
EOL

# return true if the file arg matches any of the ignore patterns
ignored_file()
{
	while read PATTERN; do
		if [[ "$1" == $PATTERN ]]; then
			return 0
		fi
	done < .license-ignore
	return 1
}

# return true if the file arg does not contain the expected license (ignore empty files)
wrong_license()
{
	if [ -s "$1" ]; then
		for line in "${LICENSE[@]}"; do
			if ! grep -q "$line" "$1"; then
				return 0
			fi
		done
	fi
	return 1
}

# scan all the file arguments, check their license
WRONG=0
while [ $# -gt 0 ]; do
	if ! ignored_file "$1" && wrong_license "$1"; then
		echo "$1" >&2
		WRONG=$(( WRONG + 1 ))
	fi
	shift
done

# fail if any file with the wrong or no license is found
if [ $WRONG -gt 0 ]; then
	echo "$WRONG file(s) with wrong license" >&2
	exit 1
fi

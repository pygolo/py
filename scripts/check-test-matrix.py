#!/usr/bin/env python3
#
#  Copyright 2022, Pygolo Project contributors
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import sys
from pathlib import Path

if len(sys.argv) != 4:
    prog_name = Path(sys.argv[0]).stem
    print("Usage: %s <platform_name> <go_ver> <py_ver>" % prog_name, file=sys.stderr)
    sys.exit(1)

platform_name = sys.argv[1]
go_ver = sys.argv[2][2:]
py_ver = sys.argv[3]
go_ver_short = ".".join(go_ver.split(".")[:2])
py_ver_short = ".".join(py_ver.split(".")[:2])

json_table = None
with open("docs/TEST-MATRIX.md", encoding="utf8") as f:
    for line in f.readlines():
        if line.startswith("```"):
            if json_table is None:
                json_table = []
            else:
                break
        elif json_table is not None:
            json_table.append(line)

match = False
mismatches = []
platforms = set()
duplicates = set()
for row in json.loads("".join(json_table))["items"]:
    for k, v in row.items():
        row_platforms = set(x.lower().strip() for x in v.split(","))
        duplicates |= platforms & row_platforms
        platforms |= row_platforms
        if platform_name.lower() in row_platforms:
            if row["go"] == go_ver_short and k == py_ver_short:
                match = True
            if row["go"] != go_ver_short:
                mismatches.append("go_ver mismatch: wanted %s, got %s" % (row["go"], go_ver_short))
            if k != py_ver_short:
                mismatches.append("py_ver mismatch: wanted %s, got %s" % (k, py_ver_short))

status = 0
if duplicates:
    print("duplicate platforms: " + ", ".join(sorted(duplicates)), file=sys.stderr)
    status = 1
if mismatches:
    print("\n".join(mismatches), file=sys.stderr)
    status = 1
if not match and not status:
    print("platform not found: " + platform_name, file=sys.stderr)
    status = 1
sys.exit(status)

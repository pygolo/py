#!/usr/bin/env python3
#
#  Copyright 2022, Pygolo Project contributors
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sysconfig

print(
    """\
prefix=%s
exec_prefix=${prefix}
libdir=${exec_prefix}/libs
includedir=${exec_prefix}/include

Name: Python
Description: Embed/extend Python on Windows
Requires:
Version: %s
Libs: -L"${libdir}" -lpython%s
Cflags: -I"${includedir}"\
"""
    % (
        sysconfig.get_config_var("prefix"),
        sysconfig.get_config_var("py_version"),
        sysconfig.get_config_var("VERSION"),
    )
)

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestArg_ParseTuple(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_d, err := Py.Dict_New()
	defer Py.DecRef(o_d)
	expectError(t, err, nil)
	cnt := Py.RefCnt(o_d)
	defer func() {
		expect(t, Py.RefCnt(o_d), cnt)
	}()

	o_t, err := Py.Tuple_Pack(o_d, o_d)
	defer Py.DecRef(o_t)
	expectError(t, err, nil)
	expect(t, Py.RefCnt(o_d), cnt+2)

	var o, o2 py.Object
	err = Py.Arg_ParseTuple(o_t, &o, &o2)
	defer Py.DecRef(o, o2)
	expectError(t, err, nil)
	expect(t, Py.RefCnt(o), cnt+4)
	expect(t, Py.RefCnt(o2), cnt+4)
	expect(t, Py.RefCnt(o_d), cnt+4)
	expect(t, o, o_d)
	expect(t, o2, o_d)

	var f float64
	err = Py.Arg_ParseTuple(o_t, &o, &f)
	expectError(t, err, fmt.Errorf("arg #1: cannot convert a Python dict to Go float64"))
	expect(t, Py.RefCnt(o), cnt+4)
}

func TestArg_ParseTupleAndKeywords(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	v := make([]interface{}, 16)
	z := make([]interface{}, len(v))

	for i, x := range []struct {
		args   py.GoArgs
		kwargs py.GoKwArgs
		leftkw py.GoKwArgs
		vars   py.GoArgsKwArgs
		dflt   []interface{}
		test   []interface{}
		err    error
	}{
		{}, // #0
		{
			vars: *(&py.GoArgsKwArgs{}).
				AllowUnknownKeywords(),
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Optional(),
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly(),
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				KeywordOnly(),
		},
		{ // #5
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				Optional(),
			err: &py.GoError{"SystemError", "unexpected Optional delimiter at #1", nil},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Optional(),
			err: &py.GoError{"SystemError", "unexpected Optional delimiter at #1", nil},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				KeywordOnly(),
			err: &py.GoError{"SystemError", "unexpected KeywordOnly delimiter at #1", nil},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]).
				Positional(&v[1]),
			err: &py.GoError{"SystemError", "unexpected positional variable parsing at #1", nil},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				Positional(&v[0]),
			err: &py.GoError{"SystemError", "unexpected positional variable parsing at #1", nil},
		},
		{ // #10
			args: py.GoArgs{"p0"},
			err:  &py.GoError{"TypeError", "takes 0 positional arguments but 1 was given", nil},
		},
		{
			args: py.GoArgs{"p0", "p1", "p2"},
			err:  &py.GoError{"TypeError", "takes 0 positional arguments but 3 were given", nil},
		},
		{
			kwargs: py.GoKwArgs{"k0": "v0"},
			err:    &py.GoError{"TypeError", "got an unexpected keyword argument 'k0'", nil},
		},
		{
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				AllowUnknownKeywords(),
			leftkw: py.GoKwArgs{"k0": "v0"},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]),
			err: &py.GoError{"TypeError", "takes 1 positional argument but 0 were given", nil},
		},
		{ // #15
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]),
			err: &py.GoError{"TypeError", "takes 1 positional argument but 0 were given", nil},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Keyword("k0", &v[1]),
			err: &py.GoError{"TypeError", "takes 2 positional arguments but 0 were given", nil},
		},
		{
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Keyword("k0", &v[1]),
			err: &py.GoError{"TypeError", "takes 1 positional argument but 0 were given", nil},
		},
		{
			args:   py.GoArgs{"p0"},
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Keyword("k0", &v[1]).
				Keyword("k1", &v[2]),
			err: &py.GoError{"TypeError", "takes 2 positional arguments but 1 was given", nil},
		},
		{
			args: py.GoArgs{"p0", "p1"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Positional((*int)(nil)),
			err: &py.GoError{"TypeError", "positional #1: cannot store in <nil>, need a pointer", nil},
		},
		{ // #20
			args:   py.GoArgs{"p0"},
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Keyword("k0", (*int)(nil)),
			err: &py.GoError{"TypeError", "keyword 'k0': cannot store in <nil>, need a pointer", nil},
		},
		{
			args: py.GoArgs{"p0"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]),
			test: []interface{}{"p0"},
		},
		{
			args: py.GoArgs{"p0"},
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]),
			test: []interface{}{"p0"},
		},
		{
			args: py.GoArgs{"p0", "p1"},
			vars: *(&py.GoArgsKwArgs{}).
				Positional(&v[0]).
				Keyword("k0", &v[1]),
			test: []interface{}{"p0", "p1"},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Positional(&v[0]),
			dflt: []interface{}{"d0"},
		},
		{ // #25
			args: py.GoArgs{"p0"},
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Positional(&v[0]),
			dflt: []interface{}{"d0"},
			test: []interface{}{"p0"},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Keyword("k0", &v[0]),
			dflt: []interface{}{"d0"},
		},
		{
			args: py.GoArgs{"p0"},
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Keyword("k0", &v[0]),
			dflt: []interface{}{"d0"},
			test: []interface{}{"p0"},
		},
		{
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				Optional().
				Keyword("k0", &v[0]),
			dflt: []interface{}{"d0"},
			test: []interface{}{"v0"},
		},
		{
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				Keyword("k0", &v[0]),
			dflt: []interface{}{"d0"},
		},
		{ // #30
			args: py.GoArgs{"p0"},
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				Keyword("k0", &v[0]),
			err: &py.GoError{"TypeError", "takes 0 positional arguments but 1 was given", nil},
		},
		{
			kwargs: py.GoKwArgs{"k0": "v0"},
			vars: *(&py.GoArgsKwArgs{}).
				KeywordOnly().
				Keyword("k0", &v[0]),
			dflt: []interface{}{"d0"},
			test: []interface{}{"v0"},
		},
		{
			args:   py.GoArgs{"p0"},
			kwargs: py.GoKwArgs{"k1": "v1"},
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]).
				Keyword("k1", &v[1]),
			dflt: []interface{}{"d0", "d1"},
			test: []interface{}{"p0", "v1"},
		},
		{
			args:   py.GoArgs{"p0"},
			kwargs: py.GoKwArgs{"k0": "v0", "k1": "v1"},
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]).
				Keyword("k1", &v[1]),
			err: &py.GoError{"TypeError", "got multiple values for argument 'k0'", nil},
		},
		{
			args:   py.GoArgs{"p0", "p1"},
			kwargs: py.GoKwArgs{"k0": "v0", "k1": "v1", "k2": "v2"},
			vars: *(&py.GoArgsKwArgs{}).
				Keyword("k0", &v[0]).
				Keyword("k1", &v[1]),
			err: &py.GoError{"TypeError", "got multiple values for argument 'k0'", nil},
		},
	} {
		expectSubtest(t, Py, fmt.Sprintf("#%d", i), func(t *testing.T, Py py.Py) {
			if x.dflt == nil {
				copy(v, z)
			} else {
				copy(v, x.dflt)
			}

			expectParseTupleAndKeywords(t, Py, x.args, x.kwargs, x.leftkw, x.vars, x.err)

			if x.test == nil {
				copy(x.test, x.dflt)
			}
			for i, test := range x.test {
				expectElem(t, i, v[i], test)
			}
		})
	}

	expectSubtest(t, Py, "RefCnt", func(t *testing.T, Py py.Py) {
		o_d, err := Py.Dict_New()
		defer Py.DecRef(o_d)
		expectError(t, err, nil)
		cnt := Py.RefCnt(o_d)
		defer func() {
			expect(t, Py.RefCnt(o_d), cnt)
		}()

		o := py.Object{}

		vars := &py.GoArgsKwArgs{}
		vars.Optional()
		vars.Positional(&o)
		vars.KeywordOnly()
		vars.Keyword("o", &o)

		expectParseTupleAndKeywords(t, Py, py.GoArgs{o_d}, nil, nil, *vars, nil)
		defer Py.DecRef(o, o)
		expect(t, Py.RefCnt(o), cnt+2)
		expect(t, Py.RefCnt(o_d), cnt+2)
		expect(t, o, o_d)

		expectParseTupleAndKeywords(t, Py, nil, nil, nil, *vars, nil)
		defer Py.DecRef(o, o)
		expect(t, Py.RefCnt(o), cnt+4)
		expect(t, Py.RefCnt(o_d), cnt+4)
		expect(t, o, o_d)

		f := 3.14
		vars.Keyword("f", &f)
		expectParseTupleAndKeywords(t, Py, py.GoArgs{o_d}, py.GoKwArgs{"o": o_d, "f": o_d}, nil, *vars,
			&py.GoError{"TypeError", "keyword 'f': cannot convert a Python dict to Go float64", nil})
		expect(t, Py.RefCnt(o), cnt+4)
		expect(t, Py.RefCnt(o_d), cnt+4)
		expect(t, o, o_d)
		expect(t, f, 3.14)

		expectParseTupleAndKeywords(t, Py, py.GoArgs{o_d}, py.GoKwArgs{"g": o_d}, nil, *vars,
			&py.GoError{"TypeError", "got an unexpected keyword argument 'g'", nil})
		expect(t, Py.RefCnt(o), cnt+4)
		expect(t, Py.RefCnt(o_d), cnt+4)
		expect(t, o, o_d)
		expect(t, f, 3.14)
	})
}

func expectParseTupleAndKeywords(t *testing.T, Py py.Py, args py.GoArgs, kwargs, leftkw py.GoKwArgs, vars py.GoArgsKwArgs, expected error) {
	t.Helper()

	if args == nil {
		args = py.GoArgs{}
	}
	if kwargs == nil {
		kwargs = py.GoKwArgs{}
	}
	if leftkw == nil {
		leftkw = py.GoKwArgs{}
	}

	o_args, err := Py.GoToObject(args)
	defer Py.DecRef(o_args)
	expectError(t, err, nil)

	o_kwargs, err := Py.GoToObject(kwargs)
	defer Py.DecRef(o_kwargs)
	expectError(t, err, nil)

	err = Py.Arg_ParseTupleAndKeywords(o_args, o_kwargs, vars)
	expectError(t, err, expected)

	if expected == nil {
		expectObject(t, Py, o_kwargs, leftkw, nil)
	}
}

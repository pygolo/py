/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestBool(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expect(t, py.True.Type(), py.Bool_Type)
	expect(t, py.False.Type(), py.Bool_Type)

	expect(t, Py.Bool_Check(py.True), true)
	expect(t, Py.Bool_Check(py.False), true)
	expect(t, Py.Bool_Check(py.None), false)

	test, err := Py.Object_IsTrue(py.True)
	expectError(t, err, nil)
	expect(t, test, true)

	test, err = Py.Object_IsTrue(py.False)
	expectError(t, err, nil)
	expect(t, test, false)

	test, err = Py.Object_IsTrue(py.None)
	expectError(t, err, nil)
	expect(t, test, false)
}

func TestBoolToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.GoToObject(true)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o, py.True)

	o, err = Py.GoToObject(false)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o, py.False)
}

func TestBoolFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expectObject(t, Py, py.True, true, nil)
	expectObject(t, Py, py.False, false, nil)
	expectObject(t, Py, py.None, false, nil)

	expectObjectAny(t, Py, py.True, true, nil)
	expectObjectAny(t, Py, py.False, false, nil)
	expectObjectAny(t, Py, py.None, nil, fmt.Errorf("cannot convert a Python NoneType to Go interface {}"))
}

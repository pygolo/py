/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestBuffer(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_buf, err := Py.NewGoBuffer(-1)
	expectError(t, err, fmt.Errorf("invalid buffer size: -1"))

	o_buf, err = Py.NewGoBuffer(0)
	defer Py.DecRef(o_buf)
	expectError(t, err, nil)

	size := 11

	o_buf, err = Py.NewGoBuffer(size)
	defer Py.DecRef(o_buf)
	expectError(t, err, nil)
	cnt := Py.RefCnt(o_buf)

	var buf py.Buffer
	expect(t, buf.UnsafeSlice(), []byte(nil))

	err = Py.Object_GetBuffer(o_buf, &buf, py.Buf_simple)
	expectError(t, err, nil)
	expect(t, Py.RefCnt(o_buf), cnt+1)
	expect(t, len(buf.UnsafeSlice()), size)
	for _, x := range buf.UnsafeSlice() {
		expect(t, x, byte(0))
	}

	f := func(b py.Buffer) {
		expect(t, Py.RefCnt(o_buf) > cnt+1, true)
		expect(t, len(b.UnsafeSlice()), size)
		data := b.UnsafeSlice()
		for i, x := range data {
			expect(t, x, byte(0))
			data[i] = byte(i)
		}
	}
	o_f, err := Py.GoToObject(f)
	defer Py.DecRef(o_f)
	expectError(t, err, nil)
	expect(t, Py.RefCnt(o_buf), cnt+1)

	o_ret, err := Py.Object_CallFunction(o_f, o_buf)
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)
	expect(t, Py.RefCnt(o_buf), cnt+1)

	for i, x := range buf.UnsafeSlice() {
		expect(t, x, byte(i))
	}

	Py.Buffer_Release(&buf)
	expect(t, buf.UnsafeSlice(), []byte(nil))
	expect(t, Py.RefCnt(o_buf), cnt)
}

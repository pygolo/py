/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"testing"

	"gitlab.com/pygolo/py"
)

var bytesTestCases = []string{
	``,
	`Hello, 世界`,
	`Hello, world!\0`,
	`With\0 null\0 chars\0`,
}

func TestBytesToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range bytesTestCases {
		o, err := Py.GoToObject([]byte(tc))
		defer Py.DecRef(o)
		expectError(t, err, nil)

		o_str, err := Py.Object_Call(py.Unicode_Type.AsObject(), py.GoArgs{o}, py.GoKwArgs{"encoding": "utf-8"})
		defer Py.DecRef(o_str)
		expectError(t, err, nil)
		expectObject(t, Py, o_str, tc, nil)
	}
}

func TestBytesFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range bytesTestCases {
		o, err := Py.Object_Call(py.Bytes_Type.AsObject(), py.GoArgs{tc}, py.GoKwArgs{"encoding": "utf-8"})
		defer Py.DecRef(o)
		expectError(t, err, nil)

		var b []byte
		err = Py.GoFromObject(o, &b)
		expectError(t, err, nil)
		expect(t, b, []byte(tc))
	}
}

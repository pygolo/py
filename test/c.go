/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

// #include <limits.h>
// #include <stdint.h>
// #include <float.h>
import "C"
import "unsafe"

func cINT_SIZE() uintptr     { return unsafe.Sizeof(C.int(0)) }
func cUINT_SIZE() uintptr    { return unsafe.Sizeof(C.uint(0)) }
func cLONG_SIZE() uintptr    { return unsafe.Sizeof(C.long(0)) }
func cULONG_SIZE() uintptr   { return unsafe.Sizeof(C.ulong(0)) }
func cLLONG_SIZE() uintptr   { return unsafe.Sizeof(C.longlong(0)) }
func cULLONG_SIZE() uintptr  { return unsafe.Sizeof(C.ulonglong(0)) }
func cUINTPTR_SIZE() uintptr { return unsafe.Sizeof(C.uintptr_t(0)) }

func cINT_MIN() int64       { return int64(C.INT_MIN) }
func cINT_MAX() int64       { return int64(C.INT_MAX) }
func cUINT_MAX() uint64     { return uint64(C.UINT_MAX) }
func cLONG_MIN() int64      { return int64(C.LONG_MIN) }
func cLONG_MAX() int64      { return int64(C.LONG_MAX) }
func cULONG_MAX() uint64    { return uint64(C.ULONG_MAX) }
func cLLONG_MIN() int64     { return int64(C.LLONG_MIN) }
func cLLONG_MAX() int64     { return int64(C.LLONG_MAX) }
func cULLONG_MAX() uint64   { return uint64(C.ULLONG_MAX) }
func cUINTPTR_MAX() uintptr { return uintptr(C.UINTPTR_MAX) }

func cFLT_MIN() float32 { return float32(C.FLT_MIN) }
func cFLT_MAX() float32 { return float32(C.FLT_MAX) }
func cDBL_MIN() float64 { return float64(C.DBL_MIN) }
func cDBL_MAX() float64 { return float64(C.DBL_MAX) }

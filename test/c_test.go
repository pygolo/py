/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"math"
	"runtime"
	"testing"
	"unsafe"
)

const (
	// for older Go versions (copied from Go 1.17)
	intSize    = 32 << (^uint(0) >> 63) // 32 or 64
	maxInt     = 1<<(intSize-1) - 1
	minInt     = -1 << (intSize - 1)
	maxUint    = 1<<intSize - 1
	maxUintptr = maxUint
)

func TestC(t *testing.T) {
	expect(t, cINT_SIZE(), unsafe.Sizeof(int32(0)))
	expect(t, cUINT_SIZE(), unsafe.Sizeof(uint32(0)))
	if runtime.GOOS == "windows" {
		expect(t, cLONG_SIZE(), unsafe.Sizeof(int32(0)))
		expect(t, cULONG_SIZE(), unsafe.Sizeof(uint32(0)))
	} else {
		expect(t, cLONG_SIZE(), unsafe.Sizeof(int(0)))
		expect(t, cULONG_SIZE(), unsafe.Sizeof(uint(0)))
	}
	expect(t, cLLONG_SIZE(), unsafe.Sizeof(int64(0)))
	expect(t, cULLONG_SIZE(), unsafe.Sizeof(uint64(0)))
	expect(t, cUINTPTR_SIZE(), unsafe.Sizeof(uintptr(0)))
	expect(t, cUINTPTR_SIZE(), unsafe.Sizeof(uint(0)))

	expect(t, cINT_MIN(), int64(math.MinInt32))
	expect(t, cINT_MAX(), int64(math.MaxInt32))
	expect(t, cUINT_MAX(), uint64(math.MaxUint32))
	if runtime.GOOS == "windows" {
		expect(t, cLONG_MIN(), int64(math.MinInt32))
		expect(t, cLONG_MAX(), int64(math.MaxInt32))
		expect(t, cULONG_MAX(), uint64(math.MaxUint32))
	} else {
		expect(t, cLONG_MIN(), int64(minInt))
		expect(t, cLONG_MAX(), int64(maxInt))
		expect(t, cULONG_MAX(), uint64(maxUint))
	}
	expect(t, cLLONG_MIN(), int64(math.MinInt64))
	expect(t, cLLONG_MAX(), int64(math.MaxInt64))
	expect(t, cULLONG_MAX(), uint64(math.MaxUint64))
	expect(t, cUINTPTR_MAX(), uintptr(maxUintptr))

	expect(t, cFLT_MIN(), float32(0))
	expect(t, cFLT_MAX(), float32(math.MaxFloat32))
	expect(t, cDBL_MIN(), float64(0))
	expect(t, cDBL_MAX(), float64(math.MaxFloat64))
}

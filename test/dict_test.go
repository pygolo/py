/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"testing"

	"gitlab.com/pygolo/py"
)

func TestDict(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	tc := map[interface{}]interface{}{}

	o, err := Py.GoToObject(tc)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Dict_Type)
	expect(t, Py.Dict_CheckExact(o), true)
	expectObjectLen(t, Py, o, len(tc))

	expectObject(t, Py, py.True, tc, &py.GoError{"AttributeError", "'bool' object has no attribute 'items'", nil})
}

func TestDictStringBool(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := map[string]bool{
		"zero":  true,
		"one":   false,
		"two":   false,
		"three": true,
		"four":  false,
		"five":  true,
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Dict_Type)
		expect(t, Py.Dict_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		test2 := make(map[string]interface{})
		err = Py.GoFromObject(o, &test2)
		expectError(t, err, nil)
		expect(t, len(test2), len(tc))
		for key, value := range test2 {
			expectElem(t, key, value, tc[key])
		}

		test3 := make(map[interface{}]bool)
		err = Py.GoFromObject(o, &test3)
		expectError(t, err, nil)
		expect(t, len(test3), len(tc))
		for key, value := range test3 {
			expectElem(t, key, value, tc[key.(string)])
		}

		test4 := make(map[interface{}]interface{})
		err = Py.GoFromObject(o, &test4)
		expectError(t, err, nil)
		expect(t, len(test4), len(tc))
		for key, value := range test4 {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test5 map[interface{}]interface{}
		err = Py.GoFromObject(o, &test5)
		expectError(t, err, nil)
		expect(t, len(test5), len(tc))
		for key, value := range test5 {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test6 interface{}
		err = Py.GoFromObject(o, &test6)
		expectError(t, err, nil)
		expect(t, len(test6.(map[interface{}]interface{})), len(tc))
		for key, value := range test6.(map[interface{}]interface{}) {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test7 map[int]bool
		err = Py.GoFromObject(o, &test7)
		expectMatch(t, err.Error(), `key ".*": cannot convert a Python str to Go int`)

		var test8 map[string]int
		err = Py.GoFromObject(o, &test8)
		expectMatch(t, err.Error(), `value ".*": cannot convert a Python bool to Go int`)
	})
}

func TestDictStringInterface(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := map[string]interface{}{
		"zero":  true,
		"one":   false,
		"two":   false,
		"three": true,
		"four":  "",
		"five":  true,
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Dict_Type)
		expect(t, Py.Dict_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		expectObject(t, Py, o, map[string]bool{
			"zero":  true,
			"one":   false,
			"two":   false,
			"three": true,
			"four":  false,
			"five":  true,
		}, nil)

		expectObject(t, Py, o, map[interface{}]bool{
			"zero":  true,
			"one":   false,
			"two":   false,
			"three": true,
			"four":  false,
			"five":  true,
		}, nil)

		test4 := make(map[interface{}]interface{})
		err = Py.GoFromObject(o, &test4)
		expectError(t, err, nil)
		expect(t, len(test4), len(tc))
		for key, value := range test4 {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test5 map[interface{}]interface{}
		err = Py.GoFromObject(o, &test5)
		expectError(t, err, nil)
		expect(t, len(test5), len(tc))
		for key, value := range test5 {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test6 interface{}
		err = Py.GoFromObject(o, &test6)
		expectError(t, err, nil)
		expect(t, len(test6.(map[interface{}]interface{})), len(tc))
		for key, value := range test6.(map[interface{}]interface{}) {
			expectElem(t, key, value, tc[key.(string)])
		}

		var test7 map[int]interface{}
		err = Py.GoFromObject(o, &test7)
		expectMatch(t, err.Error(), `key ".*": cannot convert a Python str to Go int`)
	})
}

func TestDictInterfaceBool(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := map[interface{}]bool{
		false:   true,
		"one":   false,
		"two":   false,
		"three": true,
		true:    false,
		"five":  true,
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Dict_Type)
		expect(t, Py.Dict_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		expectObject(t, Py, o, map[string]bool{
			"False": true,
			"one":   false,
			"two":   false,
			"three": true,
			"True":  false,
			"five":  true,
		}, nil)

		expectObject(t, Py, o, map[string]interface{}{
			"False": true,
			"one":   false,
			"two":   false,
			"three": true,
			"True":  false,
			"five":  true,
		}, nil)

		test4 := make(map[interface{}]interface{})
		err = Py.GoFromObject(o, &test4)
		expectError(t, err, nil)
		expect(t, len(test4), len(tc))
		for key, value := range test4 {
			expectElem(t, key, value, tc[key])
		}

		var test5 map[interface{}]interface{}
		err = Py.GoFromObject(o, &test5)
		expectError(t, err, nil)
		expect(t, len(test5), len(tc))
		for key, value := range test5 {
			expectElem(t, key, value, tc[key])
		}

		var test6 interface{}
		err = Py.GoFromObject(o, &test6)
		expectError(t, err, nil)
		expect(t, len(test6.(map[interface{}]interface{})), len(tc))
		for key, value := range test6.(map[interface{}]interface{}) {
			expectElem(t, key, value, tc[key])
		}

		var test7 map[interface{}]int
		err = Py.GoFromObject(o, &test7)
		expectMatch(t, err.Error(), `value ".*": cannot convert a Python bool to Go int`)
	})
}

func TestDictInterfaceInterface(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := map[interface{}]interface{}{
		false:   true,
		"one":   false,
		"two":   false,
		"three": true,
		"four":  map[interface{}]interface{}{},
		true:    []interface{}{},
		"five": map[interface{}]interface{}{
			"six": true,
			false: "seven",
			"sub": map[interface{}]interface{}{
				true: false,
				"list": []interface{}{
					"ten",
					"eleven",
					"twelve",
					map[interface{}]interface{}{
						"fourteen?": true,
					},
					"thirteen",
				},
				"false": "true",
			},
		},
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Dict_Type)
		expect(t, Py.Dict_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		expectObject(t, Py, o, map[string]bool{
			"False": true,
			"one":   false,
			"two":   false,
			"three": true,
			"four":  false,
			"True":  false,
			"five":  true,
		}, nil)

		expectObject(t, Py, o, map[string]interface{}{
			"False": true,
			"one":   false,
			"two":   false,
			"three": true,
			"four":  map[interface{}]interface{}{},
			"True":  []interface{}{},
			"five": map[interface{}]interface{}{
				"six": true,
				false: "seven",
				"sub": map[interface{}]interface{}{
					true: false,
					"list": []interface{}{
						"ten",
						"eleven",
						"twelve",
						map[interface{}]interface{}{
							"fourteen?": true,
						},
						"thirteen",
					},
					"false": "true",
				},
			},
		}, nil)

		expectObject(t, Py, o, map[interface{}]bool{
			false:   true,
			"one":   false,
			"two":   false,
			"three": true,
			"four":  false,
			true:    false,
			"five":  true,
		}, nil)

		var test5 map[interface{}]interface{}
		err = Py.GoFromObject(o, &test5)
		expectError(t, err, nil)
		expect(t, len(test5), len(tc))
		for key, value := range test5 {
			expectElem(t, key, value, tc[key])
		}

		var test6 interface{}
		err = Py.GoFromObject(o, &test6)
		expectError(t, err, nil)
		expect(t, len(test6.(map[interface{}]interface{})), len(tc))
		for key, value := range test6.(map[interface{}]interface{}) {
			expectElem(t, key, value, tc[key])
		}

		var test7 map[interface{}]int
		err = Py.GoFromObject(o, &test7)
		expectMatch(t, err.Error(), `value ".*": cannot convert a Python .* to Go int`)
	})
}

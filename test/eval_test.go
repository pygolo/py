/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"math"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestCompileEval(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_globals, err := Py.Dict_New()
	defer Py.DecRef(o_globals)
	expectError(t, err, nil)

	o_locals, err := Py.Dict_New()
	defer Py.DecRef(o_locals)
	expectError(t, err, nil)

	o_code, err := Py.CompileString("test = lambda x: x", "test", py.Single_input)
	defer Py.DecRef(o_code)
	expectError(t, err, nil)

	o_ret, err := Py.Eval_EvalCode(o_code, o_globals, o_locals)
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)
	expect(t, o_ret, py.None)

	o_test, err := Py.Dict_GetItem(o_locals, "test")
	expectError(t, err, nil)

	for _, tc := range []interface{}{
		"Hello, 世界",
		math.Pi,
		true,
		-1,
	} {
		o_ret, err = Py.Object_CallFunction(o_test, tc)
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
		expectObject(t, Py, o_ret, tc, nil)
	}
}

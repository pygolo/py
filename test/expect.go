/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"reflect"
	"regexp"
	"testing"

	"gitlab.com/pygolo/py"
)

func expectf(t testing.TB, format string, value, expected interface{}) {
	t.Helper()
	if !reflect.DeepEqual(value, expected) {
		if reflect.TypeOf(value) == reflect.TypeOf(expected) {
			t.Errorf(format, value, expected)
		} else {
			t.Errorf(format+" (%T != %T)", value, expected, value, expected)
		}
	}
}

func expect(t testing.TB, value, expected interface{}) {
	t.Helper()
	expectf(t, "value is %#v, expected %#v", value, expected)
}

func expectElem(t testing.TB, key, value, expected interface{}) {
	t.Helper()
	format := fmt.Sprintf("elem %#v is %%#v, expected %%#v", key)
	expectf(t, format, value, expected)
}

func expectError(t testing.TB, err, expected interface{}) {
	t.Helper()
	if err == nil && expected == nil {
		// no error
	} else if err == nil {
		t.Fatalf("error is %v, expected %q", err, expected)
	} else if expected == nil {
		t.Fatalf("error is %q, expected %v", err, expected)
	} else {
		expectf(t, "error is %q, expected %q", err, expected)
	}
}

func expectMatch(t testing.TB, value, re string) {
	t.Helper()
	matched, err := regexp.MatchString(re, value)
	expectError(t, err, nil)
	expect(t, matched, true)
	if !matched {
		t.Errorf("value is %q, expected match %q", value, re)
	}
}

func expectObject(t testing.TB, Py py.Py, o py.Object, expected interface{}, expectedError error) {
	t.Helper()
	value := reflect.New(reflect.TypeOf(expected))
	defer func() {
		if value, ok := value.Interface().(*py.Object); ok {
			defer Py.DecRef(*value)
		}
	}()
	err := Py.GoFromObject(o, value.Interface())
	expectError(t, err, expectedError)
	if err == nil {
		expect(t, reflect.Indirect(value).Interface(), expected)
	}
}

func expectObjectAny(t testing.TB, Py py.Py, o py.Object, expected interface{}, expectedError error) {
	t.Helper()
	var value interface{}
	defer func() {
		if value, ok := value.(py.Object); ok {
			defer Py.DecRef(value)
		}
	}()
	err := Py.GoFromObject(o, &value)
	expectError(t, err, expectedError)
	if err == nil {
		expect(t, value, expected)
	}
}

func expectObjectLen(t testing.TB, Py py.Py, o py.Object, expected int) {
	t.Helper()
	length, err := Py.Object_Length(o)
	expectError(t, err, nil)
	expect(t, length, expected)
}

func expectObjectAttr(t testing.TB, Py py.Py, o py.Object, attr_name string, expected interface{}, expectedError error) {
	t.Helper()
	o_value, err := Py.Object_GetAttr(o, attr_name)
	defer Py.DecRef(o_value)
	expectError(t, err, nil)
	expectObject(t, Py, o_value, expected, expectedError)
}

func expectSubtest(t *testing.T, Py py.Py, name string, fn func(*testing.T, py.Py)) {
	t.Helper()

	Py.GoLeavePython()
	defer Py.GoEnterPython()

	t.Run(name, func(t *testing.T) {
		Py, err := Py.GoNewFlow(nil)
		defer Py.Close()
		expectError(t, err, nil)

		Py.GoEnterPython()
		defer Py.GoLeavePython()

		fn(t, Py)
	})
}

func expectPythonVersionOrHigher(t *testing.T, Py py.Py, wanted string) {
	t.Helper()

	if lexicalMajorMinor(getPythonVersion(t, Py)) < lexicalMajorMinor(wanted) {
		t.Skip(fmt.Sprintf("Requires Python %s or higher", wanted))
	}
}

func expectPythonVersionUpTo(t *testing.T, Py py.Py, wanted string) {
	t.Helper()

	if lexicalMajorMinor(getPythonVersion(t, Py)) >= lexicalMajorMinor(wanted) {
		t.Skip(fmt.Sprintf("Requires Python up to %s", wanted))
	}
}

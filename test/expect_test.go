/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"runtime"
	"syscall"
	"testing"

	"gitlab.com/pygolo/py"
)

type expectTestCase struct {
	test      interface{}
	expect    interface{}
	err       string
	failed    bool
	failedNow bool
}

func (tc expectTestCase) assert(t *testing.T, test *expectTest) {
	t.Helper()
	if test.err != tc.err {
		t.Errorf("error is %q, expected %q", test.err, tc.err)
	}
	if test.failed != tc.failed {
		t.Errorf("failed is %t, expected %t", test.failed, tc.failed)
	}
	if test.failedNow != tc.failedNow {
		t.Errorf("failedNow is %t, expected %t", test.failedNow, tc.failedNow)
	}
}

type expectTest struct {
	*testing.T
	err       string
	failed    bool
	failedNow bool
}

func (t *expectTest) Errorf(format string, args ...interface{}) {
	t.err = fmt.Sprintf(format, args...)
	t.Fail()
}

func (t *expectTest) Fail() {
	t.failed = true
}

func (t *expectTest) Fatalf(format string, args ...interface{}) {
	t.err = fmt.Sprintf(format, args...)
	t.FailNow()
}

func (t *expectTest) FailNow() {
	t.failedNow = true
}

func TestExpect(t *testing.T) {
	for _, tc := range []expectTestCase{
		{
			test:   "some value",
			expect: "some value",
		},
		{
			test:   "some value",
			expect: "some other value",
			err:    `value is "some value", expected "some other value"`,
			failed: true,
		},
	} {
		test := &expectTest{T: t}
		expect(test, tc.test, tc.expect)
		tc.assert(t, test)
	}
}

func TestExpectElem(t *testing.T) {
	for i, tc := range []expectTestCase{
		{
			test:   "some value",
			expect: "some value",
		},
		{
			test:   "some value",
			expect: "some other value",
			err:    `elem 1 is "some value", expected "some other value"`,
			failed: true,
		},
	} {
		test := &expectTest{T: t}
		expectElem(test, i, tc.test, tc.expect)
		tc.assert(t, test)
	}
}

func TestExpectError(t *testing.T) {
	errno0 := "errno 0"
	if runtime.GOOS == "windows" {
		errno0 = "The operation completed successfully."
	}

	for _, tc := range []expectTestCase{
		{
			test:   nil,
			expect: nil,
		},
		{
			test:      fmt.Errorf("some error"),
			expect:    nil,
			err:       `error is "some error", expected <nil>`,
			failedNow: true,
		},
		{
			test:      nil,
			expect:    fmt.Errorf("some error"),
			err:       `error is <nil>, expected "some error"`,
			failedNow: true,
		},
		{
			test:   fmt.Errorf("some error"),
			expect: fmt.Errorf("some other error"),
			err:    `error is "some error", expected "some other error"`,
			failed: true,
		},
		{
			test:   syscall.Errno(0),
			expect: fmt.Errorf(syscall.Errno(0).Error()),
			err:    fmt.Sprintf(`error is "%s", expected "%s" (syscall.Errno != *errors.errorString)`, errno0, errno0),
			failed: true,
		},
	} {
		test := &expectTest{T: t}
		expectError(test, tc.test, tc.expect)
		tc.assert(t, test)
	}
}

func TestExpectMatch(t *testing.T) {
	for _, tc := range []expectTestCase{
		{
			test:   "some value",
			expect: "some value",
		},
		{
			test:   "some value",
			expect: "some val.+",
		},
		{
			test:   "some value",
			expect: "some other value",
			err:    `value is "some value", expected match "some other value"`,
			failed: true,
		},
		{
			test:   "some value",
			expect: "some value .+",
			err:    `value is "some value", expected match "some value .+"`,
			failed: true,
		},
	} {
		test := &expectTest{T: t}
		expectMatch(test, tc.test.(string), tc.expect.(string))
		tc.assert(t, test)
	}
}

func TestExpectObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []expectTestCase{
		{
			test:   py.None,
			expect: py.None,
		},
		{
			test:   py.None,
			expect: false,
		},
		{
			test:   py.True,
			expect: false,
			err:    "value is true, expected false",
			failed: true,
		},
		{
			test:      py.True,
			expect:    1,
			err:       `error is "cannot convert a Python bool to Go int", expected <nil>`,
			failedNow: true,
		},
	} {
		test := &expectTest{T: t}
		expectObject(test, Py, tc.test.(py.Object), tc.expect, nil)
		tc.assert(t, test)
	}
}

func TestExpectObjectAny(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []expectTestCase{
		{
			test:   py.False,
			expect: false,
		},
		{
			test:   py.True,
			expect: false,
			err:    "value is true, expected false",
			failed: true,
		},
		{
			test:   py.True,
			expect: 1,
			err:    `value is true, expected 1 (bool != int)`,
			failed: true,
		},
		{
			test:   py.False,
			expect: py.False,
			err:    fmt.Sprintf("value is false, expected %#v (bool != py.Object)", py.False),
			failed: true,
		},
	} {
		test := &expectTest{T: t}
		expectObjectAny(test, Py, tc.test.(py.Object), tc.expect, nil)
		tc.assert(t, test)
	}
}

func TestExpectObjectLen(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []expectTestCase{
		{
			test:   "test",
			expect: 4,
		},
		{
			test:   []int{0, 1},
			expect: 2,
		},
		{
			test:   []int{0, 1, 2, 3},
			expect: 2,
			err:    "value is 4, expected 2",
			failed: true,
		},
		{
			test:      py.False,
			expect:    0,
			err:       `error is "TypeError: object of type 'bool' has no len()", expected <nil>`,
			failedNow: true,
		},
	} {
		o, err := Py.GoToObject(tc.test)
		defer Py.DecRef(o)
		expectError(t, err, nil)

		test := &expectTest{T: t}
		expectObjectLen(test, Py, o, tc.expect.(int))
		tc.assert(t, test)
	}
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import "C"
import (
	"math"
	"unsafe"

	"gitlab.com/pygolo/py"
)

type NoPoint struct {
	Point
}

type Point struct {
	X float64 `python:"x"`
	Y float64 `python:"y"`
	Z float64 `python:"z"`
}

type FreeVector struct {
	DX float64 `python:"dx"`
	DY float64 `python:"dy"`
	DZ float64 `python:"dz"`
}

// Distance returns the distance between p and other
func (p Point) Distance(other Point) float64 {
	return math.Sqrt(math.Pow(p.X-other.X, 2) + math.Pow(p.Y-other.Y, 2) + math.Pow(p.Z-other.Z, 2))
}

// Magnitude returns the magnitude of the vector
func (v FreeVector) Magnitude() float64 {
	return math.Sqrt(math.Pow(v.DX, 2) + math.Pow(v.DY, 2) + math.Pow(v.DZ, 2))
}

func (v *FreeVector) PyInit(Py py.Py, args, kwargs py.Object) error {
	var a, b Point
	vars := py.GoArgsKwArgs{}
	vars.AllowUnknownKeywords()
	vars.Optional()
	vars.Keyword("a", &a)
	vars.Keyword("b", &b)
	err := Py.Arg_ParseTupleAndKeywords(args, kwargs, vars)
	if err != nil {
		return err
	}
	v.DX = b.X - a.X
	v.DY = b.Y - a.Y
	v.DZ = b.Z - a.Z
	return nil
}

// modf mimics Python's math.modf
func modf(f float64) (float64, float64) {
	integ, fract := math.Modf(f)
	if math.IsInf(f, 0) {
		fract = 0
	}
	return fract, integ
}

func nop() {
}

// count_args returns the number of passed arguments
func count_args(args ...py.Object) int {
	return len(args)
}

func summation(n int) int {
	ret := 0
	for i := 0; i < n; i++ {
		ret += i
	}
	return ret
}

// ncall calls the callable object num times, each from its own goroutine
func ncall(Py py.Py, num int, callable py.Object) error {
	cb := Py.GoNewClosureBuddy()
	defer cb.Close()

	Py.GoEnterPython()
	defer Py.GoLeavePython()

	for i := 0; i < num; i++ {
		go func(cl *py.GoClosure) {
			Py, err := Py.GoNewFlow(cl)
			defer Py.Close()
			if err != nil {
				panic(err)
			}

			Py.GoEnterPython()
			defer Py.GoLeavePython()

			ret, err := Py.Object_CallFunction(callable)
			defer Py.DecRef(ret)
			if err != nil {
				panic(err)
			}
		}(cb.Capture(callable))
	}

	return nil
}

//export PyInit_ext
func PyInit_ext() unsafe.Pointer {
	return py.GoExtend(func(Py py.Py, m py.Object) error {
		if err := Py.Module_SetDocString(m, "doc"); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "pi", math.Pi); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "modf", modf); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "nop", nop); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "count_args", count_args); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "summation", summation); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "ncall", ncall); err != nil {
			return err
		}
		if err := Py.GoRegisterStruct(Point{}); err != nil {
			return err
		}
		t, _ := Py.GoGetStructType(Point{})
		defer Py.DecRef(t.AsObject())
		if err := Py.Object_SetAttr(m, "Point", t); err != nil {
			return err
		}
		if err := Py.GoRegisterStruct((*NoPoint)(nil)); err != nil {
			return err
		}
		t, _ = Py.GoGetStructType(&NoPoint{})
		defer Py.DecRef(t.AsObject())
		if err := Py.Object_SetAttr(m, "NoPoint", t); err != nil {
			return err
		}
		if err := Py.GoRegisterStruct(FreeVector{}); err != nil {
			return err
		}
		t, _ = Py.GoGetStructType(FreeVector{})
		defer Py.DecRef(t.AsObject())
		if err := Py.Object_SetAttr(m, "FreeVector", t); err != nil {
			return err
		}
		var value string
		p := py.GoProperty{}
		if err := Py.Object_SetAttr(m, "nil_prop", p); err != nil {
			return err
		}
		p = py.GoProperty{
			Getter: py.GoFunction{Fn: func(m py.Object) string { return value }},
		}
		if err := Py.Object_SetAttr(m, "ro_prop", p); err != nil {
			return err
		}
		p = py.GoProperty{
			Setter: py.GoFunction{Fn: func(m py.Object, v string) { value = v }},
		}
		if err := Py.Object_SetAttr(m, "wo_prop", p); err != nil {
			return err
		}
		o_buf, err := Py.NewGoBuffer(256)
		defer Py.DecRef(o_buf)
		if err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "buffer", o_buf); err != nil {
			return err
		}
		var buf py.Buffer
		if err := Py.Object_GetBuffer(o_buf, &buf, py.Buf_simple); err != nil {
			return err
		}
		defer Py.Buffer_Release(&buf)
		data := buf.UnsafeSlice()
		for i := range data {
			data[i] = byte(i)
		}
		return nil
	})
}

// required by cgo but unused
func main() {
}

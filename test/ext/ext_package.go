//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"embed"

	"gitlab.com/pygolo/py"
)

//go:embed *.py
var embedded embed.FS

func init() {
	py.GoAtInit(func(Py py.Py, m py.Object) error {
		o_name, err := Py.Object_GetAttr(m, "__name__")
		defer Py.DecRef(o_name)
		if err != nil {
			return err
		}
		var name string
		err = Py.GoFromObject(o_name, &name)
		if err != nil {
			return err
		}
		fs := py.GoFSImport{FS: embedded, Namespace: name}
		if err := Py.Object_SetAttr(m, "__path__", []interface{}{fs}); err != nil {
			return err
		}
		if err := Py.Object_SetAttr(m, "__package__", o_name); err != nil {
			return err
		}
		return nil
	})
}

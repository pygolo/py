/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"math"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestFloat(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expect(t, Py.Float_GetMax(), math.MaxFloat64)

	o, err := Py.Float_FromDouble(math.MaxFloat64)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Float_Type)
	expect(t, Py.Float_CheckExact(o), true)
	test, err := Py.Float_AsDouble(o)
	expectError(t, err, nil)
	expect(t, test, math.MaxFloat64)
}

func TestFloatToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.GoToObject(math.MaxFloat64)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Float_Type)
	expect(t, Py.Float_CheckExact(o), true)
	test, err := Py.Float_AsDouble(o)
	expectError(t, err, nil)
	expect(t, test, math.MaxFloat64)
}

func TestFloatFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.Float_FromDouble(math.MaxFloat64)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Float_Type)
	expect(t, Py.Float_CheckExact(o), true)
	expectObject(t, Py, o, math.MaxFloat64, nil)
}

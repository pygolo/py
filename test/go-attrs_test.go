/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"testing"

	"gitlab.com/pygolo/py"
)

func TestGoAttrsMap(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_test, err := Py.GoToObject("test")
	defer Py.DecRef(o_test)
	expect(t, err, nil)

	m := map[string]interface{}{
		"t0": 0,
		"t1": "1",
		"t2": 2.0,
		"t3": Py.NewRef(o_test),
	}
	var attrs py.GoAttrs = py.GoAttrsMap(m)

	for k, v := range m {
		o_value, err := attrs.PyGetAttr(Py, k)
		defer Py.DecRef(o_value)
		expect(t, err, nil)
		expectObject(t, Py, o_value, v, nil)
	}

	n := Py.RefCnt(o_test)

	o_value, err := attrs.PyGetAttr(Py, "test")
	defer Py.DecRef(o_value)
	expect(t, err, nil)
	expectObject(t, Py, o_value, py.None, nil)

	err = attrs.PySetAttr(Py, "test", "test value")
	expect(t, err, nil)

	o_value, err = attrs.PyGetAttr(Py, "test")
	defer Py.DecRef(o_value)
	expect(t, err, nil)
	expectObject(t, Py, o_value, "test value", nil)

	err = attrs.PySetAttr(Py, "test", o_test)
	expect(t, err, nil)
	expect(t, Py.RefCnt(o_test), n+1)

	o_value, err = attrs.PyGetAttr(Py, "test")
	expect(t, err, nil)
	expect(t, o_value, o_test)
	expect(t, Py.RefCnt(o_test), n+2)
	Py.DecRef(o_value)
	expect(t, Py.RefCnt(o_test), n+1)

	err = attrs.PySetAttr(Py, "test", o_test)
	expect(t, err, nil)
	expect(t, Py.RefCnt(o_test), n+1)

	err = attrs.PySetAttr(Py, "test", nil)
	expect(t, err, nil)
	expect(t, Py.RefCnt(o_test), n)

	o_value, err = attrs.PyGetAttr(Py, "t3")
	expect(t, err, nil)
	expect(t, o_value, o_test)
	expect(t, Py.RefCnt(o_test), n+1)
	Py.DecRef(o_value)
	expect(t, Py.RefCnt(o_test), n)

	err = attrs.PySetAttr(Py, "t3", nil)
	expect(t, err, nil)
	expect(t, Py.RefCnt(o_test), n-1)

	o_value, err = attrs.PyGetAttr(Py, "test")
	defer Py.DecRef(o_value)
	expect(t, err, nil)
	expectObject(t, Py, o_value, py.None, nil)

	err = attrs.PySetAttr(Py, "test", py.Object{})
	expect(t, err, nil)

	o_value, err = attrs.PyGetAttr(Py, "test")
	defer Py.DecRef(o_value)
	expect(t, err, nil)
	expectObject(t, Py, o_value, py.None, nil)
}

type panickingAttrs struct {
	py.GoAttrs
}

func (p panickingAttrs) PyGetAttr(Py py.Py, attr_name string) (py.Object, error) {
	if attr_name == "__name__" {
		panic(attr_name)
	}
	return p.GoAttrs.PyGetAttr(Py, attr_name)
}

func (p panickingAttrs) PySetAttr(Py py.Py, attr_name string, attr_value interface{}) error {
	if attr_name == "__name__" {
		panic(attr_name)
	}
	return p.GoAttrs.PySetAttr(Py, attr_name, attr_value)
}

func TestGoAttrspanic(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	fun := py.GoFunction{
		Fn:      func() {},
		GoAttrs: panickingAttrs{py.GoAttrsMap{}},
	}

	o_fun, err := Py.GoToObject(fun)
	defer Py.DecRef(o_fun)
	expectError(t, err, nil)

	o, err := Py.Object_GetAttr(o_fun, "__name__")
	defer Py.DecRef(o)
	expectError(t, err, &py.GoError{"AttributeError", "panic: __name__", nil})

	err = Py.Object_SetAttr(o_fun, "__name__", "test")
	expectError(t, err, &py.GoError{"AttributeError", "panic: __name__", nil})
}

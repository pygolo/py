/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"testing"

	"gitlab.com/pygolo/py"
)

func TestClosure(t *testing.T) {
	t.Run("None", func(t *testing.T) {
		Py, err := py.GoEmbed()
		defer Py.Close()
		expectError(t, err, nil)

		buddy := Py.GoNewClosureBuddy()
		buddy.Close()
	})

	t.Run("Accept", func(t *testing.T) {
		Py, err := py.GoEmbed()
		defer Py.Close()
		expectError(t, err, nil)

		o, err := Py.GoToObject("test")
		defer Py.DecRef(o)
		expectError(t, err, nil)

		buddy := Py.GoNewClosureBuddy()
		cnt := Py.RefCnt(o)

		closure := buddy.Capture(o)
		expect(t, Py.RefCnt(o), cnt+1)

		capture := closure.Accept()
		expect(t, Py.RefCnt(o), cnt+1)
		expect(t, len(capture), 1)
		expect(t, capture[0], o)
		defer Py.DecRef(o)

		buddy.Close()
		expect(t, Py.RefCnt(o), cnt+1)
	})

	t.Run("Reject", func(t *testing.T) {
		Py, err := py.GoEmbed()
		defer Py.Close()
		expectError(t, err, nil)

		o, err := Py.GoToObject("test")
		defer Py.DecRef(o)
		expectError(t, err, nil)

		buddy := Py.GoNewClosureBuddy()
		cnt := Py.RefCnt(o)

		closure := buddy.Capture(o)
		expect(t, Py.RefCnt(o), cnt+1)

		closure.Reject()
		expect(t, Py.RefCnt(o), cnt+1)

		buddy.Close()
		expect(t, Py.RefCnt(o), cnt)
	})
}

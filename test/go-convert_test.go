/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"math"
	"reflect"
	"testing"

	"gitlab.com/pygolo/py"
)

type testStruct struct {
	I int
}

var convTestStruct = py.GoConvConf{
	TypeOf:     testStruct{},
	ToObject:   testStructToObject,
	FromObject: testStructFromObject,
}

func testStructToObject(Py py.Py, a interface{}) (o py.Object, e error) {
	switch a.(testStruct) {
	case testStruct{-1}:
		o = py.None
	case testStruct{1}:
		o = py.True
	default:
		e = fmt.Errorf("value is neither -1 nor 1: %v", a)
	}
	Py.IncRef(o)
	return
}

func testStructFromObject(Py py.Py, o py.Object, a interface{}) error {
	v := a.(*testStruct)
	switch o {
	case py.None:
		*v = testStruct{-1}
	case py.True:
		*v = testStruct{1}
	default:
		return fmt.Errorf("object is neither None nor True: %v", o)
	}
	return nil
}

func TestGoConvConf(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	type testCase struct {
		py.GoConvConf
		err error
	}

	testcases := []testCase{
		{py.GoConvConf{},
			fmt.Errorf("either TypeOf, Kind or TypeObject must be set")},
		{py.GoConvConf{TypeOf: struct{}{}},
			fmt.Errorf("either ToObject or FromObject must be set")},
		{py.GoConvConf{Kind: reflect.Struct},
			fmt.Errorf("either ToObject or FromObject must be set")},
		{py.GoConvConf{TypeObject: py.None_Type},
			fmt.Errorf("FromObject must be set")},
	}
	for _, tc := range testcases {
		// with invalid parameters, must fail
		err := Py.GoRegisterConversions(tc.GoConvConf)
		expectError(t, err, tc.err)
	}

	testcases = []testCase{
		{py.GoConvConf{TypeOf: struct{}{}, ToObject: testStructToObject},
			fmt.Errorf("Type handler is already registered: struct {}")},
		{py.GoConvConf{TypeOf: struct{}{}, FromObject: testStructFromObject},
			fmt.Errorf("Type handler is already registered: struct {}")},
		{py.GoConvConf{TypeObject: py.None_Type, FromObject: testStructFromObject},
			fmt.Errorf("TypeObject handler is already registered: %v", py.None_Type)},
	}
	var convs []py.GoConvConf
	for _, tc := range testcases {
		convs = append(convs, tc.GoConvConf)
	}

	// duplicate the last element to check the removal of all the previous
	// converters when registered in group
	convs2 := append(convs, convs[len(convs)-1])

	// first (group) registration (with duplicate), must fail at the last element
	err = Py.GoRegisterConversions(convs2...)
	expectError(t, err, testcases[len(testcases)-1].err)

	// second (group) registration, must succeed
	err = Py.GoRegisterConversions(convs...)
	expectError(t, err, nil)

	// third (group) registration, must fail at the first element
	err = Py.GoRegisterConversions(convs...)
	expectError(t, err, testcases[0].err)

	// fourth (one by one) registration, all must fail
	for _, tc := range testcases {
		err := Py.GoRegisterConversions(tc.GoConvConf)
		expectError(t, err, tc.err)
	}

	// registration after deregistration, must succeed
	Py.GoDeregisterConversions(convs...)
	err = Py.GoRegisterConversions(convs...)
	expectError(t, err, nil)
	Py.GoDeregisterConversions(convs...)
}

func TestToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []struct {
		a    interface{}
		test py.Object
	}{
		{py.None, py.None},
		{py.True, py.True},
		{py.False, py.False},
		{py.None_Type, py.None_Type.AsObject()},
		{py.Bool_Type, py.Bool_Type.AsObject()},
	} {
		o, err := Py.GoToObject(tc.a)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o, tc.test)
	}

	_, err = Py.GoToObject(nil)
	expectError(t, err, fmt.Errorf("cannot convert a Go <nil> to Python"))

	err = Py.GoRegisterConversions(convTestStruct)
	expectError(t, err, nil)

	var testcases = []struct {
		testStruct
		py.Object
		err error
	}{
		{testStruct{-2}, py.Object{}, fmt.Errorf("value is neither -1 nor 1: {-2}")},
		{testStruct{-1}, py.None, nil},
		{testStruct{0}, py.Object{}, fmt.Errorf("value is neither -1 nor 1: {0}")},
		{testStruct{1}, py.True, nil},
		{testStruct{2}, py.Object{}, fmt.Errorf("value is neither -1 nor 1: {2}")},
	}
	for _, tc := range testcases {
		o, err := Py.GoToObject(tc.testStruct)
		defer Py.DecRef(o)
		expectError(t, err, tc.err)
		expect(t, o, tc.Object)
	}

	Py.GoDeregisterConversions(convTestStruct)
}

func TestFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var a py.TypeObject

	err = Py.GoFromObject(py.None, &a)
	expectError(t, err, fmt.Errorf("cannot convert a Python NoneType to Go py.TypeObject"))
	err = Py.GoFromObject(py.None, (*py.TypeObject)(nil))
	expectError(t, err, fmt.Errorf("cannot store in <nil>, need a pointer"))

	err = Py.GoFromObject(py.None_Type.AsObject(), &a)
	expectError(t, err, nil)
	expect(t, a, py.None_Type)
	Py.DecRef(a.AsObject())

	var test testStruct

	err = Py.GoFromObject(py.Object{}, nil)
	expectError(t, err, fmt.Errorf("cannot store in <nil>, need a pointer"))
	err = Py.GoFromObject(py.Object{}, (*testStruct)(nil))
	expectError(t, err, fmt.Errorf("cannot store in <nil>, need a pointer"))
	err = Py.GoFromObject(py.Object{}, &test)
	expectError(t, err, fmt.Errorf("cannot convert a Python <nil> to Go test.testStruct"))
	err = Py.GoFromObject(py.None, &test)
	expectError(t, err, &py.GoError{"AttributeError", "'NoneType' object has no attribute 'I'", nil})

	err = Py.GoRegisterConversions(convTestStruct)
	expectError(t, err, nil)

	var testcases = []struct {
		py.Object
		testStruct
		err error
	}{
		{py.None, testStruct{-1}, nil},
		{py.False, testStruct{0}, fmt.Errorf("object is neither None nor True: %v", py.False)},
		{py.True, testStruct{1}, nil},
	}
	for _, tc := range testcases {
		var test testStruct
		err := Py.GoFromObject(tc.Object, &test)
		expectError(t, err, tc.err)
		expect(t, test, tc.testStruct)

		var o_test py.Object
		defer Py.DecRef(o_test)
		err = Py.GoFromObject(tc.Object, &o_test)
		expectError(t, err, nil)
		expect(t, o_test, tc.Object)
	}

	Py.GoDeregisterConversions(convTestStruct)

	err = Py.GoFromObject(py.None, &test)
	expectError(t, err, &py.GoError{"AttributeError", "'NoneType' object has no attribute 'I'", nil})
}

func TestFromObjectNonPointer(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	testcases := []struct {
		value interface{}
		err   error
	}{
		{nil, fmt.Errorf("cannot store in <nil>, need a pointer")},
		{true, fmt.Errorf("cannot store in bool, need a pointer")},
		{testStruct{}, fmt.Errorf("cannot store in test.testStruct, need a pointer")},
	}
	for _, tc := range testcases {
		err := Py.GoFromObject(py.None, tc.value)
		expectError(t, err, tc.err)
	}
}

func TestDestructuring(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	tc := map[interface{}]interface{}{
		false:   0,
		1:       true,
		"two":   2.0,
		math.Pi: "Pi",
	}

	o, err := Py.GoToObject(tc)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expectObjectLen(t, Py, o, len(tc))

	var test map[py.Object]py.Object
	err = Py.GoFromObject(o, &test)
	expectError(t, err, nil)
	expect(t, len(test), len(tc))

	for o_key, o_value := range test {
		defer Py.DecRef(o_key, o_value)

		var key interface{}
		err = Py.GoFromObject(o_key, &key)
		expectError(t, err, nil)

		var value interface{}
		err = Py.GoFromObject(o_value, &value)
		expectError(t, err, nil)

		expectError(t, value, tc[key])
	}
}

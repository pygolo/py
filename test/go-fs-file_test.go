//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"bufio"
	"os"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestFSFileToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	test_lines := []string{
		"test line 1\n",
		"test line 2\n",
		"test line 3\n",
	}

	filename := "test-go-fs-file.txt"
	file, err := os.Create(filename)
	expectError(t, err, nil)

	expectSubtest(t, Py, "Write", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject(py.GoFSFile{File: file})
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		for _, line := range test_lines {
			o_ret, err := Py.Object_CallMethod(o_file, "write", []byte(line))
			defer Py.DecRef(o_ret)
			expectError(t, err, nil)
			expectObject(t, Py, o_ret, len(line), nil)
		}

		o_ret, err := Py.Object_CallMethod(o_file, "flush")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		scanner := bufio.NewScanner(file)
		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)

		file.Seek(0, 0)
		scanner = bufio.NewScanner(file)
		for _, line := range test_lines {
			expect(t, scanner.Scan(), true)
			expectError(t, scanner.Err(), nil)
			expect(t, scanner.Text()+"\n", line)
		}

		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)
	})

	expectSubtest(t, Py, "ReadLines", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject(py.GoFSFile{File: file})
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		file.Seek(0, 0)
		o_lines, err := Py.Object_CallMethod(o_file, "readlines")
		defer Py.DecRef(o_lines)
		expectError(t, err, nil)
		expectObject(t, Py, o_lines, test_lines, nil)
	})

	err = file.Close()
	expectError(t, err, nil)
	err = os.Remove(filename)
	expectError(t, err, nil)
}

//go:build go1.16
// +build go1.16

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"embed"
	"fmt"
	"io/fs"
	"path"
	"testing"
	"testing/fstest"
	"unicode/utf8"

	"gitlab.com/pygolo/py"
)

var content []byte = []byte("# my 2¢")

var directory *fstest.MapFile = &fstest.MapFile{
	Mode: fs.ModeDir,
}

var module *fstest.MapFile = &fstest.MapFile{
	Data: content,
}

var fs0 fstest.MapFS = fstest.MapFS{
	"fs0.py": module,
}

var fs1 fstest.MapFS = fstest.MapFS{
	// valid content but wrong name, no .py suffix
	"fs1": module,
}

var fs2 fstest.MapFS = fstest.MapFS{
	"ns/fs2.py": module,
}

var fs3 fstest.MapFS = fstest.MapFS{
	// file fs3.py has priority over directory fs3
	"fs3":    directory,
	"fs3.py": module,
}

var fs4 fstest.MapFS = fstest.MapFS{
	"ns4": directory,
}

var fs5 fstest.MapFS = fstest.MapFS{
	// pkg/__init__.py has priority over pkg.py
	"ns/pkg/__init__.py":     module,
	"ns/pkg/sub/__init__.py": module,
	"ns/pkg/sub/fs5.py":      module,
	"ns/pkg.py":              module,
}

//go:embed embedded.py
var fs6 embed.FS

func TestGoFSImport(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	expectError(t, err, nil)
	o_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_path)
	expectError(t, err, nil)

	efs := []interface{}{
		fs0,
		fs1,
		fs2,
		fs3,
		fs4,
		fs5,
		fs6,
		py.GoFSImport{FS: fs0, Namespace: "ns"},
		py.GoFSImport{FS: fs0, Namespace: "ns.pkg"},
		py.GoFSImport{FS: fs0, Namespace: "ns2"},
		py.GoFSImport{FS: fs0, Namespace: "ns3.ns31.pkg.sub"},
		py.GoFSImport{FS: fs0, Namespace: "ns3.ns32"},
		py.GoFSImport{FS: fs5, Namespace: "ns4", Prefix: "ns/"},
		py.GoFSImport{FS: fs5, Namespace: "ns5", Prefix: "ns//pkg"},
		py.GoFSImport{FS: fs5, Namespace: "ns4", Prefix: "/ns", Root: "caput"},
	}

	for _, fs := range efs {
		err = Py.List_Append(o_path, fs)
		expectError(t, err, nil)
	}

	fspath := func(nr int, file string) string {
		g, ok := efs[nr].(py.GoFSImport)
		if !ok {
			g = py.GoFSImport{FS: efs[nr].(fs.FS)}
		}
		return path.Join(g.String(), file)
	}

	for _, tc := range []struct {
		module   string
		package_ string
		file     interface{}
		path     interface{}
		err      error
	}{
		{
			module: "fs0",
			file:   fspath(0, "fs0.py"),
		},
		{
			module: "fs1",
			err:    &py.GoError{"ModuleNotFoundError", "No module named 'fs1'", nil},
		},
		{
			module: "fs3",
			file:   fspath(3, "fs3.py"),
		},
		{
			module: "embedded",
			file:   fspath(6, "embedded.py"),
		},
		{
			module:   "ns",
			package_: "ns",
			path: fmt.Sprintf("_NamespacePath(['%s', '%s', '%s', '%s'])",
				fspath(2, "ns"), fspath(5, "ns"), fspath(7, ""), fspath(8, "")),
		},
		{
			module:   "ns.fs0",
			package_: "ns",
			file:     fspath(7, "fs0.py"),
		},
		{
			module:   "ns.fs2",
			package_: "ns",
			file:     fspath(2, "ns/fs2.py"),
		},
		{
			module:   "ns.pkg",
			package_: "ns.pkg",
			file:     fspath(5, "ns/pkg/__init__.py"),
			path:     fmt.Sprintf("['%s']", fspath(5, "ns/pkg")),
		},
		{
			// earlier than the file system that provides ns.pkg.fs0 there is
			// another that defines ns.pkg as a package (not a namespace package),
			// because that file system does not have any fs0 module the search
			// is stopped there and the module is not found.
			module: "ns.pkg.fs0",
			err:    &py.GoError{"ModuleNotFoundError", "No module named 'ns.pkg.fs0'", nil},
		},
		{
			module:   "ns.pkg.sub",
			package_: "ns.pkg.sub",
			file:     fspath(5, "ns/pkg/sub/__init__.py"),
			path:     fmt.Sprintf("['%s']", fspath(5, "ns/pkg/sub")),
		},
		{
			module:   "ns.pkg.sub.fs5",
			package_: "ns.pkg.sub",
			file:     fspath(5, "ns/pkg/sub/fs5.py"),
		},
		{
			module:   "ns2",
			package_: "ns2",
			path:     fmt.Sprintf("_NamespacePath(['%s'])", fspath(9, "")),
		},
		{
			module:   "ns2.fs0",
			package_: "ns2",
			file:     fspath(9, "fs0.py"),
		},
		{
			module:   "ns3",
			package_: "ns3",
			path:     fmt.Sprintf("_NamespacePath(['%s', '%s'])", fspath(10, ""), fspath(11, "")),
		},
		{
			module:   "ns3.ns31",
			package_: "ns3.ns31",
			path:     fmt.Sprintf("_NamespacePath(['%s'])", fspath(10, "")),
		},
		{
			module:   "ns3.ns31.pkg",
			package_: "ns3.ns31.pkg",
			path:     fmt.Sprintf("_NamespacePath(['%s'])", fspath(10, "")),
		},
		{
			module:   "ns3.ns31.pkg.sub",
			package_: "ns3.ns31.pkg.sub",
			path:     fmt.Sprintf("_NamespacePath(['%s'])", fspath(10, "")),
		},
		{
			module:   "ns3.ns31.pkg.sub.fs0",
			package_: "ns3.ns31.pkg.sub",
			file:     fspath(10, "fs0.py"),
		},
		{
			module:   "ns3.ns32",
			package_: "ns3.ns32",
			path:     fmt.Sprintf("_NamespacePath(['%s'])", fspath(11, "")),
		},
		{
			module:   "ns3.ns32.fs0",
			package_: "ns3.ns32",
			file:     fspath(11, "fs0.py"),
		},
		{
			module:   "ns4",
			package_: "ns4",
			path: fmt.Sprintf("_NamespacePath(['%s', '%s', '%s'])",
				fspath(4, "ns4"), fspath(12, ""), fspath(14, "")),
		},
		{
			module:   "ns4.pkg",
			package_: "ns4.pkg",
			file:     fspath(12, "pkg/__init__.py"),
			path:     fmt.Sprintf("['%s']", fspath(12, "pkg")),
		},
		{
			module:   "ns5",
			package_: "ns5",
			file:     fspath(13, "__init__.py"),
			path:     fmt.Sprintf("['%s']", fspath(13, "")),
		},
	} {
		expectSubtest(t, Py, tc.module, func(t *testing.T, Py py.Py) {
			o, err := Py.Import_Import(tc.module)
			defer Py.DecRef(o)
			expectError(t, err, tc.err)
			if tc.err == nil {
				expectObjectAttr(t, Py, o, "__name__", tc.module, nil)
				expectObjectAttr(t, Py, o, "__package__", tc.package_, nil)
				if tc.path != nil {
					expectObjectAttr(t, Py, o, "__path__", tc.path, nil)
				}
				if tc.file != nil {
					expectObjectAttr(t, Py, o, "__file__", tc.file.(string), nil)
				}
			}
		})
	}

	expect(t, fspath(14, "sub2"), "caput@ns4:/ns/sub2")
}

func TestGoFSImportResourceLegacy(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)
	expectPythonVersionOrHigher(t, Py, "3.7")

	// remove when https://github.com/python/cpython/issues/127012 is fixed
	expectPythonVersionUpTo(t, Py, "3.13")

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	expectError(t, err, nil)
	o_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_path)
	expectError(t, err, nil)
	err = Py.List_Append(o_path, fs5)
	expectError(t, err, nil)

	o_resources, err := Py.Import_Import("importlib.resources")
	defer Py.DecRef(o_resources)
	expectError(t, err, nil)

	o_ret, err := Py.Object_CallMethod(o_resources, "is_resource", "ns.pkg", "__init__.py")
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)
	expectObject(t, Py, o_ret, true, nil)

	o_ret, err = Py.Object_CallMethod(o_resources, "path", "ns.pkg", "__init__.py")
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_text, err := Py.Object_CallMethod(o_resources, "read_text", "ns.pkg", "__init__.py", "utf-8")
	defer Py.DecRef(o_text)
	expectError(t, err, nil)
	expectObjectLen(t, Py, o_text, utf8.RuneCountInString(string(content)))
	expectObject(t, Py, o_text, string(content), nil)

	o_bytes, err := Py.Object_CallMethod(o_resources, "read_binary", "ns.pkg", "__init__.py")
	defer Py.DecRef(o_bytes)
	expectError(t, err, nil)
	expectObject(t, Py, o_bytes, content, nil)
}

func TestGoFSImportResource(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)
	expectPythonVersionOrHigher(t, Py, "3.11")

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	expectError(t, err, nil)
	o_path, err := Py.Object_GetAttr(o_sys, "path")
	defer Py.DecRef(o_path)
	expectError(t, err, nil)
	err = Py.List_Append(o_path, fs5)
	expectError(t, err, nil)

	o_resources, err := Py.Import_Import("importlib.resources")
	defer Py.DecRef(o_resources)
	expectError(t, err, nil)

	o_files, err := Py.Object_CallMethod(o_resources, "files", "ns.pkg")
	defer Py.DecRef(o_files)
	expectError(t, err, nil)

	o_file, err := Py.Object_CallMethod(o_files, "__truediv__", "__init__.py")
	defer Py.DecRef(o_file)
	expectError(t, err, nil)

	o_text, err := Py.Object_CallMethod(o_file, "read_text", "utf-8")
	defer Py.DecRef(o_text)
	expectError(t, err, nil)
	expectObjectLen(t, Py, o_text, utf8.RuneCountInString(string(content)))
	expectObject(t, Py, o_text, string(content), nil)

	o_bytes, err := Py.Object_CallMethod(o_file, "read_bytes")
	defer Py.DecRef(o_bytes)
	expectError(t, err, nil)
	expectObject(t, Py, o_bytes, content, nil)
}

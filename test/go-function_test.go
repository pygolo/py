/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"reflect"
	"testing"
	"testing/quick"

	"gitlab.com/pygolo/py"
)

func TestGoFunctionType(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expect(t, Py.GoFunction_Type().AsObject().Type(), py.Type_Type)

	o, err := Py.Object_CallFunction(Py.GoFunction_Type().AsObject())
	defer Py.DecRef(o)
	expectError(t, err, &py.GoError{"TypeError", "cannot create 'GoFunction' instances", nil})
}

func TestFunctionToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.Dict_New()
	defer Py.DecRef(o)
	expectError(t, err, nil)
	cnt := Py.RefCnt(o)
	defer func() {
		expect(t, Py.RefCnt(o), cnt)
	}()

	for _, tc := range []struct {
		fn     interface{}
		args   py.GoArgs
		kwargs py.GoKwArgs
		result interface{}
		err    error
		access bool
	}{
		{func() {}, nil, nil, py.None, nil, false},
		{func(a, b int) int { return a + b }, py.GoArgs{11, 13}, nil, 11 + 13, nil, false},
		{func(a, b int) int { return a * b }, py.GoArgs{11, 13}, nil, 11 * 13, nil, false},
		{func() (bool, error) { return true, nil }, nil, nil, py.True, nil, false},
		{func() (bool, int, float64, string) { return false, 1, 2.0, "3" }, nil, nil, []interface{}{false, 1, 2.0, "3"}, nil, false},
		{func(o py.Object) (bool, error) { return Py.Object_IsTrue(o) }, py.GoArgs{1}, nil, true, nil, true},
		{func(o py.Object) (bool, error) { return Py.Object_IsTrue(o) }, py.GoArgs{true}, nil, true, nil, true},
		{func(o py.Object) (bool, error) { return Py.Object_IsTrue(o) }, py.GoArgs{false}, nil, false, nil, true},
		{func(o py.Object) (bool, error) { return Py.Object_IsTrue(o) }, py.GoArgs{py.None}, nil, false, nil, true},
		{func(o py.Object) (bool, error) { return Py.Object_IsTrue(o) }, py.GoArgs{[]int{}}, nil, false, nil, true},
		{func(o py.Object) { expect(t, Py.RefCnt(o) > cnt, true) }, py.GoArgs{o}, nil, false, nil, true},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, py.GoArgs{1}, nil, py.True, nil, true},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, py.GoArgs{true}, nil, py.True, nil, true},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, py.GoArgs{false}, nil, py.False, nil, true},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, py.GoArgs{""}, nil, py.False, nil, true},
		{func(a ...int) int { return len(a) }, py.GoArgs{0}, nil, 1, nil, false},
		{func(_, _ int, c ...int) int { return len(c) }, py.GoArgs{0, 1, 2, 3}, nil, 2, nil, false},
		{func(_ int, _ py.Py, _ int, c ...int) int { return len(c) }, py.GoArgs{0, 1, 2, 3}, nil, 2, nil, false},

		{func(_ int, _ py.Py) {}, py.GoArgs{0}, nil, py.None, nil, false},
		{func(_ py.Py, _ int) {}, py.GoArgs{0}, nil, py.None, nil, false},
		{Py.GoToObject, py.GoArgs{"bound method"}, nil, "bound method", nil, true},
		{py.Py.GoToObject, py.GoArgs{"unbound method"}, nil, "unbound method", nil, true},

		{func(p py.Py) { p.GoEnterPython(); p.GoLeavePython() }, nil, nil, py.None, nil, false},
		{func(p py.Py) { p.GoLeavePython(); p.GoEnterPython() }, nil, nil, py.None, nil, true},

		{func() { panic("booh!") }, nil, nil, nil,
			&py.GoError{"RuntimeError", "panic: booh!", nil}, false},
		{func() {}, nil, py.GoKwArgs{"a": 0}, nil,
			&py.GoError{"TypeError", "Unexpected keyword arguments", nil}, false},
		{func(a, b int) int { return a / b }, py.GoArgs{1, 0}, nil, nil,
			&py.GoError{"RuntimeError", "panic: runtime error: integer divide by zero", nil}, false},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, py.GoArgs{0, 1}, nil, 0,
			&py.GoError{"TypeError", "takes 1 positional argument but 2 were given", nil}, true},
		{func(b bool) (py.Object, error) { return Py.GoToObject(b) }, nil, nil, 0,
			&py.GoError{"TypeError", "takes 1 positional argument but 0 were given", nil}, true},
		{func(a uint8) uint8 { return a }, py.GoArgs{256}, nil, nil,
			&py.GoError{"RuntimeError", "arg #0: Python int doesn't fit, cannot convert to Go uint8", nil}, false},
		{func(a int) int { return a }, py.GoArgs{256}, nil, uint8(0),
			fmt.Errorf("Python int doesn't fit, cannot convert to Go uint8"), false},
		{func() error { return fmt.Errorf("some error") }, nil, nil, nil,
			&py.GoError{"RuntimeError", "some error", nil}, false},
		{func() error { return &py.GoError{"ValueError", "some python error", nil} }, nil, nil, nil,
			&py.GoError{"ValueError", "some python error", nil}, false},
		{(func())(nil), nil, nil, nil,
			fmt.Errorf("cannot convert a Go <nil> to Python GoFunction"), false},
		{py.GoFunction{}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go <nil> to Python GoFunction"), false},
		{py.GoFunction{Fn: 0}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go int to Python GoFunction"), false},

		{func(a, b py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func(py.Py, py.Py) to Python GoFunction: cannot handle multiple py.Py parameters"), false},
		{func(_ *py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func(*py.Py) to Python GoFunction: cannot handle *py.Py parameters"), false},
		{func(_ []py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func([]py.Py) to Python GoFunction: cannot handle []py.Py parameters"), false},
		{func(_ *[]py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func(*[]py.Py) to Python GoFunction: cannot handle *[]py.Py parameters"), false},
		{func(_ []*py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func([]*py.Py) to Python GoFunction: cannot handle []*py.Py parameters"), false},
		{func(_ int, _ ...py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func(int, ...py.Py) to Python GoFunction: cannot handle a variable number of py.Py parameters"), false},
		{func(_ int, _ ...*py.Py) {}, nil, nil, nil,
			fmt.Errorf("cannot convert a Go func(int, ...*py.Py) to Python GoFunction: cannot handle a variable number of *py.Py parameters"), false},
	} {
		fn := tc.fn
		if _, ok := fn.(py.GoFunction); !ok && tc.access {
			fn = py.GoFunction{Fn: fn, InterpreterAccess: tc.access}
		}

		o, err := Py.GoToObject(fn)
		defer Py.DecRef(o)
		if err != nil {
			expectError(t, err, tc.err)
			continue
		}
		expect(t, o.Type(), Py.GoFunction_Type())

		expectObjectAttr(t, Py, o, "__class__", Py.GoFunction_Type().AsObject(), nil)
		expectObjectAttr(t, Py, o, "__name__", py.None, nil)
		expectObjectAttr(t, Py, o, "__module__", py.None, nil)

		if fn, ok := fn.(py.GoFunction); ok {
			expectObjectAttr(t, Py, o, "__doc__", reflect.TypeOf(fn.Fn).String(), nil)
		} else {
			expectObjectAttr(t, Py, o, "__doc__", reflect.TypeOf(tc.fn).String(), nil)
		}

		o_ret, err := Py.Object_Call(o, tc.args, tc.kwargs)
		defer Py.DecRef(o_ret)
		if err != nil {
			expectError(t, err, tc.err)
			continue
		}
		expectObject(t, Py, o_ret, tc.result, tc.err)
	}
}

func TestFunctionFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	abs_inc := func(n int) int {
		if n > 0 {
			return n + 1
		} else if n < 0 {
			return n - 1
		}
		return 0
	}

	o, err := Py.GoToObject(abs_inc)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), Py.GoFunction_Type())

	var f0 func()
	err = Py.GoFromObject(o, &f0)
	expectError(t, err, fmt.Errorf("cannot convert a Python <func(int) int> to Go <func()>"))

	var f1 func(int)
	err = Py.GoFromObject(o, &f1)
	expectError(t, err, fmt.Errorf("cannot convert a Python <func(int) int> to Go <func(int)>"))

	var f2 func(int) int
	err = Py.GoFromObject(o, &f2)
	expectError(t, err, nil)
	expectError(t, quick.CheckEqual(f2, abs_inc, nil), nil)

	var f3 func()
	err = Py.GoFromObject(py.True, &f3)
	expectError(t, err, fmt.Errorf("cannot convert a Python bool to Go func()"))

	var i0 int
	err = Py.GoFromObject(o, &i0)
	expectError(t, err, fmt.Errorf("cannot convert a Python GoFunction to Go int"))

	var a0 interface{}
	err = Py.GoFromObject(o, &a0)
	expectError(t, err, nil)
	_, ok := a0.(func())
	expect(t, ok, false)

	var a1 interface{}
	err = Py.GoFromObject(o, &a1)
	expectError(t, err, nil)
	_, ok = a1.(func(int))
	expect(t, ok, false)

	var a2 interface{}
	err = Py.GoFromObject(o, &a2)
	expectError(t, err, nil)
	_, ok = a2.(func(int) int)
	expect(t, ok, true)
	expectError(t, quick.CheckEqual(a2, abs_inc, nil), nil)
}

func benchmarkCallFunction(b *testing.B, fn interface{}, params ...interface{}) {
	b.Helper()

	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(b, err, nil)

	o_fn, err := Py.GoToObject(fn)
	defer Py.DecRef(o_fn)
	expectError(b, err, nil)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		o_ret, _ := Py.Object_CallFunction(o_fn, params...)
		Py.DecRef(o_ret)
	}
	b.StopTimer()
}

func BenchmarkCallFunctionNoArgs(b *testing.B) {
	benchmarkCallFunction(b,
		func() {})
}

func BenchmarkCallFunctionNoArgsRetInt(b *testing.B) {
	benchmarkCallFunction(b,
		func() int { return 42 })
}

func BenchmarkCallFunctionNoArgsRetNilErr(b *testing.B) {
	benchmarkCallFunction(b,
		func() error { return nil })
}

func BenchmarkCallFunctionNoArgsRetErr(b *testing.B) {
	benchmarkCallFunction(b,
		func() error { return fmt.Errorf("test error") })
}

func BenchmarkCallFunctionPyArg(b *testing.B) {
	benchmarkCallFunction(b,
		func(_ py.Py) {})
}

func BenchmarkCallFunction3IntArgs(b *testing.B) {
	benchmarkCallFunction(b,
		func(_, _, _ int) {},
		0, 1, 2)
}

func BenchmarkCallFunction6IntArgs(b *testing.B) {
	benchmarkCallFunction(b,
		func(_, _, _, _, _, _ int) {},
		0, 1, 2, 3, 4, 5)
}

func BenchmarkCallFunction9IntArgs(b *testing.B) {
	benchmarkCallFunction(b,
		func(_, _, _, _, _, _, _, _, _ int) {},
		0, 1, 2, 3, 4, 5, 6, 7, 8)
}

func BenchmarkCallFunction9VarIntArgs(b *testing.B) {
	benchmarkCallFunction(b,
		func(_ ...int) {},
		0, 1, 2, 3, 4, 5, 6, 7, 8)
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"runtime"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestOSFileToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	test_lines := []string{
		"test line 1\n",
		"test line 2\n",
		"test line 3\n",
	}

	filename := "test-go-os-file.txt"
	file, err := os.Create(filename)
	expectError(t, err, nil)

	expectSubtest(t, Py, "Nil", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject((*os.File)(nil))
		defer Py.DecRef(o_file)
		expectError(t, err, fmt.Errorf("file is nil"))
	})

	expectSubtest(t, Py, "NotReadable", func(t *testing.T, Py py.Py) {
		file, err := os.OpenFile(filename, os.O_WRONLY, 0)
		expectError(t, err, nil)

		o_file, err := Py.GoToObject(file)
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "read")
		defer Py.DecRef(o_ret)
		expectError(t, err.(*py.GoError).Type, "OSError")
		expectMatch(t, err.(*py.GoError).Value, "Bad file descriptor")

		o_ret, err = Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		err = file.Close()
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "NotWritable", func(t *testing.T, Py py.Py) {
		file, err := os.OpenFile(filename, os.O_RDONLY, 0)
		expectError(t, err, nil)

		o_file, err := Py.GoToObject(py.GoOSFile{File: file, Mode: "w"})
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "write", "test")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		o_ret, err = Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err.(*py.GoError).Type, "OSError")
		expectMatch(t, err.(*py.GoError).Value, "Bad file descriptor")

		err = file.Close()
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "WriteLines", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject(py.GoOSFile{File: file, Mode: "w"})
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		for _, line := range test_lines {
			o_ret, err := Py.Object_CallMethod(o_file, "write", line)
			defer Py.DecRef(o_ret)
			expectError(t, err, nil)
			expectObject(t, Py, o_ret, len(line), nil)
		}

		o_ret, err := Py.Object_CallMethod(o_file, "flush")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		scanner := bufio.NewScanner(file)
		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)

		file.Seek(0, 0)
		scanner = bufio.NewScanner(file)
		for _, line := range test_lines {
			expect(t, scanner.Scan(), true)
			expectError(t, scanner.Err(), nil)
			expect(t, scanner.Text()+"\n", line)
		}

		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)

		o_ret, err = Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "ReadLines", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject(file)
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		file.Seek(0, 0)
		o_lines, err := Py.Object_CallMethod(o_file, "readlines")
		defer Py.DecRef(o_lines)
		expectError(t, err, nil)
		expectObject(t, Py, o_lines, test_lines, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "Seek", func(t *testing.T, Py py.Py) {
		o_file, err := Py.GoToObject(file)
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		o_pos, err := Py.Object_CallMethod(o_file, "tell")
		expectError(t, err, nil)

		pos, err := file.Seek(0, io.SeekCurrent)
		expectError(t, err, nil)

		expectObject(t, Py, o_pos, pos, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	err = file.Close()
	expectError(t, err, nil)
	err = os.Remove(filename)
	expectError(t, err, nil)
}

func TestOSFileFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	test_lines := []string{
		"test line 1\n",
		"test line 2\n",
		"test line 3\n",
	}

	filename := "test-go-os-file.txt"

	o_builtins, err := Py.Import_Import("builtins")
	defer Py.DecRef(o_builtins)
	expectError(t, err, nil)

	o_file, err := Py.Object_CallMethod(o_builtins, "open", filename, "w+")
	defer Py.DecRef(o_file)
	expectError(t, err, nil)

	expectSubtest(t, Py, "NotReadable", func(t *testing.T, Py py.Py) {
		o_file, err := Py.Object_CallMethod(o_builtins, "open", filename, "w")
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		var file *os.File
		err = Py.GoFromObject(o_file, &file)
		expectError(t, err, nil)

		buf := make([]byte, 1)
		_, err = file.Read(buf)
		if runtime.GOOS == "windows" {
			expectError(t, err.Error(), "read : Access is denied.")
		} else {
			expectError(t, err.Error(), "read : bad file descriptor")
		}

		err = file.Close()
		expectError(t, err, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "NotWritable", func(t *testing.T, Py py.Py) {
		o_file, err := Py.Object_CallMethod(o_builtins, "open", filename, "r")
		defer Py.DecRef(o_file)
		expectError(t, err, nil)

		var file *os.File
		err = Py.GoFromObject(o_file, &file)
		expectError(t, err, nil)

		buf := make([]byte, 1)
		_, err = file.Write(buf)
		if runtime.GOOS == "windows" {
			expectError(t, err.Error(), "write : Access is denied.")
		} else {
			expectError(t, err.Error(), "write : bad file descriptor")
		}

		err = file.Close()
		expectError(t, err, nil)

		o_ret, err := Py.Object_CallMethod(o_file, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "WriteLines", func(t *testing.T, Py py.Py) {
		var file *os.File
		err := Py.GoFromObject(o_file, &file)
		expectError(t, err, nil)

		for _, line := range test_lines {
			ret, err := file.WriteString(line)
			expectError(t, err, nil)
			expect(t, ret, len(line))
		}

		err = file.Sync()
		expectError(t, err, nil)

		file.Seek(0, 0)
		o_lines, err := Py.Object_CallMethod(o_file, "readlines")
		defer Py.DecRef(o_lines)
		expectError(t, err, nil)
		expectObject(t, Py, o_lines, test_lines, nil)

		err = file.Close()
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "ReadLines", func(t *testing.T, Py py.Py) {
		var file *os.File
		err := Py.GoFromObject(o_file, &file)
		expectError(t, err, nil)

		file.Seek(0, 0)
		scanner := bufio.NewScanner(file)
		for _, line := range test_lines {
			expect(t, scanner.Scan(), true)
			expectError(t, scanner.Err(), nil)
			expect(t, scanner.Text()+"\n", line)
		}

		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)

		err = file.Close()
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "NoFileno", func(t *testing.T, Py py.Py) {
		var file *os.File
		err := Py.GoFromObject(py.None, &file)
		expectError(t, err, fmt.Errorf("cannot convert a Python NoneType to Go *os.File"))
	})

	o_ret, err := Py.Object_CallMethod(o_file, "close")
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)
	err = os.Remove(filename)
	expectError(t, err, nil)
}

func TestOSPipeToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	test_lines := []string{
		"test line 1\n",
		"test line 2\n",
		"test line 3\n",
	}

	expectSubtest(t, Py, "WriteLines", func(t *testing.T, Py py.Py) {
		fileR, fileW, err := os.Pipe()
		expectError(t, err, nil)

		o_fileW, err := Py.GoToObject(py.GoOSFile{File: fileW, Mode: "w"})
		defer Py.DecRef(o_fileW)
		expectError(t, err, nil)
		err = fileW.Close()
		expectError(t, err, nil)

		for _, line := range test_lines {
			o_ret, err := Py.Object_CallMethod(o_fileW, "write", line)
			defer Py.DecRef(o_ret)
			expectError(t, err, nil)
			expectObject(t, Py, o_ret, len(line), nil)
		}

		o_ret, err := Py.Object_CallMethod(o_fileW, "flush")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		scanner := bufio.NewScanner(fileR)
		for _, line := range test_lines {
			expect(t, scanner.Scan(), true)
			expectError(t, scanner.Err(), nil)
			expect(t, scanner.Text()+"\n", line)
		}

		o_ret, err = Py.Object_CallMethod(o_fileW, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)

		expect(t, scanner.Scan(), false)
		expectError(t, scanner.Err(), nil)

		err = fileR.Close()
		expectError(t, err, nil)
	})

	expectSubtest(t, Py, "ReadLines", func(t *testing.T, Py py.Py) {
		fileR, fileW, err := os.Pipe()
		expectError(t, err, nil)

		o_fileR, err := Py.GoToObject(fileR)
		defer Py.DecRef(o_fileR)
		expectError(t, err, nil)
		err = fileR.Close()
		expectError(t, err, nil)

		for _, line := range test_lines {
			ret, err := fileW.WriteString(line)
			expectError(t, err, nil)
			expect(t, ret, len(line))
		}

		err = fileW.Close()
		expectError(t, err, nil)

		o_lines, err := Py.Object_CallMethod(o_fileR, "readlines")
		defer Py.DecRef(o_lines)
		expectError(t, err, nil)
		expectObject(t, Py, o_lines, test_lines, nil)

		o_ret, err := Py.Object_CallMethod(o_fileR, "close")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})
}

func TestConsole(t *testing.T) {
	prompt := "test prompt"
	response := "test response"

	stdinR, stdinW, err := os.Pipe()
	expectError(t, err, nil)

	stdoutR, stdoutW, err := os.Pipe()
	expectError(t, err, nil)

	stderrR, stderrW, err := os.Pipe()
	expectError(t, err, nil)

	t.Run("InputOutput", func(t *testing.T) {
		Py, err := py.GoEmbed()
		defer Py.Close()
		expectError(t, err, nil)

		o_sys, err := Py.Import_Import("sys")
		defer Py.DecRef(o_sys)
		expectError(t, err, nil)

		o_builtins, err := Py.Import_Import("builtins")
		defer Py.DecRef(o_builtins)
		expectError(t, err, nil)

		o_warnings, err := Py.Import_Import("warnings")
		defer Py.DecRef(o_warnings)
		expectError(t, err, nil)

		for _, f := range []struct {
			file *os.File
			name string
			mode string
		}{
			{stdinR, "stdin", "r"},
			{stdoutW, "stdout", "w"},
			{stderrW, "stderr", "w"},
		} {
			o_file, err := Py.Object_GetAttr(o_sys, f.name)
			defer Py.DecRef(o_file)
			expectError(t, err, nil)

			o_ret, err := Py.Object_CallMethod(o_file, "close")
			defer Py.DecRef(o_ret)
			expectError(t, err, nil)

			o_file, err = Py.GoToObject(py.GoOSFile{File: f.file, Mode: f.mode})
			defer Py.DecRef(o_file)
			expectError(t, err, nil)

			err = f.file.Close()
			expectError(t, err, nil)

			err = Py.Object_SetAttr(o_sys, f.name, o_file)
			expectError(t, err, nil)

			o_fd, err := Py.Object_CallMethod(o_file, "fileno")
			defer Py.DecRef(o_fd)
			expectError(t, err, nil)

			var fd int
			err = Py.GoFromObject(o_fd, &fd)
			expectError(t, err, nil)

			message := fmt.Sprintf("unclosed file .* name=%d mode='%s'", fd, f.mode)
			o_ret, err = Py.Object_CallMethod(o_warnings, "filterwarnings", "ignore", message, py.Exc_ResourceWarning)
			defer Py.DecRef(o_ret)
			expectError(t, err, nil)
		}

		_, err = stdinW.WriteString(response + "\n")
		expectError(t, err, nil)

		o_response, err := Py.Object_CallMethod(o_builtins, "input", prompt+"\n")
		defer Py.DecRef(o_response)
		expectError(t, err, nil)
		expectObject(t, Py, o_response, response, nil)

		o_ret, err := Py.Object_CallMethod(o_builtins, "print", "bye!")
		defer Py.DecRef(o_ret)
		expectError(t, err, nil)
	})

	scanner := bufio.NewScanner(stdoutR)
	expect(t, scanner.Scan(), true)
	expect(t, scanner.Text(), prompt)
	expect(t, scanner.Scan(), true)
	expect(t, scanner.Text(), "bye!")
	expect(t, scanner.Scan(), false)
	expectError(t, scanner.Err(), nil)

	scanner = bufio.NewScanner(stderrR)
	expect(t, scanner.Scan(), false)
	expectError(t, scanner.Err(), nil)

	err = stdinW.Close()
	expectError(t, err, nil)
	err = stdoutR.Close()
	expectError(t, err, nil)
	err = stderrR.Close()
	expectError(t, err, nil)
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"testing"

	"gitlab.com/pygolo/py"
)

type TestStruct struct {
	B bool `python:",omit"`
	I int
	S string `python:"s"`
	f float64
}

func (ts TestStruct) GetB() bool {
	return ts.B
}

func (ts TestStruct) GetI() int {
	return ts.I
}

func (ts TestStruct) GetS() string {
	return ts.S
}

func (ts *TestStruct) SetS(s string) {
	ts.S = s
}

func (ts TestStruct) getF() float64 {
	return ts.f
}

func (ts *TestStruct) setF(f float64) {
	ts.f = f
}

func (ts TestStruct) String() string {
	return fmt.Sprintf("%#v", ts)
}

func expectStructSubtest(t *testing.T, Py py.Py, s interface{}, fn func(*testing.T, py.Py, bool)) {
	t.Helper()

	expectSubtest(t, Py, "Unregistered", func(t *testing.T, Py py.Py) {
		fn(t, Py, false)
	})

	err := Py.GoRegisterStruct(s)
	defer Py.GoDeregisterStruct(s)
	expectError(t, err, nil)

	expectSubtest(t, Py, "Registered", func(t *testing.T, Py py.Py) {
		fn(t, Py, true)
	})
}

func TestStructType(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []interface{}{
		nil,
		(*int)(nil),
		0,
		"",
	} {
		err := Py.GoRegisterStruct(tc)
		expectError(t, err, fmt.Errorf("could not normalize struct: not a struct or a pointer to struct: %T", tc))
	}

	type S struct{}

	t_s, ok := Py.GoGetStructType(S{})
	expect(t, ok, false)
	t_s, ok = Py.GoGetStructType(&S{})
	expect(t, ok, false)

	err = Py.GoRegisterStruct(S{})
	expectError(t, err, nil)

	t_s, ok = Py.GoGetStructType(S{})
	defer Py.DecRef(t_s.AsObject())
	expect(t, ok, true)
	t_s2, ok := Py.GoGetStructType(&S{})
	defer Py.DecRef(t_s2.AsObject())
	expect(t, ok, true)
	expect(t, t_s, t_s2)

	Py.GoDeregisterStruct(&S{})
	_, ok = Py.GoGetStructType(S{})
	expect(t, ok, false)
	_, ok = Py.GoGetStructType(&S{})
	expect(t, ok, false)

	type S2 struct{ S }

	err = Py.GoRegisterStruct(S2{})
	expectError(t, err, nil)
	t_s, ok = Py.GoGetStructType(S{})
	defer Py.DecRef(t_s.AsObject())
	expect(t, ok, true)

	Py.GoDeregisterStruct(S2{})
	_, ok = Py.GoGetStructType(S{})
	expect(t, ok, false)
}

func TestStructToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []struct {
		s interface{}
		e error
	}{
		{struct {
			B bool `python:",omitempty"`
		}{}, fmt.Errorf(`could not get struct fields: field 'B' has unknown tags: [omitempty]`)},
	} {
		_, err = Py.GoToObject(tc.s)
		expectError(t, err, tc.e)

		_, ok := Py.GoGetStructType(tc.s)
		expect(t, ok, false)

		err = Py.GoRegisterStruct(tc.s)
		expectError(t, err, tc.e)

		_, ok = Py.GoGetStructType(tc.s)
		expect(t, ok, false)
	}

	expectSubtest(t, Py, "StructWithFields", func(t *testing.T, Py py.Py) {
		type S struct {
			B bool `python:" , omit "` // extra spaces to test the tag trimming
			I int
			S string `python:"   s  "` // extra spaces to test the tag trimming
		}

		expectStructSubtest(t, Py, S{}, func(t *testing.T, Py py.Py, registered bool) {
			ts := S{I: 1, S: "one", B: true}
			ts2 := &S{I: 2, S: "two"}

			for i, x := range []interface{}{ts, &ts} {
				o, err := Py.GoToObject(x)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `&test.S{B:true, I:1, S:"one"}`, nil)
				expectObjectAttr(t, Py, o, "__class__", o.Type(), nil)

				o2, err := Py.GoToObject(ts2)
				defer Py.DecRef(o2)
				expectError(t, err, nil)
				expectObject(t, Py, o2, `&test.S{B:false, I:2, S:"two"}`, nil)
				expectObjectAttr(t, Py, o2, "__class__", o2.Type(), nil)

				// Python types are equal only if the Go struct was registered
				expect(t, o.Type() == o2.Type(), registered)

				_, err = Py.Object_GetAttr(o, "nonexistent")
				expectError(t, err, &py.GoError{"AttributeError", "'S' object has no attribute 'nonexistent'", nil})

				err = Py.Object_SetAttr(o, "nonexistent", false)
				expectError(t, err.(*py.GoError).Type, "AttributeError")
				expectMatch(t, err.(*py.GoError).Value, "'S' object has no attribute 'nonexistent'.*")

				expectObjectAttr(t, Py, o, "s", "one", nil)

				err = Py.Object_SetAttr(o, "s", "three")
				expectError(t, err, nil)
				expectObject(t, Py, o, `&test.S{B:true, I:1, S:"three"}`, nil)

				expectObjectAttr(t, Py, o, "s", "three", nil)

				if i == 0 {
					expect(t, ts.S, "one")
				} else {
					expect(t, ts.S, "three")
				}
			}
		})
	})

	expectSubtest(t, Py, "StructWithFieldsRename", func(t *testing.T, Py py.Py) {
		ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

		for _, x := range []interface{}{ts, &ts} {
			gs := py.GoStruct{
				Struct: x,
			}
			gs.Exclude("I")
			gs.Rename("S", "ß")

			expectStructSubtest(t, Py, gs, func(t *testing.T, Py py.Py, _ bool) {
				o, err := Py.GoToObject(gs)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"one", f:3.14}`, nil)
				expectObjectAttr(t, Py, o, "__class__", o.Type(), nil)

				expectObjectAttr(t, Py, o, "ß", "one", nil)

				_, err = Py.Object_GetAttr(o, "S")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'S'", nil})

				_, err = Py.Object_GetAttr(o, "I")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'I'", nil})
			})
		}
	})

	expectSubtest(t, Py, "StructWithMethods", func(t *testing.T, Py py.Py) {
		expectStructSubtest(t, Py, TestStruct{}, func(t *testing.T, Py py.Py, _ bool) {
			ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

			for i, x := range []interface{}{ts, &ts} {
				o, err := Py.GoToObject(x)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"one", f:3.14}`, nil)
				expectObjectAttr(t, Py, o, "__class__", o.Type(), nil)

				o_ret, err := Py.Object_CallMethod(o, "GetB")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.B, nil)

				o_ret, err = Py.Object_CallMethod(o, "GetI")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.I, nil)

				o_ret, err = Py.Object_CallMethod(o, "GetS")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.S, nil)

				o_ret, err = Py.Object_CallMethod(o, "SetS", "three")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"three", f:3.14}`, nil)

				o_ret, err = Py.Object_CallMethod(o, "GetS")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, "three", nil)

				if i == 0 {
					expect(t, ts.S, "one")
				} else {
					expect(t, ts.S, "three")
				}

				_, err = Py.Object_CallMethod(o, "getF")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'getF'", nil})
			}
		})
	})

	expectSubtest(t, Py, "StructWithMethodsRename", func(t *testing.T, Py py.Py) {
		ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

		for _, x := range []interface{}{ts, &ts} {
			gs := py.GoStruct{
				Struct: x,
			}
			gs.Exclude("GetB")
			gs.Rename("GetS", "get_s")

			expectStructSubtest(t, Py, gs, func(t *testing.T, Py py.Py, _ bool) {
				o, err := Py.GoToObject(gs)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"one", f:3.14}`, nil)
				expectObjectAttr(t, Py, o, "__class__", o.Type(), nil)

				_, err = Py.Object_CallMethod(o, "GetB")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'GetB'", nil})

				_, err = Py.Object_CallMethod(o, "GetS")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'GetS'", nil})

				o_ret, err := Py.Object_CallMethod(o, "GetI")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.I, nil)

				o_ret, err = Py.Object_CallMethod(o, "get_s")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.S, nil)
			})
		}
	})

	expectSubtest(t, Py, "StructWithProperties", func(t *testing.T, Py py.Py) {
		gs := py.GoStruct{
			Struct: TestStruct{},
		}
		gs.Property("f", TestStruct.getF, (*TestStruct).setF)

		expectStructSubtest(t, Py, gs, func(t *testing.T, Py py.Py, _ bool) {
			ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

			for i, x := range []interface{}{ts, &ts} {
				gs.Struct = x

				o, err := Py.GoToObject(gs)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"one", f:3.14}`, nil)
				expectObjectAttr(t, Py, o, "f", 3.14, nil)

				err = Py.Object_SetAttr(o, "f", 6.28)
				expectError(t, err, nil)
				expectObject(t, Py, o, `test.TestStruct{B:true, I:1, S:"one", f:6.28}`, nil)
				expectObjectAttr(t, Py, o, "f", 6.28, nil)

				if i == 0 {
					expect(t, ts.f, 3.14)
				} else {
					expect(t, ts.f, 6.28)
				}
			}
		})
	})

	expectSubtest(t, Py, "StructWithMembersRenameErrors", func(t *testing.T, Py py.Py) {
		ts := TestStruct{}

		for _, x := range []interface{}{ts, &ts} {
			gs := py.GoStruct{
				Struct: x,
			}

			// ok
			gs.Rename("B", "b")
			gs.Rename("S", "s")
			gs.Exclude("GetS")

			// not ok
			gs.Rename("getF", "GetF")
			gs.Exclude("GetX")

			_, err := Py.GoToObject(gs)
			expectError(t, err, fmt.Errorf("could not rename member(s): B, GetX, getF"))

			err = Py.GoRegisterStruct(gs)
			expectError(t, err, fmt.Errorf("could not rename member(s): B, GetX, getF"))

			_, ok := Py.GoGetStructType(gs)
			expect(t, ok, false)
		}
	})

	expectSubtest(t, Py, "StructWithOmitByDefault", func(t *testing.T, Py py.Py) {
		ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

		for _, x := range []interface{}{ts, &ts} {
			gs := py.GoStruct{
				Struct:        x,
				OmitByDefault: true,
			}
			gs.Rename("S", "s")
			gs.Include("GetI")

			expectStructSubtest(t, Py, gs, func(t *testing.T, Py py.Py, _ bool) {
				o, err := Py.GoToObject(gs)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObjectAttr(t, Py, o, "__class__", o.Type(), nil)

				o_s, err := Py.Object_GetAttr(o, "s")
				defer Py.DecRef(o_s)
				expectError(t, err, nil)
				expectObject(t, Py, o_s, ts.S, nil)

				o_ret, err := Py.Object_CallMethod(o, "GetI")
				defer Py.DecRef(o_ret)
				expectError(t, err, nil)
				expectObject(t, Py, o_ret, ts.I, nil)

				_, err = Py.Object_GetAttr(o, "I")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'I'", nil})

				_, err = Py.Object_CallMethod(o, "GetS")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'GetS'", nil})

				_, err = Py.Object_CallMethod(o, "GetB")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'GetB'", nil})

				_, err = Py.Object_CallMethod(o, "SetS")
				expectError(t, err, &py.GoError{"AttributeError", "'TestStruct' object has no attribute 'SetS'", nil})
			})
		}
	})

	expectSubtest(t, Py, "StructWithOmitByDefaultError", func(t *testing.T, Py py.Py) {
		ts := TestStruct{I: 1, S: "one", B: true, f: 3.14}

		for _, x := range []interface{}{ts, &ts} {
			gs := py.GoStruct{
				Struct:        x,
				OmitByDefault: true,
			}
			gs.Include("B")
			gs.Include("f")

			_, err := Py.GoToObject(gs)
			expectError(t, err, fmt.Errorf("could not rename member(s): B, f"))

			err = Py.GoRegisterStruct(gs)
			expectError(t, err, fmt.Errorf("could not rename member(s): B, f"))

			_, ok := Py.GoGetStructType(gs)
			expect(t, ok, false)
		}
	})

	expectSubtest(t, Py, "NestedStruct", func(t *testing.T, Py py.Py) {
		type S struct {
			B bool `python:",omit"`
			I int
			S string `python:"s"`
		}

		type S2 struct {
			S `python:"s"`
			B bool
		}

		ts := S2{S: S{I: 3, S: "three"}}

		for _, x := range []interface{}{ts, &ts} {
			expectStructSubtest(t, Py, ts, func(t *testing.T, Py py.Py, _ bool) {
				o, err := Py.GoToObject(x)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `&test.S2{S:test.S{B:false, I:3, S:"three"}, B:false}`, nil)

				Py.GoDeregisterStruct(S{})

				o, err = Py.GoToObject(x)
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, `&test.S2{S:test.S{B:false, I:3, S:"three"}, B:false}`, nil)
			})
		}
	})
}

func TestStructFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expectSubtest(t, Py, "SimpleStruct", func(t *testing.T, Py py.Py) {
		type S struct {
			B bool `python:",omit"`
			I int
			S string `python:"s"`
		}

		ts := S{I: 1, S: "one"}

		expectStructSubtest(t, Py, ts, func(t *testing.T, Py py.Py, registered bool) {
			o, err := Py.GoToObject(ts)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, `&test.S{B:false, I:1, S:"one"}`, nil)

			var s2 S
			err = Py.GoFromObject(o, &s2)
			expectError(t, err, nil)
			expect(t, s2, ts)

			var s3 struct{ I int }
			err = Py.GoFromObject(o, &s3)
			expectError(t, err, nil)
			expect(t, s3.I, ts.I)

			var s4 struct {
				N int    `python:"I"`
				S string `python:"s"`
			}
			err = Py.GoFromObject(o, &s4)
			expectError(t, err, nil)
			expect(t, s4.N, ts.I)
			expect(t, s4.S, ts.S)

			if registered {
				var a interface{}
				err = Py.GoFromObject(o, &a)
				expectError(t, err, nil)
				expect(t, a.(*S), &ts)
			}
		})
	})

	expectSubtest(t, Py, "NestedStruct", func(t *testing.T, Py py.Py) {
		type S struct {
			B bool `python:",omit"`
			I int
			S string `python:"s"`
		}

		type S2 struct {
			S `python:"s"`
			B bool
		}

		ts := S2{S: S{I: 3, S: "three"}}

		expectStructSubtest(t, Py, ts, func(t *testing.T, Py py.Py, registered bool) {
			o, err := Py.GoToObject(ts)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, `&test.S2{S:test.S{B:false, I:3, S:"three"}, B:false}`, nil)

			var s2 S2
			err = Py.GoFromObject(o, &s2)
			expectError(t, err, nil)
			expect(t, s2, ts)

			if registered {
				var a interface{}
				err = Py.GoFromObject(o, &a)
				expectError(t, err, nil)
				expect(t, a.(*S2), &ts)
			}
		})
	})

	expectSubtest(t, Py, "Object", func(t *testing.T, Py py.Py) {
		type S struct {
			B bool `python:",omit"`
			I int
			S string `python:"s"`
		}

		expectStructSubtest(t, Py, S{}, func(t *testing.T, Py py.Py, registered bool) {
			body := map[string]interface{}{"I": 1, "B": true, "s": "one"}
			o, err := Py.Object_CallFunction(py.Type_Type.AsObject(), "MyType", [0]interface{}{}, body)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, S{I: 1, S: "one"}, nil)

			body = map[string]interface{}{"I": 1, "B": true, "s": "one"}
			o, err = Py.Object_CallFunction(py.Type_Type.AsObject(), "MyType", [0]interface{}{}, body)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, &S{I: 1, S: "one"}, fmt.Errorf("cannot convert a Python type to Go *test.S"))

			body = map[string]interface{}{"I": 1, "B": true, "s": 1}
			o, err = Py.Object_CallFunction(py.Type_Type.AsObject(), "MyType", [0]interface{}{}, body)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, S{I: 1, S: "1"}, nil)

			body = map[string]interface{}{"I": 1, "B": true}
			o, err = Py.Object_CallFunction(py.Type_Type.AsObject(), "MyType", [0]interface{}{}, body)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, S{}, &py.GoError{"AttributeError", "type object 'MyType' has no attribute 's'", nil})

			body = map[string]interface{}{"I": "1", "B": true, "s": "one"}
			o, err = Py.Object_CallFunction(py.Type_Type.AsObject(), "MyType", [0]interface{}{}, body)
			defer Py.DecRef(o)
			expectError(t, err, nil)
			expectObject(t, Py, o, S{I: 1, S: "1"}, fmt.Errorf(`attr "I": cannot convert a Python str to Go int`))

			if registered {
				t_S, ok := Py.GoGetStructType(S{})
				defer Py.DecRef(t_S.AsObject())
				expect(t, ok, true)

				o, err = Py.Object_CallFunction(t_S.AsObject(), 1, "one")
				defer Py.DecRef(o)
				expectError(t, err, nil)
				expectObject(t, Py, o, S{I: 1, S: "one"}, nil)
				expectObjectAttr(t, Py, o, "__class__", t_S, nil)
			}
		})
	})
}

func BenchmarkStructToObjectRegistered(b *testing.B) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(b, err, nil)

	ts := testStruct{}

	err = Py.GoRegisterStruct(ts)
	defer Py.GoDeregisterStruct(ts)
	expectError(b, err, nil)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		o, err := Py.GoToObject(ts)
		defer Py.DecRef(o)
		expectError(b, err, nil)
	}
	b.StopTimer()
}

func BenchmarkStructToObjectUnregistered(b *testing.B) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(b, err, nil)

	ts := testStruct{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		o, err := Py.GoToObject(ts)
		defer Py.DecRef(o)
		expectError(b, err, nil)
	}
	b.StopTimer()
}

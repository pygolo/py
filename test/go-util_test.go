/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"go/build"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestCatchError(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	err = Py.GoCatchError()
	expectError(t, err, nil)

	Py.Err_SetString(py.Exc_RuntimeError, "test error")
	err = Py.GoCatchError()
	expectError(t, err, &py.GoError{"RuntimeError", "test error", nil})

	err = Py.GoCatchError()
	expectError(t, err, nil)

	o_globals, err := Py.Dict_New()
	defer Py.DecRef(o_globals)
	expectError(t, err, nil)

	o_locals, err := Py.Dict_New()
	defer Py.DecRef(o_locals)
	expectError(t, err, nil)

	o_code, err := Py.CompileString("def foo():\n  bar()\nfoo()", "test", py.File_input)
	defer Py.DecRef(o_code)
	expectError(t, err, nil)

	pkg, err := build.Import("gitlab.com/pygolo/py", "", build.FindOnly)
	expectError(t, err, nil)

	fixPath := func(path string) string {
		return regexp.QuoteMeta(pkg.Dir + filepath.FromSlash(path))
	}

	o_ret, err := Py.Eval_EvalCode(o_code, o_globals, o_locals)
	py_err, ok := err.(*py.GoError)
	expect(t, ok, true)
	expect(t, py_err.Type, "NameError")
	expect(t, py_err.Value, "name 'bar' is not defined")
	expectMatch(t, strings.Join(py_err.Traceback, ""),
		`Traceback \(most recent call last\):\n`+
			`  File "`+fixPath("/test/go-util_test.go")+`", line \d+, in gitlab.com/pygolo/py/test.TestCatchError\n`+
			`  File "`+fixPath("/eval.go")+`", line \d+, in gitlab.com/pygolo/py.Py.Eval_EvalCode\n`+
			`  File "test", line 3, in <module>\n`+
			`  File "test", line 2, in foo\n`)
	expect(t, o_ret, py.Object{})
}

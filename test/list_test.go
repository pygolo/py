/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"math"
	"strconv"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestList(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	tc := []interface{}{}

	o, err := Py.GoToObject(tc)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.List_Type)
	expect(t, Py.List_CheckExact(o), true)
	expectObjectLen(t, Py, o, len(tc))

	expectObject(t, Py, py.True, tc, &py.GoError{"TypeError", "object of type 'bool' has no len()", nil})
}

func TestListBool(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := []bool{
		true,
		false,
		true,
		false,
		true,
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.List_Type)
		expect(t, Py.List_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		test2 := []interface{}{}
		err = Py.GoFromObject(o, &test2)
		expectError(t, err, nil)
		expect(t, len(test2), len(tc))
		for i, elem := range test2 {
			expectElem(t, i, elem, tc[i])
		}

		var test3 []interface{}
		err = Py.GoFromObject(o, &test3)
		expectError(t, err, nil)
		expect(t, len(test3), len(tc))
		for i, elem := range test3 {
			expectElem(t, i, elem, tc[i])
		}

		var test4 interface{}
		err = Py.GoFromObject(o, &test4)
		expectError(t, err, nil)
		expect(t, len(test4.([]interface{})), len(tc))
		for i, elem := range test4.([]interface{}) {
			expectElem(t, i, elem, tc[i])
		}

		expectObject(t, Py, o, []int(nil), fmt.Errorf("item #0: cannot convert a Python bool to Go int"))
		expectObject(t, Py, o, []int{}, fmt.Errorf("item #0: cannot convert a Python bool to Go int"))
		expectObject(t, Py, o, int(0), fmt.Errorf("cannot convert a Python list to Go int"))
	})
}

func TestListString(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := []string{
		``,
		`Hello, 世界`,
		`Hello, world!\0`,
		`With\0 null\0 chars\0`,
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.List_Type)
		expect(t, Py.List_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		test2 := []interface{}{}
		err = Py.GoFromObject(o, &test2)
		expectError(t, err, nil)
		expect(t, len(test2), len(tc))
		for i, elem := range test2 {
			expectElem(t, i, elem, tc[i])
		}

		var test3 []interface{}
		err = Py.GoFromObject(o, &test3)
		expectError(t, err, nil)
		expect(t, len(test3), len(tc))
		for i, elem := range test3 {
			expectElem(t, i, elem, tc[i])
		}

		var test4 interface{}
		err = Py.GoFromObject(o, &test4)
		expectError(t, err, nil)
		expect(t, len(test4.([]interface{})), len(tc))
		for i, elem := range test4.([]interface{}) {
			expectElem(t, i, elem, tc[i])
		}

		expectObject(t, Py, o, []int(nil), fmt.Errorf("item #0: cannot convert a Python str to Go int"))
		expectObject(t, Py, o, []int{}, fmt.Errorf("item #0: cannot convert a Python str to Go int"))
		expectObject(t, Py, o, int(0), fmt.Errorf("cannot convert a Python list to Go int"))
	})
}

func TestListInterface(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.Object
	defer Py.DecRef(o)

	tc := []interface{}{
		``,
		`Hello, 世界`,
		`Hello, world!\0`,
		`With\0 null\0 chars\0`,
		true,
		false,
		1,
		math.Pi,
		[]interface{}{
			true,
			`Hello, 世界`,
			0,
			[]interface{}{
				[]interface{}{
					false,
					-1,
				},
			},
		},
	}

	expectSubtest(t, Py, "ToObject", func(t *testing.T, Py py.Py) {
		o, err = Py.GoToObject(tc)
		expectError(t, err, nil)
		expect(t, o.Type(), py.List_Type)
		expect(t, Py.List_CheckExact(o), true)
		expectObjectLen(t, Py, o, len(tc))
	})

	expectSubtest(t, Py, "FromObject", func(t *testing.T, Py py.Py) {
		expectObject(t, Py, o, tc, nil)

		var test2 []interface{}
		err = Py.GoFromObject(o, &test2)
		expectError(t, err, nil)
		expect(t, test2, tc)

		expectObjectAny(t, Py, o, tc, nil)

		expectObject(t, Py, o, []bool{
			false,
			true,
			true,
			true,
			true,
			false,
			true,
			true,
			true,
		}, nil)

		expectObject(t, Py, o, []string{
			``,
			`Hello, 世界`,
			`Hello, world!\0`,
			`With\0 null\0 chars\0`,
			`True`,
			`False`,
			`1`,
			strconv.FormatFloat(math.Pi, 'f', -1, 64),
			`[True, 'Hello, 世界', 0, [[False, -1]]]`,
		}, nil)

		expectObject(t, Py, o, []int(nil), fmt.Errorf("item #0: cannot convert a Python str to Go int"))
		expectObject(t, Py, o, []int{}, fmt.Errorf("item #0: cannot convert a Python str to Go int"))
		expectObject(t, Py, o, int(0), fmt.Errorf("cannot convert a Python list to Go int"))
	})
}

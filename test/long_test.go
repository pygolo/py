/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"math"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestLong(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, i := range []int{minInt, -1, maxInt} {
		o, err := Py.Long_FromInt(i)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Long_Type)
		expect(t, Py.Long_CheckExact(o), true)
		test, err := Py.Long_AsInt(o)
		expectError(t, err, nil)
		expect(t, test, int(i))
	}

	for _, i := range []int64{math.MinInt64, -1, math.MaxInt64} {
		o, err := Py.Long_FromInt64(i)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Long_Type)
		expect(t, Py.Long_CheckExact(o), true)
		test, err := Py.Long_AsInt64(o)
		expectError(t, err, nil)
		expect(t, test, int64(i))
	}

	o, err := Py.Long_FromUint(maxUint)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Long_Type)
	expect(t, Py.Long_CheckExact(o), true)
	test, err := Py.Long_AsUint(o)
	expectError(t, err, nil)
	expect(t, test, uint(maxUint))
	_, err = Py.Long_AsInt(o)
	expectError(t, err, fmt.Errorf("Python int doesn't fit, cannot convert to Go int"))

	o, err = Py.Long_FromUint64(math.MaxUint64)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Long_Type)
	expect(t, Py.Long_CheckExact(o), true)
	test2, err := Py.Long_AsUint64(o)
	expectError(t, err, nil)
	expect(t, test2, uint64(math.MaxUint64))
	_, err = Py.Long_AsInt(o)
	expectError(t, err, fmt.Errorf("Python int doesn't fit, cannot convert to Go int"))
	_, err = Py.Long_AsInt64(o)
	expectError(t, err, fmt.Errorf("Python int doesn't fit, cannot convert to Go int64"))

	o, err = Py.Long_FromUintptr(maxUintptr)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Long_Type)
	expect(t, Py.Long_CheckExact(o), true)
	test3, err := Py.Long_AsUintptr(o)
	expectError(t, err, nil)
	expect(t, test3, uintptr(maxUintptr))
	_, err = Py.Long_AsInt(o)
	expectError(t, err, fmt.Errorf("Python int doesn't fit, cannot convert to Go int"))
}

func TestLongToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []struct {
		as    interface{}
		value interface{}
		err   error
	}{
		{Py.Long_AsInt, int(minInt), nil},
		{Py.Long_AsInt, int(-1), nil},
		{Py.Long_AsInt, int(maxInt), nil},
		{Py.Long_AsInt, uint(maxUint), fmt.Errorf("Python int doesn't fit, cannot convert to Go int")},
		{Py.Long_AsInt, uint64(math.MaxUint64), fmt.Errorf("Python int doesn't fit, cannot convert to Go int")},

		{Py.Long_AsInt64, int64(math.MinInt64), nil},
		{Py.Long_AsInt64, int64(-1), nil},
		{Py.Long_AsInt64, int64(math.MaxInt64), nil},
		{Py.Long_AsInt64, uint64(math.MaxUint64), fmt.Errorf("Python int doesn't fit, cannot convert to Go int64")},

		{Py.Long_AsUint, int(minInt), fmt.Errorf("Python int is negative, cannot convert to Go uint")},
		{Py.Long_AsUint, int64(math.MinInt64), fmt.Errorf("Python int is negative, cannot convert to Go uint")},
		{Py.Long_AsUint, uint(maxUint), nil},

		{Py.Long_AsUint64, int(minInt), fmt.Errorf("Python int is negative, cannot convert to Go uint64")},
		{Py.Long_AsUint64, int64(math.MinInt64), fmt.Errorf("Python int is negative, cannot convert to Go uint64")},
		{Py.Long_AsUint64, uint64(math.MaxUint64), nil},

		{Py.Long_AsUintptr, int(minInt), fmt.Errorf("Python int is negative, cannot convert to Go uintptr")},
		{Py.Long_AsUintptr, int64(math.MinInt64), fmt.Errorf("Python int is negative, cannot convert to Go uintptr")},
		{Py.Long_AsUintptr, uintptr(maxUintptr), nil},
	} {
		o, err := Py.GoToObject(tc.value)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Long_Type)
		expect(t, Py.Long_CheckExact(o), true)

		var test interface{}
		switch as := tc.as.(type) {
		case func(py.Object) (int, error):
			test, err = as(o)
		case func(py.Object) (int64, error):
			test, err = as(o)
		case func(py.Object) (uint, error):
			test, err = as(o)
		case func(py.Object) (uint64, error):
			test, err = as(o)
		case func(py.Object) (uintptr, error):
			test, err = as(o)
		default:
			panic("unsupported signature")
		}
		expectError(t, err, tc.err)
		if err == nil {
			expect(t, test, tc.value)
		}
	}
}

func TestLongFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range []struct {
		from  interface{}
		value interface{}
		test  interface{}
		err   error
	}{
		{Py.Long_FromInt, int(math.MinInt8), int8(math.MinInt8), nil},
		{Py.Long_FromInt, int(math.MaxInt8), int8(math.MaxInt8), nil},
		{Py.Long_FromInt, int(math.MinInt16), int16(math.MinInt16), nil},
		{Py.Long_FromInt, int(math.MaxInt16), int16(math.MaxInt16), nil},
		{Py.Long_FromInt, int(math.MinInt32), int32(math.MinInt32), nil},
		{Py.Long_FromInt, int(math.MaxInt32), int32(math.MaxInt32), nil},
		{Py.Long_FromInt, int(math.MinInt8 - 1), int8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int8")},
		{Py.Long_FromInt, int(math.MaxInt8 + 1), int8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int8")},
		{Py.Long_FromInt, int(math.MinInt16 - 1), int16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int16")},
		{Py.Long_FromInt, int(math.MaxInt16 + 1), int16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int16")},
		{Py.Long_FromInt, int(-1), uint(0), fmt.Errorf("Python int is negative, cannot convert to Go uint")},
		{Py.Long_FromInt, int(-1), uint8(0), fmt.Errorf("Python int is negative, cannot convert to Go uint8")},
		{Py.Long_FromInt, int(-1), uint16(0), fmt.Errorf("Python int is negative, cannot convert to Go uint16")},
		{Py.Long_FromInt, int(-1), uint32(0), fmt.Errorf("Python int is negative, cannot convert to Go uint32")},
		{Py.Long_FromInt, int(-1), uint64(0), fmt.Errorf("Python int is negative, cannot convert to Go uint64")},

		{Py.Long_FromInt64, int64(math.MinInt8), int8(math.MinInt8), nil},
		{Py.Long_FromInt64, int64(math.MaxInt8), int8(math.MaxInt8), nil},
		{Py.Long_FromInt64, int64(math.MinInt16), int16(math.MinInt16), nil},
		{Py.Long_FromInt64, int64(math.MaxInt16), int16(math.MaxInt16), nil},
		{Py.Long_FromInt64, int64(math.MinInt32), int32(math.MinInt32), nil},
		{Py.Long_FromInt64, int64(math.MaxInt32), int32(math.MaxInt32), nil},
		{Py.Long_FromInt64, int64(math.MinInt64), int64(math.MinInt64), nil},
		{Py.Long_FromInt64, int64(math.MaxInt64), int64(math.MaxInt64), nil},
		{Py.Long_FromInt64, int64(math.MinInt8 - 1), int8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int8")},
		{Py.Long_FromInt64, int64(math.MaxInt8 + 1), int8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int8")},
		{Py.Long_FromInt64, int64(math.MinInt16 - 1), int16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int16")},
		{Py.Long_FromInt64, int64(math.MaxInt16 + 1), int16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int16")},
		{Py.Long_FromInt64, int64(math.MinInt32 - 1), int32(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int32")},
		{Py.Long_FromInt64, int64(math.MaxInt32 + 1), int32(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go int32")},
		{Py.Long_FromInt64, int64(-1), uint(0), fmt.Errorf("Python int is negative, cannot convert to Go uint")},
		{Py.Long_FromInt64, int64(-1), uint8(0), fmt.Errorf("Python int is negative, cannot convert to Go uint8")},
		{Py.Long_FromInt64, int64(-1), uint16(0), fmt.Errorf("Python int is negative, cannot convert to Go uint16")},
		{Py.Long_FromInt64, int64(-1), uint32(0), fmt.Errorf("Python int is negative, cannot convert to Go uint32")},
		{Py.Long_FromInt64, int64(-1), uint64(0), fmt.Errorf("Python int is negative, cannot convert to Go uint64")},

		{Py.Long_FromUint, uint(math.MaxUint8), uint8(math.MaxUint8), nil},
		{Py.Long_FromUint, uint(math.MaxUint16), uint16(math.MaxUint16), nil},
		{Py.Long_FromUint, uint(math.MaxUint32), uint32(math.MaxUint32), nil},
		{Py.Long_FromUint, uint(math.MaxUint8 + 1), uint8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint8")},
		{Py.Long_FromUint, uint(math.MaxUint16 + 1), uint16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint16")},

		{Py.Long_FromUint64, uint64(math.MaxUint8), uint8(math.MaxUint8), nil},
		{Py.Long_FromUint64, uint64(math.MaxUint16), uint16(math.MaxUint16), nil},
		{Py.Long_FromUint64, uint64(math.MaxUint32), uint32(math.MaxUint32), nil},
		{Py.Long_FromUint64, uint64(math.MaxUint64), uint64(math.MaxUint64), nil},
		{Py.Long_FromUint64, uint64(math.MaxUint8 + 1), uint8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint8")},
		{Py.Long_FromUint64, uint64(math.MaxUint16 + 1), uint16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint16")},
		{Py.Long_FromUint64, uint64(math.MaxUint32 + 1), uint32(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint32")},

		{Py.Long_FromUintptr, uintptr(math.MaxUint8), uint8(math.MaxUint8), nil},
		{Py.Long_FromUintptr, uintptr(math.MaxUint16), uint16(math.MaxUint16), nil},
		{Py.Long_FromUintptr, uintptr(math.MaxUint32), uint32(math.MaxUint32), nil},
		{Py.Long_FromUintptr, uintptr(math.MaxUint8 + 1), uint8(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint8")},
		{Py.Long_FromUintptr, uintptr(math.MaxUint16 + 1), uint16(0), fmt.Errorf("Python int doesn't fit, cannot convert to Go uint16")},
	} {
		var o py.Object
		var err error
		switch from := tc.from.(type) {
		case func(int) (py.Object, error):
			o, err = from(tc.value.(int))
		case func(int64) (py.Object, error):
			o, err = from(tc.value.(int64))
		case func(uint) (py.Object, error):
			o, err = from(tc.value.(uint))
		case func(uint64) (py.Object, error):
			o, err = from(tc.value.(uint64))
		case func(uintptr) (py.Object, error):
			o, err = from(tc.value.(uintptr))
		default:
			panic("unsupported signature")
		}
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Long_Type)
		expect(t, Py.Long_CheckExact(o), true)
		expectObject(t, Py, o, tc.test, tc.err)
	}

	for _, tc := range []interface{}{minInt, -1, maxInt, uint(maxUint)} {
		o, err := Py.GoToObject(tc)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expectObject(t, Py, o, tc, nil)
	}
}

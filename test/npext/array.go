/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

// #define NO_IMPORT_ARRAY
// #include "npext.h"
//
// int pyArray_CheckExact(PyObject *o)
// {
//     return PyArray_CheckExact(o);
// }
import "C"
import (
	"gitlab.com/pygolo/py"
)

type Array_Descr struct {
	d *C.PyArray_Descr
}

func (np NumPy) Array_CheckExact(o py.Object) bool {
	return C.pyArray_CheckExact(obj(o)) != 0
}

func (np NumPy) Array_FromAny(o py.Object, d Array_Descr, min_depth, max_depth, requirements int, c py.Object) (py.Object, error) {
	return np.wrap(C.PyArray_FromAny(obj(o), d.d, C.int(min_depth), C.int(max_depth), C.int(requirements), obj(c)))
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

// #define NO_IMPORT_ARRAY
// #include "npext.h"
import "C"
import (
	"fmt"

	"gitlab.com/pygolo/py"
)

func check_array_type(Py py.Py, o py.Object) error {
	np := NumPy{Py}
	if !np.Array_CheckExact(o) {
		return fmt.Errorf("arg is of type: %s", o.Type().Name())
	}
	return nil
}

func convert_array(Py py.Py, o py.Object) (py.Object, error) {
	np := NumPy{Py}
	Py.GoEnterPython()
	defer Py.GoLeavePython()
	return np.Array_FromAny(o, Array_Descr{}, 0, 0, C.NPY_ARRAY_BEHAVED, py.Object{})
}

func npext(Py py.Py, m py.Object) error {
	np := NumPy{Py}
	if err := np.init(); err != nil {
		return err
	}
	if err := Py.Object_SetAttr(m, "check_array_type", check_array_type); err != nil {
		return err
	}
	if err := Py.Object_SetAttr(m, "convert_array", convert_array); err != nil {
		return err
	}
	return nil
}

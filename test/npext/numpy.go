/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

// #include "npext.h"
//
// void* numpy_init(void)
// {
//   // import_array is a macro that makes the caller return NULL on error
//   import_array();
//   return (void *) 42;
// }
import "C"
import (
	"unsafe"

	"gitlab.com/pygolo/py"
)

type NumPy struct {
	py.Py
}

func (np NumPy) init() (err error) {
	if C.numpy_init() == nil {
		return np.GoCatchError()
	}
	return nil
}

func (np NumPy) wrap(o *C.PyObject) (py.Object, error) {
	if o == nil {
		return py.Object{}, np.GoCatchError()
	}
	return py.NewObject(unsafe.Pointer(o)), nil
}

func obj(o py.Object) *C.PyObject {
	return (*C.PyObject)(o.Pointer())
}

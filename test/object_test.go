/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestRef(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	Py.NewRef(py.Object{})
	Py.IncRef(py.Object{})
	Py.DecRef(py.Object{})
	Py.IncRef(py.Object{}, py.Object{})
	Py.DecRef(py.Object{}, py.Object{})
}

func TestTypeObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	expect(t, py.Object{}.Type(), py.TypeObject{})

	o, err := py.None_Type.AsObject().AsTypeObject()
	expectError(t, err, nil)
	expect(t, o, py.None_Type)

	_, err = py.None.AsTypeObject()
	expectError(t, err, fmt.Errorf("not a type object"))

	o = py.NewTypeObject(py.None_Type.AsObject().Pointer())
	expect(t, o, py.None_Type)
}

func TestTypeObjectToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.GoToObject(py.None_Type)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o, py.None_Type.AsObject())
}

func TestTypeObjectFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	var o py.TypeObject
	err = Py.GoFromObject(py.None_Type.AsObject(), &o)
	defer Py.DecRef(o.AsObject())
	expectError(t, err, nil)
	expect(t, o, py.None_Type)

	err = Py.GoFromObject(py.None, &o)
	expectError(t, err, fmt.Errorf("cannot convert a Python NoneType to Go py.TypeObject"))

	var a interface{}
	err = Py.GoFromObject(py.None_Type.AsObject(), &a)
	expectError(t, err, nil)
	expect(t, a, py.None_Type)
	Py.DecRef(a.(py.TypeObject).AsObject())
}

func TestObjectCall(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o_logging, err := Py.Import_Import("logging")
	defer Py.DecRef(o_logging)
	expectError(t, err, nil)

	o_basic_config, err := Py.Object_GetAttr(o_logging, "basicConfig")
	defer Py.DecRef(o_basic_config)
	expectError(t, err, nil)

	o_ret, err := Py.Object_Call(o_basic_config, py.GoArgs{}, py.GoKwArgs{"format": "%(message)s"})
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_ret, err = Py.Object_CallMethod(o_logging, "warning", "test %s", "call")
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_warning, err := Py.Object_GetAttr(o_logging, "warning")
	defer Py.DecRef(o_warning)
	expectError(t, err, nil)

	o_ret, err = Py.Object_CallFunction(o_warning, "test call%d", 2)
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_ret, err = Py.Object_Call(o_warning, py.GoArgs{"test call%d", 3}, nil)
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_ret, err = Py.Object_Call(o_warning, py.GoArgs{"test %s%s", "call", "4"}, py.GoKwArgs{})
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	o_args, err := Py.Tuple_Pack("test %s%s", "call", 5)
	defer Py.DecRef(o_args)
	expectError(t, err, nil)

	o_ret, err = Py.Object_Call(o_warning, o_args, py.Object{})
	defer Py.DecRef(o_ret)
	expectError(t, err, nil)

	_, err = Py.Object_Call(o_warning, "invalid argument type", py.GoKwArgs{})
	expectError(t, err, fmt.Errorf("args must be either of type GoArgs or Object"))

	_, err = Py.Object_Call(o_warning, py.GoArgs{}, "invalid argument type")
	expectError(t, err, fmt.Errorf("kwargs must be either of type GoKwArgs or Object"))

	_, err = Py.Object_Call(o_warning, nil, nil)
	expectError(t, err, &py.GoError{"TypeError", "warning() missing 1 required positional argument: 'msg'", nil})

	_, err = Py.Object_Call(o_warning, py.GoArgs{}, nil)
	expectError(t, err, &py.GoError{"TypeError", "warning() missing 1 required positional argument: 'msg'", nil})

	_, err = Py.Object_Call(o_warning, py.Object{}, nil)
	expectError(t, err, &py.GoError{"TypeError", "warning() missing 1 required positional argument: 'msg'", nil})
}

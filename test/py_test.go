/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"sync"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestPy(t *testing.T) {
	Py := py.Py{}
	err := Py.Close()
	expectError(t, err, fmt.Errorf("could not close the context: the state is not initialized"))

	Py, err = py.GoEmbed()
	expectError(t, err, nil)
	err = Py.Close()
	expectError(t, err, nil)

	Py, err = py.GoEmbed()
	expectError(t, err, nil)

	_, err = py.GoEmbed()
	expectError(t, err, fmt.Errorf("the interpreter is already initialized"))
	err = Py.Close()
	expectError(t, err, nil)

	Py, err = py.GoEmbed()
	expectError(t, err, nil)
	err = Py.Close()
	expectError(t, err, nil)

	err = Py.Close()
	expectError(t, err, fmt.Errorf("could not close the context: the state is not initialized"))
}

func TestPyFlow(t *testing.T) {
	Py, err := py.GoEmbed()
	expectError(t, err, nil)

	_, err = Py.GoNewFlow(nil)
	expectError(t, err, fmt.Errorf("this goroutine already owns a Py context"))

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		Py2, err := Py.GoNewFlow(nil)
		expectError(t, err, nil)
		_, err = Py.GoNewFlow(nil)
		expectError(t, err, fmt.Errorf("this goroutine already owns a Py context"))
		err = Py2.Close()
		expectError(t, err, nil)

		Py2, err = Py.GoNewFlow(nil)
		expectError(t, err, nil)
		err = Py2.Close()
		expectError(t, err, nil)

		_, err = Py2.GoNewFlow(nil)
		expectError(t, err, fmt.Errorf("could not start a new flow: the state is not initialized"))
		err = Py2.Close()
		expectError(t, err, fmt.Errorf("could not close the context: the state is not initialized"))
	}()

	Py.GoLeavePython()
	wg.Wait()
	Py.GoEnterPython()

	err = Py.Close()
	expectError(t, err, nil)
}

func BenchmarkPyFlow(b *testing.B) {
	Py, err := py.GoEmbed()
	expectError(b, err, nil)

	Py.GoLeavePython()
	defer func() {
		Py.GoEnterPython()
		Py.Close()
	}()

	b.SetParallelism(8)
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		wg := sync.WaitGroup{}
		for pb.Next() {
			wg.Add(1)
			go func() {
				defer wg.Done()

				Py2, err := Py.GoNewFlow(nil)
				expectError(b, err, nil)
				err = Py2.Close()
				expectError(b, err, nil)
			}()
			wg.Wait()
		}
	})
}

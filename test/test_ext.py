#
#  Copyright 2022, Pygolo Project contributors
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import math
import re
import threading
from collections import namedtuple

import ext
import pytest
from hypothesis import given
from hypothesis import strategies as st


def nop():
    pass


def summation(n):
    ret = 0
    for i in range(n):
        ret += i
    return ret


def test_module():
    assert ext.__name__ == "ext"
    assert ext.__doc__ == "doc"
    assert ext.pi == math.pi


@pytest.mark.skipif(not getattr(ext, "__package__", None), reason="Requires Go 1.16 or higher")
def test_package():
    import ext.test

    assert ext.__package__ == "ext"
    assert ext.test.__name__ == "ext.test"
    assert ext.test.__package__ == "ext"
    assert re.match("^embed.FS{0[xX][0-9a-fA-F]+}@ext:/test.py$", ext.test.__file__)


def test_object_type():
    assert ext.GoObject.__name__ == "GoObject"
    assert ext.GoObject.__module__ == "ext"

    msg = "cannot create 'GoObject' instances"
    with pytest.raises(TypeError, match=msg) as exc:
        _ = ext.GoObject()
    assert re.match(msg, str(exc.value))


def test_function_type():
    assert ext.GoFunction.__name__ == "GoFunction"
    assert ext.GoFunction.__module__ == "ext"

    msg = "cannot create 'GoFunction' instances"
    with pytest.raises(TypeError, match=msg) as exc:
        _ = ext.GoFunction()
    assert re.match(msg, str(exc.value))


def test_property_type():
    assert ext.GoProperty.__name__ == "GoProperty"
    assert ext.GoProperty.__module__ == "ext"

    msg = "cannot create 'GoProperty' instances"
    with pytest.raises(TypeError, match=msg) as exc:
        _ = ext.GoProperty()
    assert re.match(msg, str(exc.value))


@given(st.floats())
def test_function_call_args(x):
    if math.isnan(x):
        a = ext.modf(x)
        b = math.modf(x)
        assert math.isnan(a[0]) == math.isnan(b[0])
        assert math.isnan(a[1]) == math.isnan(b[1])
    else:
        assert ext.modf(x) == math.modf(x)


def test_function_call():
    # free function
    assert ext.count_args() == 0
    assert ext.count_args("a", "b") == 2

    # free function in a tuple
    T = namedtuple("T", ["count_args"])
    assert T(ext.count_args).count_args() == 0
    assert T(ext.count_args).count_args("a", "b") == 2


def test_method_call():
    class C:
        static_count_args = staticmethod(ext.count_args)
        class_count_args = classmethod(ext.count_args)
        count_args = ext.count_args

    # static method
    assert C.static_count_args() == 0
    assert C.static_count_args("a", "b") == 2
    assert C().static_count_args() == 0
    assert C().static_count_args("a", "b") == 2

    # class method
    assert C.class_count_args() == 1
    assert C.class_count_args("a", "b") == 3
    assert C().class_count_args() == 1
    assert C().class_count_args("a", "b") == 3

    # instance method
    assert C.count_args() == 0
    assert C.count_args("a", "b") == 2
    assert C().count_args() == 1
    assert C().count_args("a", "b") == 3


def test_function_attrs():
    assert ext.modf.__name__ == "modf"
    assert ext.modf.__doc__ == "func(float64) (float64, float64)"
    assert ext.modf.__class__ == ext.GoFunction
    assert ext.modf.__module__ == "ext"

    msg = "attribute '__class__' of 'GoFunction' objects is not writable"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.modf.__class__ = None
    assert str(exc.value) == msg

    msg = "'GoFunction' object has no attribute 'bogus'"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.modf.bogus = "bogus"
    assert re.match(msg, str(exc.value))


def test_buffer():
    assert ext.buffer.__name__ == "buffer"
    assert ext.buffer.__class__ == ext.GoObject
    assert ext.buffer.__module__ == "ext"

    for i, x in enumerate(bytes(ext.buffer)):
        assert x == i

    b = memoryview(ext.buffer)
    for i in range(len(b)):
        b[i] = 0

    for x in bytes(ext.buffer):
        assert x == 0


def test_property():
    assert ext.nil_prop.__name__ == "nil_prop"
    assert ext.ro_prop.__name__ == "ro_prop"
    assert ext.wo_prop.__name__ == "wo_prop"

    assert ext.nil_prop.__class__ == ext.GoProperty
    assert ext.nil_prop.__module__ == "ext"

    msg = "property 'nil_prop' of 'module' has no getter"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.nil_prop.__get__(ext)
    assert re.match(msg, str(exc.value))

    msg = "property 'nil_prop' of 'module' has no setter"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.nil_prop.__set__(ext, "bogus")
    assert re.match(msg, str(exc.value))

    msg = "property 'wo_prop' of 'module' has no getter"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.wo_prop.__get__(ext)
    assert re.match(msg, str(exc.value))

    msg = "property 'ro_prop' of 'module' has no setter"
    with pytest.raises(AttributeError, match=msg) as exc:
        ext.ro_prop.__set__(ext, "bogus")
    assert re.match(msg, str(exc.value))

    assert ext.ro_prop.__get__(ext) == ""
    ext.wo_prop.__set__(ext, "test")
    assert ext.ro_prop.__get__(ext) == "test"


def test_struct():
    assert ext.Point.__name__ == "Point"
    assert ext.NoPoint.__name__ == "NoPoint"
    assert ext.FreeVector.__name__ == "FreeVector"
    # assert ext.Point.__doc__ == "Point(x, y, z)"
    assert ext.Point.__class__ == type
    assert ext.NoPoint.__class__ == type
    assert ext.FreeVector.__class__ == type

    p = ext.Point(0.0, 1.0, 2.0)
    p2 = ext.Point(0.0, z=2.0, y=1.0)
    assert p.x == p2.x
    assert p.y == p2.y
    assert p.z == p2.z

    msg = "takes 2 positional arguments but 1 was given"
    with pytest.raises(TypeError, match=msg) as exc:
        ext.Point.Distance(p)
    assert str(exc.value) == msg

    msg = "cannot create 'NoPoint' instances"
    with pytest.raises(TypeError, match=msg) as exc:
        ext.NoPoint()
    assert str(exc.value) == msg

    a = ext.Point(0.0, 1.0, 2.0)
    b = ext.Point(3.0, 4.0, 5.0)
    v = ext.FreeVector(a, b)
    assert a.Distance(b) == v.Magnitude()

    assert p.__class__ == ext.Point
    assert v.__class__ == ext.FreeVector


def test_struct_inheritance():
    class Vector(ext.FreeVector, ext.Point):
        def __init__(self, a, v):
            b = ext.Point(a.x + v.dx, a.y + v.dy, a.z + v.dz)
            super().__init__(x=a.x, y=a.y, z=a.z, a=a, b=b)

    assert Vector.__name__ == "Vector"
    assert Vector.__class__ == type

    p = ext.Point(0.0, 1.0, 2.0)
    f = ext.FreeVector(ext.Point(), ext.Point(3.0, 4.0, 5.0))
    v = Vector(p, f)
    assert isinstance(v, ext.Point)
    assert isinstance(v, ext.FreeVector)
    assert v.__class__ == Vector
    assert v.x == 0.0
    assert v.y == 1.0
    assert v.z == 2.0
    assert v.dx == 3.0
    assert v.dy == 4.0
    assert v.dz == 5.0

    class VectorOld(ext.Point, ext.FreeVector):
        def __init__(self, a, v):
            b = ext.Point(a.x + v.dx, a.y + v.dy, a.z + v.dz)
            ext.Point.__init__(self, a.x, a.y, a.z)
            ext.FreeVector.__init__(self, a, b)

    v = VectorOld(p, f)
    assert isinstance(v, ext.Point)
    assert isinstance(v, ext.FreeVector)
    assert v.__class__ == VectorOld
    assert v.x == 0.0
    assert v.y == 1.0
    assert v.z == 2.0
    assert v.dx == 3.0
    assert v.dy == 4.0
    assert v.dz == 5.0

    msg = "type 'NoPoint' is not an acceptable base type"
    with pytest.raises(TypeError, match=msg) as exc:

        class NoPoint(ext.NoPoint):
            pass

    assert str(exc.value) == msg


@given(
    st.floats(allow_nan=False, allow_infinity=False),
    st.floats(allow_nan=False, allow_infinity=False),
    st.floats(allow_nan=False, allow_infinity=False),
    st.floats(allow_nan=False, allow_infinity=False),
    st.floats(allow_nan=False, allow_infinity=False),
    st.floats(allow_nan=False, allow_infinity=False),
)
def test_distance(ax, ay, az, bx, by, bz):
    a = ext.Point(ax, ay, az)
    b = ext.Point(bx, by, bz)
    assert ext.Point.Distance(a, b) == a.Distance(b)
    assert ext.Point.Distance(b, a) == b.Distance(a)
    assert a.Distance(b) == b.Distance(a)


def test_ncall():
    cond = threading.Condition()
    count = 1000

    def dec():
        nonlocal count

        with cond:
            if count == 0:
                return
            count -= 1
            if count == 0:
                cond.notify_all()

    ext.ncall(count, dec)
    with cond:
        while count:
            cond.wait()
    assert count == 0


def test_function_go_nop(benchmark):
    benchmark(ext.nop)


def test_function_py_nop(benchmark):
    benchmark(nop)


def test_function_go_sum10(benchmark):
    benchmark(ext.summation, 10)


def test_function_py_sum10(benchmark):
    benchmark(summation, 10)


def test_function_go_sum100(benchmark):
    benchmark(ext.summation, 100)


def test_function_py_sum100(benchmark):
    benchmark(summation, 100)


def test_function_go_sum1000(benchmark):
    benchmark(ext.summation, 1000)


def test_function_py_sum1000(benchmark):
    benchmark(summation, 1000)


def test_function_go_sum10000(benchmark):
    benchmark(ext.summation, 10000)


def test_function_py_sum10000(benchmark):
    benchmark(summation, 10000)


def test_function_go_ncall0(benchmark):
    benchmark(lambda: ext.ncall(0, nop))


def test_function_go_ncall50(benchmark):
    benchmark(lambda: ext.ncall(50, nop))

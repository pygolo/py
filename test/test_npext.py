#
#  Copyright 2022, Pygolo Project contributors
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import npext
import numpy as np
import pytest
from hypothesis import given
from hypothesis import strategies as st


def test_check_array_type():
    a = np.array([1, 2, 3, 4, 5])
    npext.check_array_type(a)

    msg = "arg is of type: NoneType"
    with pytest.raises(RuntimeError, match=msg) as exc:
        npext.check_array_type(None)
    assert str(exc.value) == msg


@given(st.lists(st.integers()))
def test_convert_array(x):
    a = npext.convert_array(x)
    npext.check_array_type(a)
    assert len(a) == len(x)


def test_convert_array100(benchmark):
    benchmark(npext.convert_array, range(100))


def test_convert_array1000(benchmark):
    benchmark(npext.convert_array, range(1000))


def test_convert_array10000(benchmark):
    benchmark(npext.convert_array, range(10000))

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"math"
	"testing"

	"gitlab.com/pygolo/py"
)

func TestTuple(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	o, err := Py.Tuple_Pack(py.True, py.False, py.None)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Tuple_Type)
	expect(t, Py.Tuple_CheckExact(o), true)
	expectObjectLen(t, Py, o, 3)
}

func TestTupleFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	tc := []interface{}{
		py.True,
		`Hello, 世界`,
		math.Pi,
		maxInt,
	}

	o, err := Py.Tuple_Pack(tc...)
	defer Py.DecRef(o)
	expectError(t, err, nil)
	expect(t, o.Type(), py.Tuple_Type)
	expect(t, Py.Tuple_CheckExact(o), true)
	expectObjectLen(t, Py, o, len(tc))

	expectObjectAny(t, Py, o, []interface{}{
		true,
		`Hello, 世界`,
		math.Pi,
		maxInt,
	}, nil)
}

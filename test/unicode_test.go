/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"testing"
	"unicode/utf8"

	"gitlab.com/pygolo/py"
)

var unicodeTestcases = []string{
	``,
	`Hello, 世界`,
	`Hello, world!\0`,
	`With\0 null\0 chars\0`,
}

func TestUnicode(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range unicodeTestcases {
		o, err := Py.Unicode_FromString(tc)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Unicode_Type)
		expect(t, Py.Unicode_CheckExact(o), true)
		expect(t, Py.Unicode_GetLength(o), utf8.RuneCountInString(tc))
		test, err := Py.Unicode_AsUTF8(o)
		expectError(t, err, nil)
		if !utf8.ValidString(test) {
			t.Errorf("test is not valid utf-8: %q", test)
		}
		expect(t, test, tc)
	}
}

func TestUnicodeToObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range unicodeTestcases {
		o, err := Py.GoToObject(tc)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expect(t, o.Type(), py.Unicode_Type)
		test, err := Py.Unicode_AsUTF8(o)
		expectError(t, err, nil)
		expect(t, test, tc)
	}
}

func TestUnicodeFromObject(t *testing.T) {
	Py, err := py.GoEmbed()
	defer Py.Close()
	expectError(t, err, nil)

	for _, tc := range unicodeTestcases {
		o, err := Py.Unicode_FromString(tc)
		defer Py.DecRef(o)
		expectError(t, err, nil)
		expectObject(t, Py, o, tc, nil)
		expectObjectAny(t, Py, o, tc, nil)
	}
}

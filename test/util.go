/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/pygolo/py"
)

func lexicalMajorMinor(ver string) string {
	parts := strings.Split(ver, ".")
	return fmt.Sprintf("%03s.%03s", parts[0], parts[1])
}

func getPythonVersion(t testing.TB, Py py.Py) string {
	t.Helper()

	o_sys, err := Py.Import_Import("sys")
	defer Py.DecRef(o_sys)
	expectError(t, err, nil)

	o_version, err := Py.Object_GetAttr(o_sys, "version")
	defer Py.DecRef(o_version)
	expectError(t, err, nil)

	var version string
	err = Py.GoFromObject(o_version, &version)
	expectError(t, err, nil)

	return version
}

/*
 * Copyright 2022, Pygolo Project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py

// #include "pygolo.h"
import "C"

// Type_IsSubtype returns true if a is a subtype of b.
//
// C API: https://docs.python.org/3/c-api/type.html#c.PyType_IsSubtype
func (Py Py) Type_IsSubtype(a, b TypeObject) bool {
	return C.PyType_IsSubtype(a.t, b.t) != 0
}
